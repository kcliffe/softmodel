﻿using NServiceBus;
using NServiceBus.Logging;
using Raven.Client;
using SoftModel.Messages;
using System;

namespace ThirdParty
{
    public class RecordStateChangeEventHandler : IHandleMessages<SoftModelStateChangeEvent>
    {
        public IDocumentSession Session { get; set; }
        public ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().Name);

        public RecordStateChangeEventHandler(IDocumentSession session)
        {
            Session = session;
        }

        public void Handle(SoftModelStateChangeEvent message)
        {
            //Session.Store(new ThirdPartyTransaction
            //{
            //    EventType = message.OriginatingEvent.EventName,
            //    TargetId = message.OriginatingEvent.Target.Id,
            //    Received = DateTime.Now
            //});

            Log.Debug("Recieved event: " + message);
        }
    }
}

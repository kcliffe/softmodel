﻿using System;

namespace ThirdParty
{
    public class ThirdPartyTransaction
    {
        public string EventType { get; set; }
        public string TargetId { get; set; }
        public DateTime Received { get; set; }
    }
}

﻿using System.Reflection;
using NServiceBus;
using NServiceBus.UnitOfWork;
using Raven.Client;
using Raven.Client.Document;
using StructureMap;
using System.Configuration;
using StructureMap.Pipeline;
using SoftModel.Messages;
using NServiceBus.Persistence.Raven;
using System;

namespace Integration
{
    public class EndpointConfig : IConfigureThisEndpoint, IWantCustomInitialization, AsA_Server
    {
        /// <summary>
        /// Configure NServiceBus
        /// </summary>
        public void Init()
        {
            // Use Jason for message serialization
            Configure.Serialization.Json();

            Configure
                // Only check this assembly for handlers
                .With()
                .Log4Net()
                // IOC with Structuremap
                .StructureMapBuilder()
                .UseTransport<Msmq>()
                .InMemorySubscriptionStorage()
                .UseInMemoryTimeoutPersister()
                // Message defined in this namespace
                .DefiningMessagesAs(t => t.Namespace == "SoftModel.Messages")
                .UnicastBus();
        }
    }

    public class SetupContainer : IWantCustomInitialization
    {
        public IBus Bus { get; set; }

        public void Init()
        {
            var store = new DocumentStore { Url = ConfigurationManager.AppSettings["operationStoreUrl"] };

            store.ResourceManagerId = Guid.NewGuid();
            store.Initialize();
            
            // Wire up StructureMap to perform ctor / property injection for handlers
            ObjectFactory.Configure(c =>
            {
                c.For<IDocumentStore>()
                    .Singleton()
                    .Use(store);

                c.For<IDocumentSession>()
                    .Use(ctx => ctx.GetInstance<IDocumentStore>().OpenSession("SoftModel"));

                c.For<IManageUnitsOfWork>()
                    .LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.PerRequest))
                    .Use<RavenUnitOfWork>();
            });
        }
    }
}

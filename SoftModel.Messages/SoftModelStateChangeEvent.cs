﻿using NServiceBus;

namespace SoftModel.Messages
{
    public class SoftModelStateChangeEvent : IEvent
    {
        public SoftModelEventBase OriginatingEvent { get; set; }

        public override string ToString()
        {
            return string.Format("Target Id: {0}: Event Name:{1}", OriginatingEvent.Target.Id, OriginatingEvent.EventName);
        }
    }
}
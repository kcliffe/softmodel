﻿using SoftModel.Schema;

namespace SoftModel.Messages
{
    public enum SchemaChangeType
    {
        AddField,
        RenameField, 
        DisableField
    }

    // TODO. Seperate messages!
    public class SchemaChange
    {
        public SchemaChangeType SchemaChangeType { get; set; }

        //private readonly SchemaChangeType schemaChangeType;
        //public SchemaChangeType SchemaChangeType
        //{
        //    get { return schemaChangeType; }
        //}

        public string OldName { get; set; }

        //private readonly string oldName;
        //public string OldName
        //{
        //    get { return oldName; }
        //}

        public string NewName { get; set; }

        //private readonly string newName;
        //public string NewName
        //{
        //    get { return newName; }
        //}

        public string DisabledFieldName { get; set; }

        //private readonly string disabledFieldName;
        //public string DisabledFieldName
        //{
        //    get { return disabledFieldName; }
        //}

        public FieldDescriptor NewField { get; set; }

        //private readonly FieldDescriptor newField;
        //public FieldDescriptor NewField
        //{
        //    get { return newField; }
        //}

        // Json serialization
        public SchemaChange()
        {
        }

        public SchemaChange(SchemaChangeType schemaChangeType)
        {
            SchemaChangeType = schemaChangeType;
        }

        /// <summary>
        /// Field rename. TODO. Do we care?
        /// </summary>
        /// <param name="schemaChangeType"></param>
        /// <param name="oldName"></param>
        /// <param name="newName"></param>
        public SchemaChange(SchemaChangeType schemaChangeType, string oldName, string newName)
        {
            OldName = oldName;
            NewName = newName; 
        }

        /// <summary>
        /// Field added
        /// </summary>
        /// <param name="schemaChangeType"></param>
        /// <param name="fieldDescriptor"></param>
        public SchemaChange(SchemaChangeType schemaChangeType, FieldDescriptor fieldDescriptor)
        {
            NewField = fieldDescriptor;
        }

        /// <summary>
        /// Field disabled
        /// </summary>
        /// <param name="schemaChangeType"></param>
        /// <param name="disabledFieldName"></param>
        public SchemaChange(SchemaChangeType schemaChangeType, string disabledFieldName)
        {
            DisabledFieldName = disabledFieldName;
        }
    }
}

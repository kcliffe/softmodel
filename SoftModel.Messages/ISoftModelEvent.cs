﻿namespace SoftModel.Messages
{
    /// <summary>
    /// Marker interface for events that can be published
    /// </summary>
    public interface ISoftModelEvent
    {
    }
}

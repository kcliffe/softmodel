﻿using System.Collections.Generic;

namespace SoftModel.Messages
{
    public class SurveyPublishedEvent
    {
        public string GroupIdentifier { get; set; }

        public string SurveyId { get; set; }
    }
}

﻿using SoftModel.Schema;
using SoftModel.Schema.Survey;
using System.Collections.Generic;

namespace SoftModel.Messages
{       
    public class SoftModelEventBase
    {
        /// <summary>
        /// Required by MessageQueue.Send()
        /// </summary>
        public SoftModelEventBase()
        {
        }
     
        // private set removed for message queue transmission
        public dynamic Target { get; set; }

        // private set removed for message queue transmission
        public string OriginatingUser { get; set; }

        // Used when the event needs to be displayed e.g. Audit logs
        public string EventName { get; set; }

        public SoftModelEventBase(dynamic target, string originatingUser)
        {            
            Target = target;
            OriginatingUser = originatingUser;
        }
    }

    public class SurveyCreatedEvent : SoftModelEventBase
    {
        public SurveyCreatedEvent(Survey survey, string originatingUser) 
            : base(survey, originatingUser)
        {
        }
    }

    public class SurveyUpdatedEvent : SoftModelEventBase
    {
        public SurveyUpdatedEvent(Survey survey, string originatingUser)
            : base(survey, originatingUser)
        {
        }
    }

    public class SoftModelSchemaCreatedEvent : SoftModelEventBase
    {
        public SoftModelSchemaCreatedEvent(SchemaBase recordDescriptor, string originatingUser)
            : base(recordDescriptor, originatingUser)
        {
            EventName = "Schema Created";
        }
    }

    public class SoftModelSchemaUpdatedEvent : SoftModelEventBase
    {
        // TODO. Use immutable form below when we have Json serialier extension for private properties        
        public IList<SchemaChange> SchemaChanges { get; set; }

        public SoftModelSchemaUpdatedEvent(RecordDescriptor recordDescriptor,
            IList<SchemaChange> schemaChanges, string originatingUser)
            : base(null, originatingUser)
        {
            SchemaChanges = schemaChanges;
            Target = recordDescriptor;

            EventName = "Schema Updated";
        }
    }

    // After create
    public class SoftModelRecordCreated : SoftModelEventBase
    {
        public bool IsChild { get; set; }

        public SoftModelRecordCreated(Record record, string originatingUser, bool isChild = false)
            : base(record, originatingUser)
        {
            IsChild = isChild;
            EventName = "Record Created";
        }        
    }

    public class SoftModelRecordUpdated : SoftModelEventBase
    {
        public SoftModelRecordUpdated(Record record, string originatingUser)
            : base(record, originatingUser)
        {
            EventName = "Record Updated";
        }
    }

    public class SoftModelRecordDisabled : SoftModelEventBase
    {
        public SoftModelRecordDisabled(Record record, string originatingUser)
            : base(record, originatingUser)
        {
            EventName = "Record Disabled";
        }
    }
}

﻿using System;

namespace SoftModel.Query.Audit
{
    public interface IAuditQuery
    {
        AuditSearchResult AuditLogSearch(string orderBy, string userName, DateTime? start, DateTime? end, 
            int? pageNum, int? pageSize, string eventType, string entityName);
    }
}

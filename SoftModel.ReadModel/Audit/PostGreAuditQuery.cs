﻿using Dapper;
using log4net;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace SoftModel.Query.Audit
{
    public class PostGreAuditQuery : IAuditQuery
    {
        private readonly string connString;
        private const string schemaName = "public";
        private static ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        // private static string DateFormat = "yyyy-MM-ddThh:mm:ss";

        public PostGreAuditQuery()
        {
            connString = ConfigurationManager.ConnectionStrings["readModel-postgre"].ConnectionString;
        }

        /// <summary>
        /// Return a "page" of audit results
        /// </summary>
        /// <param name="userName">filter by username</param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="pageNum"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public AuditSearchResult AuditLogSearch(string orderBy, string userName, DateTime? start, DateTime? end, 
            int? pageNum, int? pageSize, string eventType, string entityname)
        {
            using (var connection = new NpgsqlConnection(connString))
            {
                try
                {
                    // TODO. Why am i doing all this ref stuff crap?
                    bool userLikeMatch = userName.EndsWith("*");
                    bool eventTypeLikeMatch = eventType.EndsWith("*");
                    bool entitynameLikeMatch = entityname.EndsWith("*");

                    var selectClause = PrepareAuditQuery(ref userName, ref start, end, ref pageNum, pageSize, orderBy, ref eventType, ref entityname);
                    var countClause = PrepareCountQuery(userLikeMatch, ref userName, ref start, end, ref eventType, eventTypeLikeMatch, ref entityname, entitynameLikeMatch);

                    connection.Open();

                    var queryParams = new
                        {
                            entityName = entitynameLikeMatch ? entityname + "%" : entityname,
                            userName = userLikeMatch ? userName + "%" : userName,
                            eventType = eventTypeLikeMatch ? eventType + "%" : eventType,
                            start = start,
                            end = end
                        };

                    var pageResults = connection.QueryAsync<AuditLog>(selectClause, queryParams).Result;
                    var totalItems = connection.QueryAsync<long>(countClause,queryParams).Result.Single();

                    return new AuditSearchResult { 
                        // Audit records
                        AuditLogEntries = pageResults,
                        // Total count for current search criteria (allowing paging)
                        TotalResultCount = totalItems
                    };
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    // swallow
                    return new AuditSearchResult();
                }
            }
        }

        private static string PrepareAuditQuery(ref string userName, ref DateTime? start, DateTime? end, 
            ref int? pageNum, int? pageSize, string orderBy,
            ref string eventType, ref string entityname)
        {
            bool userLike = false;
            bool eventTypeLike = false;
            bool entitynameLike = false;

            var whereClauses = new List<string>();

            if (!string.IsNullOrWhiteSpace(eventType))
            {
                eventTypeLike = userName.EndsWith("*");

                if (eventTypeLike)
                {
                    eventType = eventType.Substring(0, eventType.Length - 1);
                    whereClauses.Add(string.Format("eventType like @eventType", eventType));
                }
                else
                    whereClauses.Add(string.Format("eventType = @eventType", eventType));
            }

            if (!string.IsNullOrWhiteSpace(entityname))
            {
                entitynameLike = entityname.EndsWith("*");

                if (entitynameLike)
                {
                    entityname = entityname.Substring(0, entityname.Length - 1);
                    whereClauses.Add(string.Format("entityName like @entityname", eventType));
                }
                else
                    whereClauses.Add(string.Format("entityName = @entityname", eventType));
            }

            if (string.IsNullOrWhiteSpace(orderBy))
            {
                // should be equivalent to lastmodifed desc but uses PK
                orderBy = "id desc";
            }
            
            if (!string.IsNullOrWhiteSpace(userName))
            {
                userLike = userName.EndsWith("*");

                if (userLike)
                {
                    userName = userName.Substring(0, userName.Length - 1);
                    whereClauses.Add("username like @userName");
                }
                else                
                    whereClauses.Add("username = @userName");                
            }

            if (!start.HasValue)            
                start = DateTime.Now.AddDays(-7).StartOfDay();
            
            if (!end.HasValue)           
                start = DateTime.Now.EndOfDay();
           
            whereClauses.Add("lastmodified >= @start");
            whereClauses.Add("lastmodified <= @end");

            if (start > end) start = end.Value.StartOfDay();

            if (!pageNum.HasValue)            
                pageNum = 0;
            
            if (!pageSize.HasValue)            
                pageSize = 128;
            
            var selectClause = "select * from public.AuditLog";

            selectClause = whereClauses.Count > 0
                ? string.Format("{0} where {1} order by {4} OFFSET {2} ROWS FETCH NEXT {3} ROWS ONLY",
                    selectClause,
                    string.Join(" and ", whereClauses),
                    pageNum * pageSize,
                    pageSize,
                    orderBy)
                : selectClause;

            return selectClause;
        }

        private static string PrepareCountQuery(bool userLikeMatch, ref string userName, ref DateTime? start, DateTime? end, 
            ref string eventType, bool eventTypeLike, ref string entityname, bool entitynameLike)
        {
            var whereClauses = new List<string>();

            if (!string.IsNullOrWhiteSpace(eventType))
            {
                eventTypeLike = userName.EndsWith("*");

                if (eventTypeLike)
                    whereClauses.Add(string.Format("eventType like @eventType", eventType));
                else
                    whereClauses.Add(string.Format("eventType = @eventType", eventType));
            }

            if (!string.IsNullOrWhiteSpace(entityname))
            {
                entitynameLike = entityname.EndsWith("*");

                if (entitynameLike)
                {
                    entityname = eventType.Substring(0, eventType.Length - 1);
                    whereClauses.Add(string.Format("entityName like @entityname", eventType));
                }
                else
                    whereClauses.Add(string.Format("entityName = @entityname", eventType));
            }

            if (!string.IsNullOrWhiteSpace(userName))
            {
                if (userLikeMatch)                
                    whereClauses.Add("username like @userName");                
                else                
                    whereClauses.Add("username = @userName");                
            }

            if (!start.HasValue)            
                start = DateTime.Now.AddDays(-7).StartOfDay();
            
            if (!end.HasValue)            
                start = DateTime.Now.EndOfDay();
            
            whereClauses.Add("lastmodified >= @start");
            whereClauses.Add("lastmodified <= @end");

            if (start > end) 
                start = end.Value.StartOfDay();

            var selectClause = "select count(1) from public.AuditLog";

            selectClause = whereClauses.Count > 0
                ? string.Format("{0} where {1}",
                    selectClause,
                    string.Join(" and ", whereClauses))
                : selectClause;

            return selectClause;
        }
    }
}

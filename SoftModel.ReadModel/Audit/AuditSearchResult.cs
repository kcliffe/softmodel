﻿using System.Collections.Generic;

namespace SoftModel.Query.Audit
{
    public class AuditSearchResult
    {
        public IEnumerable<AuditLog> AuditLogEntries { get; set; }
        public long TotalResultCount { get; set; }

        public AuditSearchResult()
        {
            AuditLogEntries = new List<AuditLog>();
        }
    }
}

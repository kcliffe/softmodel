﻿using System;

namespace SoftModel.Query.Audit
{
    public class AuditLog
    {
        public string OpId { get; set; }
        public DateTime LastModified;
        public string EntityName { get; set; }
        public string EventType { get; set; }
        public string EntityId { get; set; }
        public string DiffPrev { get; set; }
        public string UserName { get; set; }
    }
}

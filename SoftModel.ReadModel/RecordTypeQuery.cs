﻿using SoftModel.Schema;
using System;
using System.Collections;
using System.Collections.Generic;

namespace SoftModel.Query
{
    public class RecordTypeQuery
    {
        public RecordDescriptor RecordDescriptor { get; set; }

        private int pageNum = 1;
        public int PageNum
        {
            get { return pageNum; }
            set { pageNum = value; }
        }

        private int pageSize;
        public int PageSize
        {
            get { return pageSize; }
            set 
            {
                if (value < 25) value = 25;
                pageSize = value; 
            }
        }

        private string filter = string.Empty;
        public string Filter
        {
            get { return filter; }
            set { filter = value; }
        }

        private string fields;
        public string Fields
        {
            get { return fields; }
            set { fields = value; }
        }

        public string OrderBy { get; set; }
       
        public DateTime? AsAtDate { get; set; }
    }

    public class AssociatedRecordsQuery : RecordTypeQuery
    {
        public RecordDescriptor AssociatedRecordDescriptor { get; set; }

        public string ParentId { get; set; }
    }
}

﻿using log4net;
using SoftModel.Schema;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;

namespace SoftModel.Query
{
    public class QueryBase
    {
        private static ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected static List<Record> ResultToRecords(RecordDescriptor recordDescriptor, DbDataReader reader)
        {
            var records = new List<Record>();
            var columnNames = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            while (reader.Read())
            {
                if (!reader.IsDBNull(0))
                {
                    var record = new Record { 
                        Id = reader.GetFieldValueAsync<string>(0).Result,
                        LastModified = reader.GetFieldValueAsync<DateTime>(1).Result
                    };
                    record.RecordDescriptorRef = recordDescriptor.ToRef();

                    // this is where a DynamicObject would be looking good.
                    // record.{Property} = reader.GetFieldValueAsync...

                    for (int i = 2; i < reader.FieldCount; i++)
                    {
                        // TODO. Indexer
                        var field = recordDescriptor.FieldDescriptors
                            .First(f => string.Compare(f.Name, columnNames[i], true, CultureInfo.InvariantCulture) == 0);

                        record.Fields.Add(new FieldValue
                        {
                            FieldName = field.Name,
                            Value = reader[i]
                        });
                    }

                    records.Add(record);
                }
            }

            return records;
        }

        /// <summary>
        /// The associated query needs the fields in the order by clause prefixed due to the joins
        /// to avoid ambiguous sql
        /// </summary>
        /// <param name="orderByClause"></param>
        /// <returns></returns>
        protected string PrefixOrderByClause(string prefix, string orderByClause)
        {
            if (string.IsNullOrWhiteSpace(orderByClause))
                return "order by prmy.id desc";

            var sb = new StringBuilder("order by ");
            var orderByElements = orderByClause.Split(',');
            foreach (var element in orderByElements)
            {
                // TODO. Handle default (desc) specifier not being present and general bounds checking
                var orderPair = element.Split(':');
                sb.AppendFormat("{0}.{1} {2}", prefix, orderPair[0], orderPair[1]);
            }

            return "?";
        }
    }
}

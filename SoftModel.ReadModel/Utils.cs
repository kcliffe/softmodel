﻿using log4net;
using System;
using System.Diagnostics;

namespace SoftModel.Query
{
    public static class Utils
    {
        private static ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void Time(Action action, Func<string> logFunc)
        {
            var sw = new Stopwatch();
            sw.Start();
            action();
            sw.Stop();
            Log.DebugFormat("{0} in {1}", logFunc(), sw.Elapsed);
        }
    }
}

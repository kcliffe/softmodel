﻿using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.SqlServerCe;
using System.Linq;
using SoftModel.Schema;
using System;

namespace SoftModel.Query
{
    /// <summary>
    /// <b>Trivial</b> implementation of a query class to return records from the relational read model.
    /// </summary>
    public class SqlCeQuery : QueryBase, IQuery
    {        
        private readonly string connString;

        public SqlCeQuery()
        {
            connString = ConfigurationManager.ConnectionStrings["readModel"].ConnectionString;

            //connString =
            //      @"Data Source=C:\Users\Us\Documents\Visual Studio 2010\Projects\SoftModel\ReadModel.Data\Softmodel.sdf;Persist Security Info=False;";
        }

        public SqlCeQuery(string connString)
        {
            this.connString = connString;
        }

        /// <summary>
        /// Return a page of ordered, filtered records given a RecordSchema
        /// </summary>
        /// <param name="recordDescriptor"></param>
        /// <param name="pageNum"></param>
        /// <param name="pageSize"></param>
        /// <param name="columnList"></param>
        /// <param name="orderByClause">Order by descending PK by default</param>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public List<Record> ForRecordsOfType(RecordTypeQuery recordTypeQuery)
        {
            using (var connection = new SqlCeConnection(connString))
            {
                connection.OpenAsync().Wait();
                var command = connection.CreateCommand();

                var tableName = recordTypeQuery.RecordDescriptor.Name.Trim().Replace(" ", "");

                var filter = recordTypeQuery.Filter ?? "1 = 1";
                string effectiveFilterClause = string.Format("{0} and {1}", filter, 
                string.Format("(lastModified IN (SELECT MAX(lastModified) FROM {0} AS O1 where O1.id = {1})", tableName, "id"));

                command.CommandText = string.Format("select {0} from [{1}] where {5} {4} OFFSET {2} ROWS FETCH NEXT {3} ROWS ONLY",
                    recordTypeQuery.Fields,
                    tableName,
                    recordTypeQuery.PageNum * recordTypeQuery.PageSize,
                    recordTypeQuery.PageSize,
                    recordTypeQuery.OrderBy,
                    effectiveFilterClause);

                command.CommandType = System.Data.CommandType.Text;

                var result = command.ExecuteReaderAsync(System.Data.CommandBehavior.SingleResult | System.Data.CommandBehavior.CloseConnection).Result;

                return ResultToRecords(recordTypeQuery.RecordDescriptor, result);
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public Record Get(string recordId, string fields, RecordDescriptor recordDescriptor)
        {
            throw new NotImplementedException();
        }

        public List<Record> AssociatedRecords(AssociatedRecordsQuery recordTypeQuery)
        {
            throw new NotImplementedException();
        }
    }
}

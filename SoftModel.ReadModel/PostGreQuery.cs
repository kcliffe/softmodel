﻿using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.SqlServerCe;
using System.Linq;
using SoftModel.Schema;
using Npgsql;
using System;
using System.Diagnostics;
using System.Text;

namespace SoftModel.Query
{
    /// <summary>
    /// <b>Trivial</b> implementation of a query class to return records from the relational read model.
    /// 
    /// This class could now badly use a refactor to replace all of the internal string manipulation
    /// - possibly something like specialized Catalog class which provides strongly typed
    /// methods to return query fragments given a RecordTypeQuery?
    /// </summary>
    public class PostGreQuery : QueryBase, IQuery
    {
        private readonly string connString;
        private const string schemaName = "public";

        public PostGreQuery()
        {
            connString = ConfigurationManager.ConnectionStrings["readModel-postgre"].ConnectionString;
        }

        public PostGreQuery(string connString)
        {
            this.connString = connString;
        }

        /// <summary>
        /// Return a  single record given a recordId
        /// </summary>
        /// <param name="recordId"></param>
        /// <param name="recordDescriptor"></param>
        /// <param name="columnList"></param>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public Record Get(string recordId, string fields, RecordDescriptor recordDescriptor)
        {
            using (var connection = new NpgsqlConnection(connString))
            {
                connection.OpenAsync().Wait();
                var command = connection.CreateCommand();

                command.CommandText = string.Format("select {0} from {3}.{1} where opId = '{2}' LIMIT 1",
                    fields,
                    recordDescriptor.Name.Trim().Replace(" ", ""),
                    recordId,
                    schemaName);

                command.CommandType = System.Data.CommandType.Text;

                var result = command.ExecuteReaderAsync(System.Data.CommandBehavior.SingleResult | System.Data.CommandBehavior.CloseConnection).Result;

                return ResultToRecords(recordDescriptor, result).First();
            }
        }

        /// <summary>
        /// Return a page of ordered, filtered records given a RecordSchema
        /// </summary>
        /// <param name="recordDescriptor"></param>
        /// <param name="pageNum"></param>
        /// <param name="pageSize"></param>
        /// <param name="columnList"></param>
        /// <param name="orderByClause">Order by descending PK by default</param>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public List<Record> ForRecordsOfType(RecordTypeQuery recordTypeQuery)
        {
            using (var connection = new NpgsqlConnection(connString))
            {
                DbCommand command = null;
                Utils.Time(() =>
                {
                    connection.OpenAsync().Wait();
                    command = connection.CreateCommand();
                }, () => "Connection opened");

                var tableName = recordTypeQuery.RecordDescriptor.Name;

                var filter = recordTypeQuery.Filter ?? "1 = 1";
                var asAt = recordTypeQuery.AsAtDate.HasValue ? "and lastModified <= '" + recordTypeQuery.AsAtDate.Value.ToString("yyyy-MM-ddT23:59:59") + "'" : string.Empty;
                var orderBy = string.IsNullOrWhiteSpace(recordTypeQuery.OrderBy) ? "" : string.Format("order by {0}", recordTypeQuery.OrderBy.ToLower());

                string effectiveFilterClause =
                    string.Format("{0} and (lastModified IN (SELECT MAX(lastModified) FROM {2}.{1} as sub where sub.id = prmy.id {3}))",
                    filter, tableName, schemaName, asAt);

                // TODO Parameterized query
                command.CommandText = string.Format("select {0} from {6}.{1} prmy where {5} {4} OFFSET {2} ROWS FETCH NEXT {3} ROWS ONLY",
                    recordTypeQuery.Fields,
                    tableName,
                    recordTypeQuery.PageNum * recordTypeQuery.PageSize,
                    recordTypeQuery.PageSize,
                    orderBy,
                    effectiveFilterClause,
                    schemaName);

                command.CommandType = System.Data.CommandType.Text;

                List<Record> records = null; DbDataReader result = null;

                Utils.Time(() =>
                {
                    result = command.ExecuteReaderAsync(System.Data.CommandBehavior.SingleResult | System.Data.CommandBehavior.CloseConnection).Result;
                }, () => string.Format("Executed query {0}", command.CommandText));

                if (result.HasRows)
                {
                    Utils.Time(() =>
                    {
                        records = ResultToRecords(recordTypeQuery.RecordDescriptor, result);
                    }, () => string.Format("Mapped {0} results", records.Count));

                    return records;
                }

                return new List<Record>();
            }
        }

        /// <summary>
        /// Return a list of records associated via a one to one, one to many, many to many relationship
        /// </summary>
        /// <param name="recordTypeQuery"></param>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public List<Record> AssociatedRecords(AssociatedRecordsQuery recordTypeQuery)
        {
            using (var connection = new NpgsqlConnection(connString))
            {
                var parentRecordTableName = recordTypeQuery.RecordDescriptor.Name.Trim().Replace(" ", "");
                var associatedRecordTableName = recordTypeQuery.AssociatedRecordDescriptor.Name.Trim().Replace(" ", "");

                var filter = recordTypeQuery.Filter ?? "1 = 1";
                var asAt = recordTypeQuery.AsAtDate.HasValue ? "and lastModified <= '" + recordTypeQuery.AsAtDate.Value.ToString("yyyy-MM-ddT23:59:59") + "'" : string.Empty;

                var joinTableName = string.Format("public.{0}_{1}",
                   parentRecordTableName,
                   associatedRecordTableName);

                var joinWhere = string.Format("parent.opid = '{0}'", recordTypeQuery.ParentId);

                string parentIdColumnName = string.Format("{0}_id", parentRecordTableName);
                string associatedIdColumnName = string.Format("{0}_id", associatedRecordTableName);
                string join = string.Format("join {0} as link on link.{1} = prmy.id join public.{2} as parent on link.{3} = parent.id",
                    joinTableName,
                    associatedIdColumnName,
                    parentRecordTableName,
                    parentIdColumnName);

                string effectiveFilterClause =
                    string.Format("{0} and {1} and (prmy.lastModified IN (SELECT MAX(lastModified) FROM {3}.{2} AS sub where sub.id = prmy.id {4}))",
                    filter, joinWhere, associatedRecordTableName, schemaName, asAt);

                DbCommand command = null;
                Utils.Time(() =>
                {
                    connection.OpenAsync().Wait();
                    command = connection.CreateCommand();
                }, () => "Connection opened");

                // columnlist won't work when we actually start selecting a specific set of columns...
                command.CommandText = string.Format("select {0} from {6}.{1} prmy {7} where {5} {4} OFFSET {2} ROWS FETCH NEXT {3} ROWS ONLY",
                    Alias("prmy", recordTypeQuery.Fields),
                    associatedRecordTableName,
                    recordTypeQuery.PageNum * recordTypeQuery.PageSize,
                    recordTypeQuery.PageSize,
                    PrefixOrderByClause("prmy", recordTypeQuery.OrderBy),
                    effectiveFilterClause,
                    schemaName,
                    join);

                command.CommandType = System.Data.CommandType.Text;

                List<Record> records = null; DbDataReader result = null;

                Utils.Time(() =>
                {
                    result = command.ExecuteReaderAsync(System.Data.CommandBehavior.SingleResult | System.Data.CommandBehavior.CloseConnection).Result;
                }, () => string.Format("Parent-child query executed {0}", command.CommandText));

                if (result.HasRows)
                {
                    Utils.Time(() =>
                    {
                        records = ResultToRecords(recordTypeQuery.AssociatedRecordDescriptor, result);
                    }, () => string.Format("Mapped {0} results", records.Count));

                    return records;
                }

                return new List<Record>();
            }
        }

        private string Alias(string alias, string columns)
        {
            StringBuilder sb = new StringBuilder();

            var cols = columns.Split(',');
            for (var i = 0; i < cols.Length; i++)
            {
                cols[i] = string.Format("{0}.{1}", alias, cols[i]);
            }

            return String.Join(",", cols);
        }
    }
}

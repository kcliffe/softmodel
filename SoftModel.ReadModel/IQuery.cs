﻿using SoftModel.Schema;
using System;
using System.Collections.Generic;

namespace SoftModel.Query
{
    public interface IQuery
    {
        Record Get(string recordId, string fields, RecordDescriptor recordDescriptor);
        List<Record> ForRecordsOfType(RecordTypeQuery recordTypeQuery);
        List<Record> AssociatedRecords(AssociatedRecordsQuery recordTypeQuery);
    }
}

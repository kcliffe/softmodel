﻿namespace SoftModel.Integration.Etl.Formatter
{
    public static class SchemaExtensions
    {
        public static string ToTableName(this string src)
        {
            return src.Trim().Replace(" ", "_");
        }

        public static string ToColumnName(this string src)
        {
            return src.Trim().Replace(" ", "_");
        }
    }
}

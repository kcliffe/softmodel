﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using Raven.Client;
using SoftModel.Schema;

namespace SoftModel.Integration.Etl.Formatter
{
    public static class Consts
    {
        // TODO. Move to config
        public const string SchemaName = "public";
    }

    /// <summary>
    /// Format (postgre dialect) commands to be executed against the relational read model.
    /// </summary>
    public class PostGreFormatter : BaseFormatter, IDbCommandFormatter
    {
        public Dictionary<string, string> surveyTypeLookup;

        public PostGreFormatter(IDocumentSession session)
            : base(session)
        {
            DateFormat = "yyyy-MM-ddThh:mm:ss";

            if (typeLookup == null)
            {
                typeLookup = new Dictionary<string, string>
                {
                    {FriendlyTypeName.String, "character varying(1000)"},
                    {FriendlyTypeName.Integer, "integer"},
                    {FriendlyTypeName.Date, "timestamp"},
                    {FriendlyTypeName.Boolean, "boolean"},
                    {FriendlyTypeName.Decimal, "numeric(7,2)"}, 
                    {FriendlyTypeName.Double, "numeric(7,2)"},                    
                    //{FriendlyTypeName.OneToMany, "integer"},
                    //{FriendlyTypeName.ManyToMany, "integer"},
                    {FriendlyTypeName.Enumeration, "character varying(25)"} 
                };
            }

            if (surveyTypeLookup == null)
            {
                surveyTypeLookup = new Dictionary<string, string>
                {
                    {"string", "character varying(1000)"},
                    {"choice", "character varying(1000)"},
                    {"multiple choice", "character varying(1000)"},
                    {"date", "timestamp"},
                    {"time", "timestamp"}
                };
            }
        }

        /// <summary>
        /// Return a string which can be assigned to a SqlCeCommand object and executed to insert into a table.
        /// 
        /// TODO: PERF. GetInsertTableStatement could cache statement and swap out using replace?
        /// </summary>
        /// <param name="record"></param>
        /// <returns></returns>
        public string GetInsertStatement(Record record)
        {
            var recordSchema = TryGetRecordSchema(record.RecordDescriptorRef.RecordDescriptorId);
            if (recordSchema == null)
            {
                throw new Exception(
                    string.Format("Hmm. Record {0} has an invalid record schema reference {1}", record.Id, record.RecordDescriptorRef.RecordDescriptorId));
            }

            // e.g. insert {table} ({columns}) values ({values})
            // assumption PK is sequence(1,1) and named after table e.g. Person -> PersonId
            // var columns =  record.RecordSchemaRef
            // NOTE: use of RETURNING statement which ensures we get a return value for callers that require it.
            const string skeletonInsert = "insert into {3}.{0} ({1}) values ({2}) returning id;";
            var tableName = recordSchema.Name.ToTableName();

            // Insert the operational db (raven) primary key, allowing us to link back to the ravendb record
            List<string> fieldDescriptorsInInsert;
            var columns = String.Join(",", GetInsertColumnList(recordSchema.FieldDescriptors, out fieldDescriptorsInInsert));
            
            // Get field values
            var values = String.Join(",", FormattedValues(recordSchema, record, fieldDescriptorsInInsert));

            var insertStatement = string.Format(skeletonInsert, tableName, columns, values, Consts.SchemaName);
            return insertStatement;
        }

        /// <summary>
        /// Return a valid sql command to create a table.                
        /// </summary>
        public string GetCreateTableStatement(RecordDescriptor recordDescriptor)
        {
            var columns = String.Join(",", GetSchemaColumnList(recordDescriptor.FieldDescriptors));

            var createTableStatement = InternalCreateTableStatement(recordDescriptor, columns);

            return createTableStatement;
        }

        /// <summary>
        /// Return a valid sql command to drop a table
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public string GetDropTableStatement(string tableName)
        {
            return string.Format("drop table {0}.{1}", Consts.SchemaName, tableName);
        }

        /// <summary>
        /// Generates an add column statement for a given data type
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="newField"></param>
        /// <returns></returns>
        public string GetAddColumnStatement(string tableName, FieldDescriptor newField)
        {
            var alterClause = typeLookup[newField.FriendlyType];

            switch (newField.FriendlyType)
            {
                case FriendlyTypeName.Decimal:
                case FriendlyTypeName.Double:
                    // TODO Precision etc
                    // alterClause = string.Format(alterClause, )
                    alterClause = string.Format(alterClause, "6", "2");
                    break;

                case FriendlyTypeName.String:
                    alterClause = string.Format(alterClause, "255");
                    break;
            }

            var statement = string.Format("alter table {0}.{1} add column {2} {3}",
                Consts.SchemaName,
                tableName,
                newField.Label,
                alterClause);

            return statement;
        }

        public string GetRenameColumnStatement(string tableName, string oldName, string newName)
        {
            throw new NotImplementedException();
        }

        public string GetDisableColumnStatement(string tableName, string disabledColumnName)
        {
            throw new NotImplementedException();
        }

        public string GetCreateJoinTableStatement(RecordDescriptor left, RecordDescriptor right)
        {
            var primaryColName = left.Name.ToTableName();
            var foreignColName = right.Name.ToTableName();

            var statement =
                string.Format("CREATE TABLE {2}.{0}_{1} ({0}_id integer NOT NULL, {1}_id integer NOT NULL) WITH (OIDS=FALSE);",
                    primaryColName,
                    foreignColName,
                    Consts.SchemaName);

            return statement;
        }

        public string GetFindParentStatement(Record child)
        {
            var parentOpId = child.GetParentId();
            var parentTableName = child.ParentRecordDescriptorName.ToTableName();

            return string.Format("select id from {0}.{1} where opId = '{2}'", Consts.SchemaName, parentTableName, parentOpId);
        }

        public string GetLinkRecordsStatement(int id, int parentId, Record child)
        {
            var linkTableName = string.Format("{0}_{1}",
                child.ParentRecordDescriptorName.ToTableName(),
                child.RecordDescriptorRef.RecordDescriptorName.ToTableName());

            return string.Format("insert into {0}.{1} values ({2}, {3})", Consts.SchemaName, linkTableName, parentId, id);
        }

        /// <summary>
        /// Very quick, very dirty routine for generating the column name / types
        /// </summary>
        private IEnumerable<string> GetSchemaColumnList(IEnumerable<FieldDescriptor> fieldDefinitions)
        {
            var columnDefs = new List<string>();
            // primary key
            columnDefs.Add("id SERIAL PRIMARY KEY");
            // link to record in operation (raven) schema
            columnDefs.Add("opId VARCHAR(255)");
            // add created date and last modified for point in time query support
            columnDefs.Add("created timestamp without time zone");
            columnDefs.Add("lastModified timestamp without time zone");
            // record is temporarily disabled - may be renabled.
            columnDefs.Add("enabled boolean DEFAULT true");
            // record is deleted - may be physically deleted.
            columnDefs.Add("deleted boolean DEFAULT false");

            foreach (var fd in fieldDefinitions)
            {
                var typeName = fd.FriendlyType;
                var columnName = typeName == "RecordRef" ? fd.Name.ToColumnName() + "Id" : fd.Name.ToColumnName();

                // ignore unmapped types for now (this will soon include references and complex types) so handle later
                if (!typeLookup.ContainsKey(typeName)) continue;

                // todo  - other attribs e.g. null / not null?
                var columnDef = string.Format("{0} {1}", columnName, typeLookup[typeName]);
                columnDefs.Add(columnDef);
            }

            return columnDefs;
        }

        private static string InternalCreateTableStatement(SchemaBase entity, string columns)
        {
            // e.g. create {table} ({columns defs})
            // assumption PK is sequence(1,1) and named after table e.g. Person -> PersonId            
            const string skeletonInsert = "create table if not exists {0}.{1} ({2}) WITH (OIDS=FALSE); " +
                "create index {1}_lastmodified on {0}.{1} using btree (lastmodified DESC);";

            var tableName = entity.Name.ToTableName();
            var createTableStatement = string.Format(skeletonInsert, Consts.SchemaName, tableName, columns);

            Log.DebugFormat("Generated create table statement: {0}", createTableStatement);
            return createTableStatement;
        }
    }
}

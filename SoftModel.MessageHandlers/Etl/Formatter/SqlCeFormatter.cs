﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using Raven.Client;
using SoftModel.Schema;

namespace SoftModel.Integration.Etl.Formatter
{
    /// <summary>
    /// Format commands to be executed against the relational read model
    /// </summary>
    public class SqlCeFormatter : BaseFormatter, IDbCommandFormatter
    {
        public SqlCeFormatter(IDocumentSession session) : base(session)
        {
            if (typeLookup == null)
            {
                typeLookup = new Dictionary<string, string>
                {
                    {FriendlyTypeName.String, "nvarchar(1000)"},
                    {FriendlyTypeName.Integer, "int"},
                    {FriendlyTypeName.Date, "datetime"},
                    {FriendlyTypeName.Boolean, "bit"},
                    {FriendlyTypeName.Decimal, "decimal(5,2)"},                
                    //{FriendlyTypeName.OneToMany, "int"},
                    //{FriendlyTypeName.ManyToMany, "int"},
                    {FriendlyTypeName.Enumeration, "nvarchar(255)"} 
                };
            }
        }

        /// <summary>
        /// Return a string which can be assigned to a SqlCeCommand object and executed to insert into a table.
        /// 
        /// TODO: PERF. GetInsertTableStatement could cache statement and swap out using replace?
        /// </summary>
        /// <param name="record"></param>
        /// <returns></returns>
        public string GetInsertStatement(Record record)
        {
            var recordDescriptor = TryGetRecordSchema(record.RecordDescriptorRef.RecordDescriptorId);
            if (recordDescriptor == null)
            {
                throw new Exception(
                    string.Format("Hmm. Record {0} has an invalid record schema reference {1}", record.Id, record.RecordDescriptorRef.RecordDescriptorId));
            }

            // e.g. insert {table} ({columns}) values ({values})
            // assumption PK is sequence(1,1) and named after table e.g. Person -> PersonId
            // var columns =  record.RecordSchemaRef
            const string skeletonInsert = "insert [{0}] ({1}) values ({2})";
            var tableName = recordDescriptor.Name.Trim().Replace(" ", "");

            // insert the operational db (raven) primary key, allowing us to link back to the ravendb record
            List<string> fieldDescriptorsInInsert;
            var columns = String.Join(",", GetInsertColumnList(recordDescriptor.FieldDescriptors, out fieldDescriptorsInInsert));

            // TODO. Escape
            var values = string.Format("'{0}',", record.Id);

            // TODO. For reference type fields (RecordRef) resolve Id
            values += String.Join(",", FormattedValues(recordDescriptor, record, fieldDescriptorsInInsert));
                        
            var insertStatement = string.Format(skeletonInsert, tableName, columns, values);
            return insertStatement;
        }

        /// <summary>
        /// Return a valid sql command to create a table.                
        /// </summary>
        public string GetCreateTableStatement(RecordDescriptor recordDescriptor)
        {
            // e.g. create {table} ({columns defs})
            // assumption PK is sequence(1,1) and named after table e.g. Person -> PersonId            
            const string skeletonInsert = "create table [{0}] ({1})";
            var tableName = recordDescriptor.Name.Trim().Replace(" ", "");
            var columns = String.Join(",", GetSchemaColumnList(recordDescriptor.FieldDescriptors));

            var createTableStatement = string.Format(skeletonInsert, tableName, columns);
            return createTableStatement;
        }

        /// <summary>
        /// Return a valid sql command to drop a table
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public string GetDropTableStatement(string tableName)
        {
            return string.Format("DROP TABLE [{0}]", tableName);
        }

        /// <summary>
        /// Very quick, very dirty routine for generating the column name / types
        /// </summary>
        private IEnumerable<string> GetSchemaColumnList(IEnumerable<FieldDescriptor> fieldDefinitions)
        {
            var columnDefs = new List<string>();
            // primary key
            columnDefs.Add("[id] INT IDENTITY (1,1) PRIMARY KEY");
            // link to record in operation (raven) schema
            columnDefs.Add("[opId] NVARCHAR(255)");
            // add created date and last modified for point in time query support
            columnDefs.Add("created");
            columnDefs.Add("lastModified");

            foreach (var fd in fieldDefinitions)
            {
                var typeName = fd.SystemTypeName;
                var columnName = typeName == "RecordRef" ? fd.Name + "Id" : fd.Name;

                // ignore unmapped types for now (this will soon include references and complex types) so handle later
                if (!typeLookup.ContainsKey(typeName)) continue;

                // todo  - other attribs e.g. null / not null?
                var columnDef = string.Format("[{0}] {1}", columnName, typeLookup[typeName]);
                columnDefs.Add(columnDef);
            }

            return columnDefs;
        }
        
        public string GetAddColumnStatement(string tableName, FieldDescriptor newField)
        {
            throw new NotImplementedException();
        }
        
        public string GetRenameColumnStatement(string tableName, string oldName, string newName)
        {
            throw new NotImplementedException();
        }

        public string GetDisableColumnStatement(string tableName, string disabledColumnName)
        {
            throw new NotImplementedException();
        }
        
        public string GetCreateJoinTableStatement(RecordDescriptor left, RecordDescriptor right)
        {
            throw new NotImplementedException();
        }

        public string GetFindParentStatement(Record child)
        {
            throw new NotImplementedException();
        }
        
        public string GetLinkRecordsStatement(int id, int parentId, Record child)
        {
            throw new NotImplementedException();
        }
        
        public string GetCreateSurveyStatement(Schema.Survey.Survey survey)
        {
            throw new NotImplementedException();
        }
    }
}

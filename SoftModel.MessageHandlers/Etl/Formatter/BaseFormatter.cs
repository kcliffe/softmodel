﻿using NServiceBus.Logging;
using Raven.Client;
using SoftModel.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Caching;

namespace SoftModel.Integration.Etl.Formatter
{
    public class BaseFormatter
    {
        protected static ILog Log = LogManager.GetLogger("");

        // policy to control lifetime of the items in the <see>ObjectCache</see>
        private readonly CacheItemPolicy policy;

        protected readonly IDocumentSession session;

        // .net to sql data type conversion
        protected static Dictionary<string, string> typeLookup;

        protected static string DateFormat = "dd-MM-yyyyThh:mm:ss";
        
        public BaseFormatter(IDocumentSession session)
        {
            this.session = session;

            policy = new CacheItemPolicy
            {
                SlidingExpiration = new TimeSpan(0, 1, 0)
            };
        }

        /// <summary>
        /// RavenDb is quick, but it enforces 30 requests per session by default. Since GetCreateTableStatement can be called many
        /// times in a "session", we need to cache RecordSchemas.
        /// </summary>
        protected virtual RecordDescriptor TryGetRecordSchema(string recordSchemaId)
        {
            ObjectCache cache = MemoryCache.Default;
            var tryGet = cache.Get(recordSchemaId) as RecordDescriptor;
            if (tryGet != null)
                return tryGet;

            var recordSchema = session.Load<RecordDescriptor>(recordSchemaId);
            if (recordSchema != null)
            {
                cache.Add(recordSchemaId, recordSchema, policy);
                return recordSchema;
            }

            return null;
        }

        /// <summary>
        /// Very quick, very dirty method to return columns in an insert statement for a given record descriptor
        /// </summary>
        protected static IEnumerable<string> GetInsertColumnList(IEnumerable<FieldDescriptor> fieldDefinitions, 
            out List<string> fieldDescriptorsInInsert)
        {
            fieldDescriptorsInInsert = new List<string>();

            // link to record in operation (raven) schema
            var columnDefs = new List<string> { "opId", "created", "lastModified" };
            fieldDescriptorsInInsert.AddRange(columnDefs);

            foreach (var fd in fieldDefinitions)
            {
                var friendlyType = fd.FriendlyType;
                var columnName = friendlyType == "RecordRef" ? fd.Name.ToColumnName() + "Id" : fd.Name.ToColumnName();

                // ignore unmapped types for now (this will soon include references and complex types) so handle later
                if (!typeLookup.ContainsKey(friendlyType)) continue;

                fieldDescriptorsInInsert.Add(fd.Name);

                var columnDef = columnName;
                columnDefs.Add(columnDef);
            }

            return columnDefs;
        }

        /// <summary>
        /// Format a value for insertion via transact Sql statement.
        /// </summary>
        protected virtual IEnumerable<string> FormattedValues(RecordDescriptor recordDescriptor,
            Record record, 
            List<string> forFieldDescriptors)
        {
            yield return string.Format("'{0}'", record.Id);

            foreach (var name in forFieldDescriptors)
            {
                // point in time fields
                if (name == "created") yield return "'" + record.Created.ToString(DateFormat) + "'";
                if (name == "lastModified") yield return "'" + record.Created.ToString(DateFormat) + "'";

                var fieldValue = record.Fields.FirstOrDefault(fv => name == fv.FieldName);
                if (fieldValue == null) continue;

                var friendlyType = recordDescriptor.GetFieldDescriptor(name).FriendlyType;
                if (friendlyType == FriendlyTypeName.String ||
                    friendlyType == FriendlyTypeName.OneToMany ||
                    friendlyType == FriendlyTypeName.Enumeration)
                {
                    yield return "'" + fieldValue.Value + "'";
                }
                else if (friendlyType == FriendlyTypeName.Date)
                {
                    // ISO8601 formatted date. Needs DateTime.Parse as Json serialization is used and Json has no date type.
                    // Date hack as Json serializer in web api doesn't appear to be deserializing to DateTime.
                    if (fieldValue.Value.GetType() == typeof(DateTime))
                        yield return "'" + fieldValue.Value.ToString(DateFormat) + "'";
                    else
                        yield return "'" + DateTime.Parse(fieldValue.Value).ToString(DateFormat) + "'";
                }
                else
                {
                    yield return fieldValue.Value.ToString();
                }
            }
        }
    }
}

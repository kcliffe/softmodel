﻿using SoftModel.Schema;
using SoftModel.Schema.Survey;

namespace SoftModel.Integration.Etl.Formatter
{
    public interface IDbCommandFormatter
    {
        string GetInsertStatement(Record record);
        string GetCreateTableStatement(RecordDescriptor recordDescriptor);
        string GetDropTableStatement(string tableName);
        string GetAddColumnStatement(string tableName, FieldDescriptor newField);
        string GetRenameColumnStatement(string tableName, string oldName, string newName);
        string GetDisableColumnStatement(string tableName, string disabledColumnName);
        string GetCreateJoinTableStatement(RecordDescriptor left, RecordDescriptor right);
        string GetFindParentStatement(Record child);
        string GetLinkRecordsStatement(int id, int parentId, Record child);
    }
}

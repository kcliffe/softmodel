﻿using SoftModel.Messages;
namespace SoftModel.Integration.Etl.Output
{
    public interface IDbOutput
    {
        int ExecuteScalar(string output);
        void ExecuteNonQuery(string output);

        void CreateAuditRecord(SoftModelEventBase @event);
    }
}

﻿using SoftModel.Messages;
using System.Data.SqlServerCe;

namespace SoftModel.Integration.Etl.Output
{
    /// <summary>
    /// Execute queries against the relational read model
    /// </summary>
    public class SqlCeOutput : IDbOutput
    {
        private readonly string connString;
        
        public SqlCeOutput(string connString)
        {
            this.connString = connString;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public void ExecuteNonQuery(string output)
        {
            using (var connection = new SqlCeConnection(connString))
            {
                connection.OpenAsync().Wait();
                var command = connection.CreateCommand();
                command.CommandText = output;
                command.CommandType = System.Data.CommandType.Text;

                command.ExecuteNonQueryAsync();

                // connection.Close();
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public int ExecuteScalar(string output)
        {
            int result;

            using (var connection = new SqlCeConnection(connString))
            {
                connection.OpenAsync().Wait();
                var command = connection.CreateCommand();
                command.CommandText = output;
                command.CommandType = System.Data.CommandType.Text;

                result = (int)command.ExecuteScalarAsync().Result;
            }

            return result;
        }

        public void CreateAuditRecord(SoftModelEventBase @event)
        {
            throw new System.NotImplementedException();
        }
    }
}

﻿using System;
using System.IO;
using System.Text;
using System.Threading;

namespace SoftModel.Integration.Etl.Output
{
    public class ToFileOutput : IDbOutput
    {
        private readonly ReaderWriterLock rwl = new ReaderWriterLock();
        private readonly TimeSpan timeOut = new TimeSpan(0, 0, 5);
        private readonly string outPath;

        public ToFileOutput(string outputPath)
        {
            System.Diagnostics.Trace.Assert(outputPath != null, "output path not set");
            outPath = outputPath;

            if (!outPath.EndsWith(@"\"))
            {
                outPath += @"\";

                if (!Directory.Exists(outPath))
                    Directory.CreateDirectory(outPath);
                
                outPath += @"out.txt";

                if (!File.Exists(outPath))
                    File.Create(outPath);
            }
        }

        public int ExecuteScalar(string output)
        {
            try
            {                
                rwl.AcquireWriterLock(timeOut);
                try
                {
                    using (var file = File.AppendText(outPath))
                    {                        
                        file.WriteAsync(output).Wait();
                    }
                    return 0;
                }
                finally
                {
                    rwl.ReleaseWriterLock();
                }
            }
            catch (ApplicationException)
            {
                return 0;
            }
        }

        public void CreateAuditRecord(Messages.SoftModelEventBase @event)
        {
            throw new NotImplementedException();
        }
        
        public void ExecuteNonQuery(string output)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using Npgsql;
using NServiceBus.Logging;
using SoftModel.Integration.Etl.Formatter;
using SoftModel.Messages;
using SoftModel.Schema;
using SoftModel.Schema.Survey;
using System;

namespace SoftModel.Integration.Etl.Output
{
    /// <summary>
    /// Execute queries against the relational read model
    /// </summary>
    public class PostGreOutput : IDbOutput
    {       
        private readonly string connString;
        protected static string DateFormat = "yyyy-MM-ddThh:mm:ss";
        protected static ILog Log = LogManager.GetLogger("");

        public PostGreOutput(string connString)
        {
            this.connString = connString;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public void ExecuteNonQuery(string output)
        {
            Log.Debug("Executing output command: " + output);
           
            using (var connection = new NpgsqlConnection(connString))
            {
                connection.OpenAsync().Wait();
                var command = connection.CreateCommand();
                command.CommandText = output;
                command.CommandType = System.Data.CommandType.Text;

                command.ExecuteScalarAsync();
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public int ExecuteScalar(string output)
        {
            Log.Debug("Executing output command: " + output);
            int result;

            using (var connection = new NpgsqlConnection(connString))
            {
                connection.OpenAsync().Wait();
                var command = connection.CreateCommand();
                command.CommandText = output;
                command.CommandType = System.Data.CommandType.Text;

                result = (int)command.ExecuteScalarAsync().Result;
            }

            return result;
       } 

        // TODO move to PostGreFormatter
        public void CreateAuditRecord(SoftModelEventBase @event)
        {
            var userName = @event.OriginatingUser;
            var lastUpdate = DateTime.Now.ToString(DateFormat);
            var diffPrev = string.Empty;
            var entityName = string.Empty;
            var entityId = string.Empty;
            var eventType = @event.EventName;

            // TODO. Strategy pattern or something..
            if (@event is SurveyCreatedEvent || @event is SurveyUpdatedEvent)
            {
                var survey = @event.Target as Survey;
                entityId = survey.Id;
                entityName = survey.Name;
            }
            else if (@event is SoftModelSchemaUpdatedEvent || @event is SoftModelSchemaCreatedEvent)
            {
                var recordDescriptor = @event.Target as RecordDescriptor;
                entityId = recordDescriptor.Id;
                entityName = recordDescriptor.Name;
            }
            else
            {
                var record = @event.Target as Record;
                entityId = record.Id;
                entityName = record.RecordDescriptorRef.RecordDescriptorName;
            }

            // TODO. Guard against sql injection
            var auditInsertStatement =
                string.Format("insert into {0}.AuditLog(lastmodified, username, opid, entityname, diffprev, eventtype) values ('{1}','{2}','{3}','{4}','{5}', '{6}')",
                    Consts.SchemaName,
                    lastUpdate,
                    userName,
                    entityId,
                    entityName,
                    diffPrev,
                    eventType);
          
            Log.Debug("Executing audit insert command: " + auditInsertStatement);

            ExecuteNonQuery(auditInsertStatement);
        }
    }
}

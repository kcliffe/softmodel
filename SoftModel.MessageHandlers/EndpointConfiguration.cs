﻿using NServiceBus;
using NServiceBus.UnitOfWork;
using Raven.Client;
using Raven.Client.Document;
using StructureMap;
using System.Configuration;
using SoftModel.Integration.Etl.Formatter;
using SoftModel.Integration.Etl.Output;
using StructureMap.Pipeline;
using SoftModel.Messages;

namespace SoftModel.Integration
{
    public class EndpointConfig : IConfigureThisEndpoint, IWantCustomInitialization, AsA_Server 
    { 
        /// <summary>
        /// Configure NServiceBus
        /// </summary>
        public void Init() 
        {
            // Use Jason for message serialization
            Configure.Serialization.Json();

            Configure
                // Only check this assembly for handlers
                .With()
                .Log4Net()
                // IOC with Structuremap
                .StructureMapBuilder()               
                .UseTransport<Msmq>()                
                .InMemorySubscriptionStorage()
                .UseInMemoryTimeoutPersister()                
                // Message defined in this namespace
                .DefiningMessagesAs(t => t.Namespace == "SoftModel.Messages")
                .DefiningEventsAs(t => t.Namespace == "SoftModel.Messages" && t.Name.EndsWith("Event"))
                .UnicastBus();
        } 
    }

    public class SetupContainer : IWantCustomInitialization
    {
        public IBus Bus { get; set; }

        public void Init() 
        {
            var store = new DocumentStore { Url = ConfigurationManager.AppSettings["operationStoreUrl"] }; 
            
            store.Initialize(); 
            
            // Wire up StructureMap to perform ctor / property injection for handlers
            ObjectFactory.Configure(c => { 
                c.For<IDocumentStore>()
                    .Singleton()
                    .Use(store); 
                
                c.For<IDocumentSession>()
                    .Use(ctx => ctx.GetInstance<IDocumentStore>().OpenSession("SoftModel")); 
                
                c.For<IManageUnitsOfWork>()
                    .LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.PerRequest))
                    .Use<RavenUnitOfWork>();

                c.For<IDbCommandFormatter>()
                    .LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.PerRequest))
                    .Use<PostGreFormatter>();

                c.For<IDbOutput>()
                    .LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.PerRequest))
                    .Use<PostGreOutput>()
                    .Ctor<string>("connString").Is(ConfigurationManager.ConnectionStrings["readModel"].ConnectionString);
                    //.Ctor<string>("outputPath").Is(@"c:\etldump");

                c.For<INotification>()
                    .Use<Notification>()
                    .Ctor<string>("softModelBaseUrl").Is(ConfigurationManager.AppSettings["sofModelBaseUrl"]);
            }); 
                
        }
               
        public void Stop()
        {            
        }

        public void Start()
        {
            // Receiver.SetBus(Bus);
        }

        public void SpecifyOrder(Order order)
        {
            // Ensure that schema update messages don't arrive before schema create messages
            order.Specify(First<SoftModelSchemaCreatedEvent>.Then<SoftModelSchemaUpdatedEvent>());
        }
    }
}

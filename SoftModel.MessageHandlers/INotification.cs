﻿namespace SoftModel.Integration
{
    public interface INotification
    {
        void Send(dynamic jsonToSend);
    }
}

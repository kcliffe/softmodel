﻿using System;
using System.Messaging;
using NServiceBus;
using SoftModel.Messages;

namespace SoftModel.Integration
{
    /// <summary>
    /// Exists soley to recieve messages from the UI publisher which uses MessageQueue class to publish because NServiceBus cannot
    /// be (easily) integrated into the UI due to DLL hell (Json.Net as per usual). 
    /// </summary>
    internal static class Receiver
    {
        static IBus bus;
        private static MessageQueue messageQueue;

        static Receiver()
        {
            messageQueue = new MessageQueue(@".\Private$\softmodel.receiver", QueueAccessMode.Receive)
                                   {
                                       Formatter = new JsonMessageFormatter(System.Text.Encoding.UTF8)
                                   };
          
            messageQueue.BeginReceive(TimeSpan.FromHours(1), messageQueue, MsReceiveCompleted);            
        }

        /// <summary>
        /// Forward messages from the UI to NSB
        /// </summary>
        /// <param name="asyncResult"></param>        
        static void MsReceiveCompleted(IAsyncResult asyncResult)
        {
            messageQueue = asyncResult.AsyncState as MessageQueue;

            // End the asynchronous receive operation
            var msg = messageQueue.EndReceive(asyncResult);

            bus.Send("softmodel.integration", msg.Body);
        }
                
        internal static void SetBus(IBus theBus)
        {
            bus = theBus;

            // Process messages already on the queue at startup
            while (messageQueue.Peek() != null)
            {
                var message = messageQueue.Receive(TimeSpan.FromSeconds(5));
                if (message != null)
                {                    
                    bus.Send("softmodel.integration", message.Body);
                }
            }    
        }
    }
}

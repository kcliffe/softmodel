﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;

namespace SoftModel.Integration
{
    /// <summary>
    /// The integration endpoint (this project) lies at the end of an async message bus. We are updated on an 
    /// eventually consistent basis. 
    /// 
    /// Clients in our ecosystem may want to be notified when data is updated.
    /// 
    /// We cannot use signalr or similar from the API directly as the data will not have been updated
    /// at that point. So we make an http call to a notification controller on the API.
    /// </summary>
    public class Notification : INotification
    {
        private readonly string softModelBaseUrl;
        // Simple store for notification throttling
        private readonly Dictionary<string, TimeSpan> lastSentByItemType;
        // Throttle period - 5 seconds
        TimeSpan minSendPeriod = new TimeSpan(0, 0, 5);

        public Notification(string softModelBaseUrl)
        {
            this.softModelBaseUrl = softModelBaseUrl;
            lastSentByItemType = new Dictionary<string, TimeSpan>();
        }

        public void Send(dynamic jsonToSend)
        {
            if (NotificationNotThrottled(jsonToSend))
            {
                var jsonContent = JsonConvert.SerializeObject(jsonToSend);
                var client = new RestClient(softModelBaseUrl);
                var request = new RestRequest("notification/notify", Method.PUT);
                request.AddParameter("application/json; charset=utf-8", jsonContent, ParameterType.RequestBody);
                request.RequestFormat = DataFormat.Json;
                var response = client.Execute(request);
            }
        }

        private bool NotificationNotThrottled(dynamic jsonToSend)
        {
            TimeSpan lastSent;
            var recordDescriptorName = jsonToSend.RecordDescriptorName;
            if (lastSentByItemType.TryGetValue(recordDescriptorName, out lastSent))
            {
                if (lastSent <= minSendPeriod)
                {
                    return false;
                }
            }

            lastSentByItemType[recordDescriptorName] = DateTime.Now.TimeOfDay;
            return true;
        }
    }
}

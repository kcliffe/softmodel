﻿using Dapper;
using Npgsql;
using NServiceBus;
using NServiceBus.Logging;
using Raven.Client;
using SoftModel.Integration.Etl.Formatter;
using SoftModel.Integration.Etl.Output;
using SoftModel.Messages;
using SoftModel.Schema.Survey;
using System;
using System.Configuration;
using System.Transactions;
using System.Linq;

namespace SoftModel.Integration.Handlers
{
    /// <summary>
    /// Push changes from the operational store to a relational store.
    /// </summary>
    public class SurveyCreatedHandler : IHandleMessages<SurveyCreatedEvent>
    {
        protected static ILog Log = LogManager.GetLogger("");
        private readonly string connString;

        public IDocumentSession Session { get; set; }
        public IDbCommandFormatter DbCommandFormatter { get; set; }
        public IDbOutput DbOutput { get; set; }

        public SurveyCreatedHandler(IDocumentSession session)
        {
            connString = ConfigurationManager.ConnectionStrings["readModel-postgre"].ConnectionString;
            Session = session;
        }

        public SurveyCreatedHandler(IDocumentSession session, IDbCommandFormatter dbCommandFormatter, IDbOutput dbOutput)
            : this(session)
        {
            DbCommandFormatter = dbCommandFormatter;
            DbOutput = dbOutput;
        }

        /// <summary>
        /// A record has been created, begin our "ETL" to the read model
        /// </summary>
        /// <param name="message"></param>
        public void Handle(SurveyCreatedEvent @event)
        {
            using (var transaction = new TransactionScope())
            {
                using (var connection = new NpgsqlConnection(connString))
                {
                    var survey = @event.Target as Survey;
                    connection.Open();

                    Log.Debug(string.Format("Creating survey {0}", survey.Name));

                    // Survey
                    var surveyId = connection.Query<int>(@"insert into survey(opId, dateCreated, lastModified, Title, createdBy) 
                        values (@opId, @today, @lastModified, @title, @createdBy) returning id",
                            new
                            {
                                opId = survey.Id,
                                today = DateTime.Now,
                                lastModified = DateTime.Now,
                                title = survey.Name,
                                createdBy = @event.OriginatingUser
                            }).Single();

                    Log.Debug(string.Format("Creating {0} questions for survey", survey.Questions.Count));

                    // Question(s)
                    foreach (var question in survey.Questions)
                    {
                        connection.Execute(@"INSERT INTO question(
                            surveyid, title, helptext, type, datecreated, lastmodified, 
                            modifiedby)
                        values (@surveyId, @title, @helpText, @type, @today, @lastModified, @createdBy)",
                            new
                            {
                                surveyId = surveyId,
                                title = question.Name,
                                helptext = question.HelpText,
                                type = question.Type,
                                today = DateTime.Now,
                                lastModified = DateTime.Now,
                                createdBy = @event.OriginatingUser
                            });
                    }                    
                }

                // create audit record
                DbOutput.CreateAuditRecord(@event);

                transaction.Complete();
            }
        }
    }
}

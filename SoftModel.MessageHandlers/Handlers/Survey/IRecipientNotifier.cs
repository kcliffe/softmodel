﻿namespace SoftModel.Integration.Handlers
{
    public interface ISurveyRecipientNotifier
    {
        void Notify(string groupId, string surveyId);
    }
}

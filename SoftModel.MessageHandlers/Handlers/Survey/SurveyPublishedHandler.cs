﻿using NServiceBus;
using Raven.Client;
using SoftModel.Messages;
using System;
using SoftModel.Integration.Handlers;
using System.IO;
using SoftModel.Schema.Security;
using System.Linq;
using SoftModel.Schema.Survey;

namespace SoftModel.Integration.Handlers
{
    public class SurveyPublishedHandler : IHandleMessages<SurveyPublishedEvent>
    {
        readonly ISurveyRecipientNotifier recipientNotifier;

        public SurveyPublishedHandler(ISurveyRecipientNotifier recipientNotifier)
        {
            this.recipientNotifier = recipientNotifier;
        }

        public void Handle(SurveyPublishedEvent message)
        {
            // fake recipient notification
            recipientNotifier.Notify(message.GroupIdentifier, message.SurveyId);
        }
    }

    public class FakeRecipientNotifier
    {
        readonly IDocumentSession session;
        public FakeRecipientNotifier(IDocumentSession session)
        {
            this.session = session;
        }

        public void Notify(string groupId, string surveyId)
        {
            // We trust the message payload is valid :-) 
            var survey = session.Load<Survey>(surveyId);
           
            // for the fake notifier, just create a file for each group
            using (var outputFile = File.CreateText(string.Format(@"\Notifications\survey_{0}", DateTime.Now.ToFileTimeUtc())))
            {
                var group = session
                    .Include<Group>(x=>x.UserRefs.Select(y=>y.UserId))
                    .Load<Group>(groupId);

                foreach (var thing in group.UserRefs)
                {
                    var user = session.Load<User>(thing.UserId);

                    outputFile.WriteLineAsync(string.Format(@"Hi {3}, you've been invited to responsd to a survey ({0}).
                        Use this link: http://localhost:49554/#survey/response?sId={1}&uId={2}",
                        survey.Name, surveyId, user.Id, user.Name)).Wait();
                }
            }
        }
    }
}

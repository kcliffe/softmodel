﻿using NServiceBus;
using Raven.Client;
using SoftModel.Expressions;
using SoftModel.Integration.Etl.Formatter;
using SoftModel.Integration.Etl.Output;
using SoftModel.Messages;
using System.Transactions;
using System.Linq;
using SoftModel.Schema;
using NServiceBus.Logging;

namespace SoftModel.Integration.Handlers
{
    /// <summary>
    /// Push changes from the operational store to a relational store.
    /// </summary>
    public class SchemaUpdatedHandler : IHandleMessages<SoftModelSchemaUpdatedEvent>
    {
        protected static ILog Log = LogManager.GetLogger("");

        public IDocumentSession Session { get; set; }
        public IDbCommandFormatter DbCommandFormatter { get; set; }
        public IDbOutput DbOutput { get; set; }

        public SchemaUpdatedHandler(IDocumentSession session)
        {
            Session = session;
        }

        public SchemaUpdatedHandler(IDocumentSession session, IDbCommandFormatter dbCommandFormatter, IDbOutput dbOutput)
        {
            Session = session;
            DbCommandFormatter = dbCommandFormatter;
            DbOutput = dbOutput;
        }

        /// <summary>
        /// A record has been updated
        /// </summary>
        /// <param name="message"></param>
        public void Handle(SoftModelSchemaUpdatedEvent @event)
        {
            var recordDescriptor = @event.Target;

            using (var transaction = new TransactionScope())
            {
                @event.SchemaChanges.ToList().ForEach(change => {
                    switch (change.SchemaChangeType)
                    {
                        case SchemaChangeType.AddField:
                            AddField(recordDescriptor, change);
                            break;

                        case SchemaChangeType.DisableField:
                            DisableField(recordDescriptor, change);
                            break;

                        case SchemaChangeType.RenameField: // label
                            RenameField(recordDescriptor, change);
                            break;
                    }
                });

                // create audit record
                DbOutput.CreateAuditRecord(@event);

                transaction.Complete();
            }
        }

        private void RenameField(RecordDescriptor recordDescriptor, SchemaChange change)
        {
            var changeSql = DbCommandFormatter.GetRenameColumnStatement(recordDescriptor.Name, change.OldName, change.NewName); 
        }

        private void DisableField(RecordDescriptor recordDescriptor, SchemaChange change)
        {
            var changeSql = DbCommandFormatter.GetDisableColumnStatement(recordDescriptor.Name, change.DisabledFieldName);
        }

        private void AddField(RecordDescriptor recordDescriptor, SchemaChange change)
        {
            string changeSql = "";

            // todo. check if relationship column
            if (change.NewField.RequiresTableJoins())
            {
                var right = Session.Load<RecordDescriptor>(change.NewField.RelatedRecordDescriptorId);
                // what we will require to gen a steatement like 
                changeSql = DbCommandFormatter.GetCreateJoinTableStatement(recordDescriptor, right);
            }
            else
            {
                // get sql for change
                changeSql = DbCommandFormatter.GetAddColumnStatement(recordDescriptor.Name, change.NewField);
            }

            Log.DebugFormat("Executing add field statement {0}", changeSql);
            DbOutput.ExecuteNonQuery(changeSql);
        }
    }

    public static class FormatterExtensions
    {
        public static bool RequiresTableJoins(this FieldDescriptor subject)
        {
            return subject.FriendlyType == FriendlyTypeName.OneToMany ||
                subject.FriendlyType == FriendlyTypeName.ManyToMany;
        }
    }
}

﻿using NServiceBus;
using NServiceBus.Logging;
using Raven.Client;
using SoftModel.Expressions;
using SoftModel.Integration.Etl.Formatter;
using SoftModel.Integration.Etl.Output;
using SoftModel.Messages;
using System.Transactions;

namespace SoftModel.Integration.Handlers
{
    /// <summary>
    /// Push changes from the operational store to a relational store.
    /// </summary>
    public class SchemaCreatedHandler : IHandleMessages<SoftModelSchemaCreatedEvent>
    {
        protected static ILog Log = LogManager.GetLogger("");

        public IDocumentSession Session { get; set; }
        public IDbCommandFormatter DbCommandFormatter { get; set; }
        public IDbOutput DbOutput { get; set; }

        public SchemaCreatedHandler(IDocumentSession session)
        {
            Session = session;          
        }

        public SchemaCreatedHandler(IDocumentSession session, IDbCommandFormatter dbCommandFormatter, IDbOutput dbOutput)
        {
            Session = session;
            DbCommandFormatter = dbCommandFormatter;
            DbOutput = dbOutput;
        }

        /// <summary>
        /// A record has been created, begin our "ETL" to the read model
        /// </summary>
        /// <param name="message"></param>
        public void Handle(SoftModelSchemaCreatedEvent @event)
        {
            // generate output statements
            var output = DbCommandFormatter.GetCreateTableStatement(@event.Target);
            
            using (var transaction = new TransactionScope())
            {
                // execute them
                Log.Debug(string.Format("Executing schema create statement {0}", output));
                DbOutput.ExecuteNonQuery(output);

                // create audit record
                DbOutput.CreateAuditRecord(@event);

                transaction.Complete();
            }
        }
    }
}

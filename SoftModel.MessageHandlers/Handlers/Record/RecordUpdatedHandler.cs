﻿using NServiceBus;
using Raven.Client;
using SoftModel.Integration.Etl.Formatter;
using SoftModel.Integration.Etl.Output;
using SoftModel.Messages;
using System.Transactions;

namespace SoftModel.Integration.Handlers
{
    /// <summary>
    /// Push changes from the operational store to a relational store.
    /// </summary>
    public class RecordUpdatedHandler : IHandleMessages<SoftModelRecordUpdated>
    {
        public IDocumentSession Session { get; set; }
        public IDbCommandFormatter DbCommandFormatter { get; set; }
        public IDbOutput DbOutput { get; set; }
        private readonly INotification notification;
        public IBus Bus { get; set; }

        public RecordUpdatedHandler(IDocumentSession session)
        {
            Session = session;
        }

        public RecordUpdatedHandler(IDocumentSession session, IDbCommandFormatter dbCommandFormatter, IDbOutput dbOutput, INotification notification)
        {
            Session = session;
            DbCommandFormatter = dbCommandFormatter;
            DbOutput = dbOutput;
            this.notification = notification;
        }

        /// <summary>
        /// A record has been updated, begin our "ETL" to the read model
        /// </summary>
        /// <param name="message"></param>
        public void Handle(SoftModelRecordUpdated @event)
        {
            // generate output statements
            var output = DbCommandFormatter.GetInsertStatement(@event.Target);

            using (var transaction = new TransactionScope())
            {
                // execute them
                DbOutput.ExecuteNonQuery(output);

                // TODO. Updates are inserts and need the same parent link stuff as insert.

                // create audit record
                DbOutput.CreateAuditRecord(@event);
               
                transaction.Complete();
            }

            notification.Send(new { RecordDescriptorName = @event.Target.RecordDescriptorRef.RecordDescriptorName, ItemId = @event.Target.Id });
            // enable integration scenarios
            Bus.Publish(new SoftModelStateChangeEvent { OriginatingEvent = @event });
        }
    }
}

﻿using NServiceBus;
using NServiceBus.Logging;
using Raven.Client;
using SoftModel.Expressions;
using SoftModel.Integration.Etl.Formatter;
using SoftModel.Integration.Etl.Output;
using SoftModel.Messages;
using SoftModel.Schema;
using System;
using System.Transactions;

namespace SoftModel.Integration.Handlers
{
    /// <summary>
    /// Push changes from the operational store to a relational store.
    /// </summary>
    public class RecordCreatedHandler : IHandleMessages<SoftModelRecordCreated>
    {
        protected static ILog Log = LogManager.GetLogger("");

        public IDocumentSession Session { get; set; }
        public IDbCommandFormatter DbCommandFormatter { get; set; }
        public IDbOutput DbOutput { get; set; }
        public IBus Bus { get; set; }

        private readonly INotification notification;
        
        public RecordCreatedHandler(IDocumentSession session)
        {
            Session = session;          
        }

        public RecordCreatedHandler(IDocumentSession session, IDbCommandFormatter dbCommandFormatter, IDbOutput dbOutput, INotification notification)
        {
            Session = session;
            DbCommandFormatter = dbCommandFormatter;
            DbOutput = dbOutput;
            this.notification = notification;
        }

        /// <summary>
        /// A record has been created, begin our "ETL" to the read model.
        /// 
        /// TODO. This and the update event are 99% similar :-) Base class.
        /// </summary>
        /// <param name="message"></param>
        public void Handle(SoftModelRecordCreated @event)
        {
            // ensure child records can locate a parent
            if (@event.IsChild)
            {
                int parentId;
                if (GetParentId(@event.Target, out parentId))
                {
                    Bus.Defer(TimeSpan.FromSeconds(5), @event);
                }
            }

            // generate output statements
            var output = DbCommandFormatter.GetInsertStatement(@event.Target);

            using (var transaction = new TransactionScope())
            {
                // execute them
                Log.DebugFormat("Inserting child with {0}", output);
                var id = DbOutput.ExecuteScalar(output);

                if (@event.IsChild)
                {
                    Log.Debug("Linking child record...");
                    LinkToParent(id, @event.Target);
                }

                // create audit record
                DbOutput.CreateAuditRecord(@event);

                transaction.Complete();
            }

            notification.Send(new { RecordDescriptorName = @event.Target.RecordDescriptorRef.RecordDescriptorName, ItemId = @event.Target.Id });

            try
            {
                // enable integration scenarios
                Bus.Publish(new SoftModelStateChangeEvent { OriginatingEvent = @event });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool GetParentId(Record child, out int parentId)
        {
            var findParentStatement = DbCommandFormatter.GetFindParentStatement(child);
            Log.DebugFormat("Finding parent with {0}", findParentStatement);

            parentId = -1;
            try
            {
                parentId = DbOutput.ExecuteScalar(findParentStatement);
                return true;
            }
            catch
            {
                // trigger a rety if somehow the parent cannot be located.
                return false;
            }
        }

        private bool LinkToParent(int id, Record child)
        {
            // Find parent id
            int parentId;
            if (GetParentId(child, out parentId))
            {
                // Generate link table insert command
                var linkRecordsStatement = DbCommandFormatter.GetLinkRecordsStatement(id, parentId, child);
                DbOutput.ExecuteNonQuery(linkRecordsStatement);
                return true;
            }

            return false;
        }
    }
}

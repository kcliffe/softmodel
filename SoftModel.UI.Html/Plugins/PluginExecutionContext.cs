﻿using log4net;
using Raven.Client;

namespace SoftModel.UI.Html.Plugins
{
    public sealed class PluginExecutionContext<T>
    {
        public int CallDepth { get; private set; }

        public T TargetBefore { get; private set; }

        public T TargetAfter { get; private set; }

        //public ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        // TODO. Exposing session lets them save without going through the pipeline.
        // Eventually we need to expose a compoment that just exposes the API
        public IDocumentSession Session { get; private set; }

        public PluginExecutionContext(T targetBefore, IDocumentSession session)
        {
            TargetBefore = targetBefore;
            Session = session;
        }

        public PluginExecutionContext(T targetBefore, T targetAfter, IDocumentSession session) 
            : this(targetBefore, session)
        {
            TargetAfter = targetAfter;
        }
    }
}
﻿using SoftModel.Schema;
using System;
using System.Linq;

namespace SoftModel.UI.Html.Plugins.System_Plugins
{
    /// <summary>
    /// Ensure all record values conform - currently mainly for date handling where JSON and the .net serializer
    /// don't automatically deserialize to DateTime. We also want to ensure dates have a valid Time portion and that they
    /// are ISO8601.
    /// </summary>
    public class RecordFormattingPlugin
    {
        public bool Handle(PluginExecutionContext<Record> context)
        {
            var before = context.TargetBefore;
            var recordDescriptor = context.Session.Load<RecordDescriptor>(before.RecordDescriptorRef.RecordDescriptorId);

            var dateFields = before
                .Fields
                .Where(f => recordDescriptor.GetFieldDescriptor(f.FieldName).FriendlyType == FriendlyTypeName.Date).ToList();

            dateFields.ForEach(x =>
            {
                if (x.Value != null)
                {
                    DateTime temp;
                    if (x.Value is string)
                    {
                        if (DateTime.TryParse(x.Value, out temp))
                        {
                            if (temp.TimeOfDay == TimeSpan.Zero)
                            {
                                var now = DateTime.Now.TimeOfDay;
                                x.Value = new DateTime(temp.Year, temp.Month, temp.Day, now.Hours, now.Minutes, now.Seconds).ToString();

                                context.Log.DebugFormat("{0} set to {1}", x.FieldName, x.Value);
                            }
                        }
                    }
                    else
                    {
                        // todo .. datetime handling
                    }
                }
            });
            
            return true;
        }
    }
}

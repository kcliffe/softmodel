﻿using SoftModel.Schema;
using System.Linq;

namespace SoftModel.UI.Html.Plugins.System_Plugins
{
    public class CreateDefaultFormDefinitionPlugin
    {
        public bool Handle(PluginExecutionContext<RecordDescriptor> context)
        {
            var before = context.TargetBefore;

            var defaultFormDefNameGivenRecordDescriptorName = before.Name + "_default";
            var existingFormDefinition = context.Session
                .Query<FormDefinition>()
                .Where(fd => fd.Name == defaultFormDefNameGivenRecordDescriptorName).FirstOrDefault();

            if (existingFormDefinition == null)
            {
                // Create new default form def
                var formDefinition = new FormDefinition
                {
                    Id = "FormDefinitions/" + defaultFormDefNameGivenRecordDescriptorName,
                    Name = defaultFormDefNameGivenRecordDescriptorName,
                    RecordDescriptorReference = new RecordDescriptorRef { 
                        RecordDescriptorId = before.Id, 
                        RecordDescriptorName = before.Name 
                    }
                };

                context.Session.Store(formDefinition);

                context.Log.Info(string.Format("Default form definition created for schema {0}.", before.Name));
            }

            return true;
        }
    }
}

﻿using SoftModel.Expressions;
using SoftModel.Schema;

namespace SoftModel.UI.Html.Plugins.System_Plugins
{
    public class EvaluateRulesPlugin
    {                
        public bool Handle(PluginExecutionContext<Record> context)
        {
            context.Log.DebugFormat("Evaluating expressions for schema {0}",
                context.TargetBefore.RecordDescriptorRef.RecordDescriptorName);

            var expressionEvaluator = new ExpressionEvaluator(context.Session);
            expressionEvaluator.Evaluate(new ExpressionRecordProxy(context.TargetBefore));

            return true;
        }
    }
}
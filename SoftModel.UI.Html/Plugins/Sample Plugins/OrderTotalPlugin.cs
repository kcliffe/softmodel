﻿using System;
using System.Linq;
using System.Net.Http;
using SoftModel.Messages;
using System.Collections.Generic;
using SoftModel.Schema;

namespace SoftModel.UI.Html.Plugins.Sample_Plugins
{
    /// <summary>
    /// Just a trivial example of a plugin to update the total of an order when an order line is posted
    /// </summary>
    public class OrderTotalPlugin // : IHandleBeforeSave<Record>, IHandleAfterSave<Record>
    {
        public bool Handle(PluginExecutionContext<Record> context)
        {
            try
            {
                var record = context.TargetBefore;

                if (record.RecordDescriptorRef.RecordDescriptorName != "Order")
                    return true;

                // 1 Access to fields via .Where. YUCK.
                // need a nice wrapper FieldValueAs<T>();
                var orderlinesFieldValue = record.FieldValue("OrderLines");
                List<string> orderlines = orderlinesFieldValue != null && 
                    (orderlinesFieldValue.Value as List<string>) != null
                    ? orderlinesFieldValue.Value as List<string>
                    : new List<string>();

                var totalPrice = 0.0M;
                if (orderlines.Any())
                {
                    // todo, get session, sum orderlines
                }

                if (record.HasFieldValue("TotalPrice"))
                {
                    // TODO. An alternative - default values for fields based on type set in CTOR 
                    record.FieldValue("TotalPrice").Value = totalPrice;
                }
                else
                { 
                    record.Fields.Add(new FieldValue
                    {
                        FieldName = "TotalPrice",
                        Value = totalPrice
                    });
                }

                return true;
            }
            catch (Exception ex)
            {
                context.Log.ErrorFormat("{0} > {1}", Properties.Resources.error_plugin_execution, ex.Message);

                // in this case prevent the orderline being saved if there is an exception updating the order
                return false;
            }
        }
    }
}
﻿namespace SoftModel.UI.Html.Plugins
{
    public static class ServiceBusFactory
    {
        public static IMessageQueue MessageQueue { get; private set; }

        public static void SetQueue(IMessageQueue messageQueueIn)
        {
            MessageQueue = messageQueueIn;
        }
    }    
}
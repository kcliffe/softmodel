﻿using System;
using SoftModel.Messages;

namespace SoftModel.UI.Html.Plugins
{
    //public enum Execute
    //{
    //    Synchronously,
    //    Asynchronously
    //}

    public enum ExecuteWhen
    {
        BeforeSave,
        AfterSave
    }

    public class EventRegistration<T> : IEventRegistration where T : SoftModelEventBase
    {
        public EventRegistration()
        {
            //Execution = executionMode;
            When = Plugins.ExecuteWhen.AfterSave;
        }

        public EventRegistration(ExecuteWhen when, Type handlerType)
        {
            // todo. validate execution mode - no sync if When is before.
            // Execution = executionMode;
            HandlerType = handlerType;
            When = when;
        }

        //public Execute Execution { get; set; }

        public ExecuteWhen When { get; set; }

        public Type HandlerType { get; set; }
    }
        
}
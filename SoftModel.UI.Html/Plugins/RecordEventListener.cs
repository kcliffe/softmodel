﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using Newtonsoft.Json;
using SoftModel.Messages;
using Raven.Client;
using System.Diagnostics;
using Autofac;
using SoftModel.Expressions;
using System.Web.Mvc;
using NServiceBus;
using log4net;

namespace SoftModel.UI.Html.Plugins
{
    /// <summary>
    /// Provides an opportunity to notify components when a command or an event occurs during processing.
    /// Notification can be synchronous (which can prevent saves from occuring if they fail or return false) or asynchronous.
    /// 
    /// TODO:
    /// - Exit on exception? (Before events)
    /// - Refactor - this class is nowhere near as DRY as it should be
    /// </summary>
    public class RecordEventListener
    {
        private readonly List<KeyValuePair<Type, EventRegistration<SoftModelEventBase>>> LocalSyncHandlers;
        private readonly List<KeyValuePair<Type, EventRegistration<SoftModelEventBase>>> LocalAsyncHandlers;
        private static IBus bus;
        private const string destQueue = "softmodel.integration";

        private static ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public RecordEventListener()
        {
            LocalSyncHandlers = new List<KeyValuePair<Type, EventRegistration<SoftModelEventBase>>>();
            LocalAsyncHandlers = new List<KeyValuePair<Type, EventRegistration<SoftModelEventBase>>>();

            if (bus == null)
            {
                Configure.Serialization.Json();
                bus = Configure.With()
                .DefaultBuilder()
                .DefiningMessagesAs(msg => msg.Namespace == "SoftModel.Messages")
                .UseTransport<Msmq>()
                .UnicastBus()
                .SendOnly();
            }
        }

        /// <summary>
        /// Register an event to be passed through the async pipeline
        /// </summary>
        /// <typeparam name="TEventType"></typeparam>
        public void RegisterAsync<TEventType>()
        {
            var eventType = typeof(TEventType);

            if (LocalAsyncHandlers.Any(x => x.Key == eventType))
            {
                Log.Debug(string.Format("Async handler exists for type {0}", typeof(TEventType)));
                return;
            }

            Log.Debug(string.Format("Registering async handler for type {0}", typeof(TEventType)));

            var eventRegistration = new EventRegistration<SoftModelEventBase>();
            LocalAsyncHandlers.Add(new KeyValuePair<Type, EventRegistration<SoftModelEventBase>>(eventType, eventRegistration));
        }

        /// <summary>
        /// Register an event to be execute synchronously
        /// </summary>
        /// <typeparam name="TEventType"></typeparam>
        /// <typeparam name="THandlerType"></typeparam>
        /// <param name="executeWhen"></param>
        public void RegisterSync<TEventType, THandlerType>(ExecuteWhen executeWhen)
        {
            var eventType = typeof(TEventType);

            Log.Debug(string.Format("Registering sync handler {1} for type {0}", eventType, typeof(THandlerType)));

            var eventRegistration = new EventRegistration<SoftModelEventBase>(executeWhen, typeof(THandlerType));
            LocalSyncHandlers.Add(new KeyValuePair<Type, EventRegistration<SoftModelEventBase>>(eventType, eventRegistration));
        }

        /// <summary>
        /// "Publish" a RecordEvent. 
        /// </summary>
        /// <param name="targetBefore"></param>
        /// <param name="session"></param>
        /// <param name="currentUser"></param>
        public bool NotifySyncBefore<T>(PluginExecutionContext<T> pluginExecutionContext, string currentUser = "Anonymous")
        {
            Log.Info("Executing synchronous before create notifications");

            var eventRegistration = LocalSyncHandlers
                .Where(x => x.Key == pluginExecutionContext.TargetBefore.GetType() && x.Value.When == ExecuteWhen.BeforeSave)
                .Select(x => x.Value);

            bool allHandlersExecuted = true;
            if (eventRegistration.Any())
            {
                try
                {
                    foreach (var plugin in eventRegistration)
                    {
                        // Execute synchronously. Returning false during a pre should prevent the event from occuring in theory.

                        // HACK. Was wanting to resolve the plugin (and it's dependancies) from the 
                        // DI container. However the WebAPi seems to want to get in the way.
                        // Closeset i get to a solution is:
                        // http://stackoverflow.com/questions/11123732/asp-net-4-web-api-rc-autofac-manual-resolving

                        // For now, all plugins must have a parameterless CTOR. We'll add depedancies to the 
                        // plugin execution conext.

                        dynamic handlerInstance = Activator.CreateInstance(plugin.HandlerType);

                        Log.InfoFormat("Executing handler of type {0} before create.", plugin.HandlerType);
                        if (!handlerInstance.Handle(pluginExecutionContext))
                        {
                            allHandlersExecuted = false;
                            Log.Warn("{0} returned false - no more plugins will be executed");
                            break;
                        }
                    }

                    return allHandlersExecuted;
                }
                catch (Exception ex)
                {
                    Log.Error("Plugin exception occurred", ex);
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// "Publish" a RecordEvent. 
        /// </summary>
        /// <param name="targetBefore"></param>
        /// <param name="session"></param>
        /// <param name="currentUser"></param>
        public bool NotifySyncAfter<T>(PluginExecutionContext<T> pluginExecutionContext, string currentUser = "Anonymous")
        {
            var eventRegistration = LocalSyncHandlers
                .Where(x => x.Key == pluginExecutionContext.TargetBefore.GetType() && x.Value.When == ExecuteWhen.AfterSave)
                .Select(x => x.Value);

            if (eventRegistration != null)
            {
                try
                {
                    bool allHandlersExecuted = true;

                    foreach (var plugin in eventRegistration)
                    {
                        // Execute synchronously. Returning false during a pre should prevent the event from occuring in theory.
                        dynamic handlerInstance = Activator.CreateInstance(plugin.HandlerType);

                        Log.InfoFormat("Executing handler of type {0} after create.", plugin.HandlerType);
                        if (!handlerInstance.Handle(pluginExecutionContext))
                        {
                            allHandlersExecuted = false;
                            Log.Warn("{0} returned false - no more plugins will be executed");
                            break;
                        }
                    }

                    return allHandlersExecuted;
                }
                catch (Exception ex)
                {
                    Log.Error("Plugin exception occurred", ex);
                    return false;
                }
            }

            return true;
        }

        public void NotifyAsync<T>(T target)
        {
            var eventRegistration = LocalAsyncHandlers
               .Where(x => x.Key == target.GetType())
               .Select(x => x.Value).FirstOrDefault();

            // async, send to the service buS
            bus.Send(destQueue, target);
        }
    }
}
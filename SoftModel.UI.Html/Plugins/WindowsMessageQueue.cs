﻿using System.Messaging;
using Newtonsoft.Json;

namespace SoftModel.UI.Html.Plugins
{
    public class WindowsMessageQueue : IMessageQueue
    {
        private readonly MessageQueue messageQueue;

        public WindowsMessageQueue(MessageQueue messageQueue)
        {
            this.messageQueue = messageQueue;            
        }

        public void Send(object o)
        {            
            // Use Json to avoid having to use [Serializable] everywhere.
            // However it seems we can't just assign a custom formatter like JsonMessageFormatter
            // as there is an issue with Message.Acknowledge
            //messageQueue.Send(JsonConvert.SerializeObject(o));
            messageQueue.Send(o);
        }
    }
}
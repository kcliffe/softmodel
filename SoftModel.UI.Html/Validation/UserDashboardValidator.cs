﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentValidation;
using Raven.Client;
using SoftModel.Schema;
using SoftModel.UI.Html.ViewModels.UI;

namespace SoftModel.UI.Html.Validation
{
    public class UserDashboardValidator : AbstractValidator<UserDashboard> 
    {
        IDocumentSession documentSession;

        public UserDashboardValidator(IDocumentSession documentSession)
        {
            this.documentSession = documentSession;

            RuleFor(userDashboard => userDashboard.Views).Must(HaveAtLeastOneView);                        
        }

        private bool HaveAtLeastOneView(IList<DashboardView> dashboardViews)
        {
            return dashboardViews != null && dashboardViews.Any();             
        }
    }
}
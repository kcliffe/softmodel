﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentValidation;
using Raven.Client;
using SoftModel.Schema;
using SoftModel.UI.Html.Properties;

namespace SoftModel.UI.Html.Validation
{
    public class RecordSchemaValidator : AbstractValidator<RecordDescriptor> 
    {
        private readonly IDocumentSession documentSession;
        private IList<string> invalidFields;
        public string InvalidFields
        {
            get
            {
                return invalidFields == null || !invalidFields.Any() ? "" : string.Join(",", invalidFields);
            }
        }

        public RecordSchemaValidator(IDocumentSession documentSession)
        {
            this.documentSession = documentSession;

            RuleFor(recordSchema => recordSchema.Name).NotEmpty();
            RuleFor(recordSchema => recordSchema.Label).NotEmpty();
            RuleFor(recordSchema => recordSchema.FieldDescriptors).Must(HaveAtLeastOneField);
            RuleFor(recordSchema => recordSchema.FieldDescriptors).Must(HaveAllFieldsSpecifyValidTypes).WithLocalizedMessage(
                () => Resources.error_record_schema_has_invalid_field_type, InvalidFields);
        }

        private bool HaveAtLeastOneField(List<FieldDescriptor> fieldDefinitions)
        {
            return fieldDefinitions != null && fieldDefinitions.Any();             
        }

        private bool HaveAllFieldsSpecifyValidTypes(List<FieldDescriptor> fieldDefinitions)
        {
            invalidFields = new List<string>();

            foreach(var fieldDef in fieldDefinitions)
            {
                if (Global.FieldTypes.FirstOrDefault(ft => ft.FriendlyTypeName == fieldDef.FriendlyType) == null)
                {
                    invalidFields.Add(fieldDef.Name);
                }
            }

            return !invalidFields.Any();     
        }
    }
}
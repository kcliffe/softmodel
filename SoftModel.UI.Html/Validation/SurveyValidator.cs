﻿using FluentValidation;
using Raven.Client;
using SoftModel.Schema.Survey;

namespace SoftModel.UI.Html.Validation
{
    public class SurveyValidator : AbstractValidator<Survey>
    {
        IDocumentSession documentSession;

        public SurveyValidator(IDocumentSession documentSession)
        {
            this.documentSession = documentSession;
            ValidatorOptions.CascadeMode = CascadeMode.StopOnFirstFailure;
        }
    }
}

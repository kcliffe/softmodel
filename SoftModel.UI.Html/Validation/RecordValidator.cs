﻿using FluentValidation;
using Raven.Client;
using SoftModel.Schema;
using System.Collections.Generic;
using System.Linq;

namespace SoftModel.UI.Html.Validation
{
    public class RecordValidator : AbstractValidator<Record> 
    {
        IDocumentSession documentSession;

        public RecordValidator(IDocumentSession documentSession)
        {
            this.documentSession = documentSession;
            ValidatorOptions.CascadeMode = CascadeMode.StopOnFirstFailure;

            RuleFor(record => record.Fields).Must(HaveAtLeastOneField);          
            // RuleFor(record => record.Fields).Must(AllFieldValuesHaveValidTypes);
            RuleFor(record => record.RecordDescriptorRef).NotNull()
                .WithLocalizedMessage(() => Properties.Resources.error_new_record_missing_recorddescriptorref);

            // TODO. Ensure record.Fields all exist against RecordSchemaDescriptor
        }

        private bool HaveAtLeastOneField(IList<FieldValue> fieldValues)
        {
            return fieldValues != null && fieldValues.Any();
        }

        // TODO. This check is crap. What we want to check is that FieldValues are valid against the RecordSchemaDescriptor
        //private bool AllFieldValuesHaveValidTypes(IList<FieldValue> fieldValues)
        //{
        //    if (fieldValues == null || !fieldValues.Any()) return true;

        //    bool allValid = true;

        //    foreach (var fieldValue in fieldValues)
        //    {                
        //        var fieldDef = Global.FieldTypes.Where(ft => ft.FriendlyTypeName.Equals(fieldValue.FieldDescriptorRef.FriendlyType));
        //        if (fieldDef == null)
        //        {
        //            allValid = false;
        //            break;
        //        }
        //    }

        //    return allValid;
        //}
    }
}
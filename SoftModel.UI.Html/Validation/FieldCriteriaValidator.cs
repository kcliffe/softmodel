﻿using FluentValidation;
using SoftModel.UI.Html.Properties;

namespace SoftModel.UI.Html.Controllers
{
    public class FieldCriteriaValidator : AbstractValidator<FieldCriteria> 
    {
        public FieldCriteriaValidator()
        {            
            RuleFor(filterCriteria => filterCriteria.RecordDescriptorName).NotEmpty().WithLocalizedMessage(() => 
                Resources.error_fieldfilter_must_have_rdname);
        }
    }
}

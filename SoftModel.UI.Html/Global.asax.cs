﻿using Autofac;
using Autofac.Integration.WebApi;
using log4net;
using Raven.Client;
using Raven.Client.Document;
using Raven.Client.Indexes;
using SoftModel.Messages;
using SoftModel.Query;
using SoftModel.Query.Audit;
using SoftModel.Schema;
using SoftModel.UI.Html.Domain;
using SoftModel.UI.Html.Domain.Store;
using SoftModel.UI.Html.Indexes;
using SoftModel.UI.Html.Plugins;
using SoftModel.UI.Html.Plugins.System_Plugins;
using System.Configuration;
using System.Messaging;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Validation;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace SoftModel.UI.Html
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        public static IDocumentStore DocumentStore;
        private static ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static ContainerBuilder builder;
        
        public static Autofac.IContainer container;

        protected void Application_Start()
        {
            Log.Debug("Executing application startup");

            // Signalr routing
            RouteTable.Routes.MapHubs();

            // AreaRegistration.RegisterAllAreas();
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // Required as we have polymorphic types in messages
            HttpConfiguration config = GlobalConfiguration.Configuration;
            
            // Hitch up the logging wagon
            // GlobalConfiguration.Configuration.EnableSystemDiagnosticsTracing();

            ConfigureIOC();

            InitializePlugins();

            GlobalConfiguration.Configuration.MessageHandlers.Add(
                new ApiKeyHeaderHandler(DocumentStore.OpenSession(ConfigurationManager.AppSettings["tennantName"])));

            // Prevent default model validator from firing (it can't handle generics and we do our own validation)
            GlobalConfiguration.Configuration.Services.Clear(typeof(IBodyModelValidator));
        }

        /// <summary>
        /// Initialise the IOC container (autofac)
        /// </summary>
        protected void ConfigureIOC()
        {
            // Create the container builder.
            builder = new ContainerBuilder();

            // Register the Web API controllers.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            BootstrapIOC();

            // Build the container.
            container = builder.Build();
            
            // Create the dependency resolver.
            var resolver = new AutofacWebApiDependencyResolver(container);
            GlobalConfiguration.Configuration.DependencyResolver = resolver;
        }

        // Apply claims transformation
        protected void Application_PostAuthenticateRequest()
        {
            //if (ClaimsPrincipal.Current.Identity.IsAuthenticated)
            //{
            //    var principal = new ClaimsTransformer().Authenticate(string.Empty, ClaimsPrincipal.Current);

            //    HttpContext.Current.User = principal;
            //    Thread.CurrentPrincipal = principal;
            //}
        }

        private static void InitializePlugins()
        {
            var recordEventListener = container.Resolve<RecordEventListener>();

            // Publish asynchronously to ETL when records are created
            Log.Info("Registering asynchronous plugins");
            recordEventListener.RegisterAsync<SoftModelRecordCreated>();
            recordEventListener.RegisterAsync<SoftModelRecordUpdated>();
            recordEventListener.RegisterAsync<SoftModelRecordDisabled>();
            recordEventListener.RegisterAsync<SoftModelSchemaCreatedEvent>();

            // Synchronous handlers
            // TODO -> Bitflag ExecuteWhen so we can mix and match e.g. BeforeCreate | BeforeUpdate
            // OR 
            // handlers implement explicit interface (multiple handlers per class)
            // e.g. Handler : IBeforeCreate, IBeforeUpdate
            Log.Info("Registering synchronous plugins");
            // recordEventListener.RegisterSync<Record, OrderTotalPlugin>(ExecuteWhen.BeforeSave);
            // recordEventListener.RegisterSync<RecordDescriptor, CreateDefaultFormDefinitionPlugin>(ExecuteWhen.BeforeSave);
            recordEventListener.RegisterSync<Record, RecordFormattingPlugin>(ExecuteWhen.BeforeSave);
            recordEventListener.RegisterSync<Record, EvaluateRulesPlugin>(ExecuteWhen.BeforeSave);
        }

        /// <summary>
        /// Init RavenDb
        /// </summary>
        /// <returns></returns>
        public static void BootstrapIOC()
        {
            Log.Info("Initializing document store");

            DocumentStore = new DocumentStore
            {
                Url = ConfigurationManager.AppSettings["operationStoreUrl"],
                DefaultDatabase = "SoftModel"
            }
            .Initialize();

            // Make REST friendly id's
            DocumentStore.Conventions.IdentityPartsSeparator = "-";
            // Only required while the SchemaController hack is in place
            DocumentStore.Conventions.AllowQueriesOnId = true;
            ((DocumentStoreBase)DocumentStore).RegisterListener(new OptimisticConcurrencyDocumentStoreListener());
            ((DocumentStoreBase)DocumentStore).RegisterListener(new OptimisticConcurrencyConversionListener());

            // Initialize indexes
            IndexCreation.CreateIndexes(typeof(Schema_Search).Assembly, DocumentStore);

            builder.Register<IDocumentStore>(c => DocumentStore).SingleInstance();
            builder.Register<IDocumentSession>(c => DocumentStore.OpenSession(ConfigurationManager.AppSettings["tennantName"])).As<IDocumentSession>().InstancePerApiRequest();
            builder.RegisterType<PostGreQuery>().As<IQuery>();
            builder.RegisterType<PostGreAuditQuery>().As<IAuditQuery>();
            builder.RegisterType<RecordEventListener>().SingleInstance();

            // Register plugins so the IOC can instantiate them and provide dependancy resolution
            builder.RegisterAssemblyTypes(typeof(WebApiApplication).Assembly)
                .Where(t => t.Name.EndsWith("Plugin")).AsSelf();           
        }
    }
}
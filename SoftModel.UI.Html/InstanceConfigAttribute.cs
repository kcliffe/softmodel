﻿using System;
using System.Net.Http.Formatting;
using System.Web.Http.Controllers;
using System.Web.Http.Validation;
using Newtonsoft.Json;

namespace SoftModel.UI.Html
{
    /// <summary>
    /// Custom configuration for the Instance contoller
    /// </summary>
    public class InstanceControllerConfigAttribute : Attribute, IControllerConfiguration
    {
        public void Initialize(HttpControllerSettings controllerSettings, HttpControllerDescriptor controllerDescriptor)
        {
            // Remove the model validation - it fails because of our dynamic properties - we also
            // perform our own validation
            controllerSettings.Services.Clear(typeof(IBodyModelValidator));

            // ControllerSettings.Formatters is a cloned list of formatters that are present on the GlobalConfiguration            
            // remove the default Json formatter
            controllerSettings.Formatters.Remove(controllerSettings.Formatters.JsonFormatter);

            // Add our overridden Json formatter which reads embedded .net type info to aid deserialization
            controllerSettings.Formatters.Insert(0, new JsonMediaTypeFormatter()
            {
                SerializerSettings = new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Objects
                }
            });
        }
    }
}
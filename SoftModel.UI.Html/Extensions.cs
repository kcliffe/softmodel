﻿using SoftModel.Schema;
using SoftModel.UI.Html.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoftModel.UI.Html
{
    public static class Extensions
    {
        public static string ToRavenIdFromQs(this string qsId)
        {
            return qsId.Replace('-', '/');
        }

        public static object GetDefaultValue(this Type t, string firendlyType)
        {
            // TODO. RecordRef, Enumeration. These should typically be required? 
            // No actually the RecordRef or List<RecordRef> wont be... 

            if (t == null || !t.IsValueType || Nullable.GetUnderlyingType(t) != null)
                return null;

            return Activator.CreateInstance(t);
        }

        public static void Localize(this RecordDescriptor recordDescriptor, LanguageResource languageResource)
        {
            foreach (var lang in languageResource.Entries)
            {
                // Optimise with Contains and custom comparer
                var field = recordDescriptor.FieldDescriptors.FirstOrDefault(x => x.Name == lang.Key);
                if (field != null)
                {
                    field.Label = lang.Value;
                }
            }
        }
    }
}
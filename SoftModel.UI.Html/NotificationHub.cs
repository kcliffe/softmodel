﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace SoftModel.UI.Html
{
    [HubName("notification")]
    public class NotificationHub : Hub
    {
    }
}
﻿using log4net;
using Omu.ValueInjecter;
using Raven.Client;
using Raven.Imports.Newtonsoft.Json;
using SoftModel.Schema;
using SoftModel.Schema.Expressions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SoftModel.UI.Html.Domain
{
    /// <summary>
    /// Utility methods for importing and exporting RecordDescriptors and associated entities.
    /// </summary>
    public class ImportExport
    {
        private static ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IDocumentSession documentSession;

        public ImportExport(IDocumentSession documentSession)
        {
            this.documentSession = documentSession;
        }

        /// <summary>
        /// Generate the export content given the aggregate root is the RecordDescriptor.
        /// </summary>
        /// <remarks>
        /// This is not the "simplest" implementation we could derive. Given that we are using a document database
        /// we could simple store all of the classes related to the aggregate root in a single document. This
        /// would reduce the method essentially to a single document retrieval operation.
        /// 
        /// However the document design for the application as a whole means that the forms and rulesets are 
        /// currently better off in their own collections linked via 1-many relationships in the RecordDescriptor.
        /// 
        /// The main benefit we still see over traditional relational systems is that in the target system
        /// correlation of related entities is far simpler because there is no need to match relational Id's 
        /// to the names we export. Identifers in the document db are strings that contain the exported names!
        /// </remarks>
        /// <param name="recordDescriptorNames"></param>
        /// <returns>The object graph that consist of the RecordDescriptors and all related entites.</returns>
        public dynamic GetRecordDescriptorsForExport(IList<string> recordDescriptorNames, out List<string> invalidNames)
        {
            invalidNames = new List<string>();
            var content = new List<ExportMeta>();

            // TODO. Raven will only allow 30 load calls per session. Paging required for more.
            foreach (var rdName in recordDescriptorNames.ToList())
            {
                var schema = documentSession.Query<RecordDescriptor>()
                    .Customize(x => x.Include<FormSet>(formSet => "FormSet/" + rdName))
                    .Customize(x => x.Include<RuleSet>(ruleSet => "RuleSet/" + rdName))
                    .Where(rd => rd.Name == rdName)
                    .FirstOrDefault();

                if (schema == null)
                {
                    invalidNames.Add(rdName);
                    continue;
                }

                var exportMeta = new ExportMeta();
                exportMeta.RecordDescriptor = schema;

                // Include Forms (there is no additional call to the Db here - these entities are downloaded
                // in RavenDb's secondary channel triggered by the .Include methods on the query above.
                exportMeta.FormSet = documentSession.Query<FormDefinition>().Where(fd => fd.RecordDescriptorReference.RecordDescriptorId == schema.Id).ToList();
                exportMeta.RuleSet = documentSession.Load<RuleSet>("RuleSet/" + schema.Name);
                content.Add(exportMeta);
            }

            return content;
        }

        /// <summary>
        /// Return a list of candidates for import which exist in the specified import path
        /// </summary>
        /// <param name="importPath"></param>
        /// <returns></returns>
        public List<ImportCandidate> GetFilesForImport(string importPath)
        {
            var importCandidates = new List<ImportCandidate>();

            var files = Directory.EnumerateFiles(importPath, "*.sme");
            foreach (var f in files)
            {
                var fileInfo = new FileInfo(f);
                importCandidates.Add(new ImportCandidate { Name = fileInfo.Name, Created = fileInfo.LastWriteTime.ToString() });
            }

            return importCandidates;
        }

        /// <summary>
        /// Import a file containing one or more RecordDescriptors and related items
        /// </summary>
        /// <param name="importPath"></param>
        /// <param name="content"></param>
        /// <param name="merge"></param>
        /// <returns></returns>
        public ImportResult Import(string importPath, string content, bool merge)
        {
            Log.InfoFormat("Importing file {0}", importPath);

            TextLog importLog = new TextLog();
            importLog.AppendFormat("Beginning import of file {0}", importPath);

            List<ExportMeta> list;
            try
            {
                list = JsonConvert.DeserializeObject<List<ExportMeta>>(content);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Failed to read import file {0}. {1}", importPath, ex.Message);
                return new ImportResult(false, importLog.Content);
            }

            foreach (var exportMeta in list)
            {
                var formSet = exportMeta.FormSet;
                var ruleSet = exportMeta.RuleSet;
                var recordDescriptor = exportMeta.RecordDescriptor;

                // Forms. These exist in both 
                foreach (var form in formSet)
                {
                    if (InsertOrUpdate(form, form.Id))
                    {
                        importLog.AppendFormat("Form {0} created (unpublished).", form.Name);
                    }
                    else
                    {
                        importLog.AppendFormat("Form {0} overwrites existing version.", form.Name);
                    }
                }

                // Ruleset. These only exists inside a ruleset. So just create or overwrite.
                if (InsertOrUpdate(ruleSet, ruleSet.Id))
                {
                    importLog.AppendFormat("Ruleset {0} created (unpublished).", recordDescriptor.Label);
                }
                else
                {
                    importLog.AppendFormat("Ruleset {0} overwrites existing version.", recordDescriptor.Label);
                }

                // Uncomment when publish function supported
                // var recordId = recordDescriptor.Id + "_unpub";

                if (InsertOrUpdate(recordDescriptor, recordDescriptor.Id))
                {
                    importLog.AppendFormat("Schema type {0} created (unpublished).", recordDescriptor.Label);
                }
                else
                {
                    importLog.AppendFormat("Schema type {0} overwrites existing version.", recordDescriptor.Label);
                }
            }

            // all operations completed with a single save operation - no need for a txn
            documentSession.SaveChanges();

            Log.InfoFormat("Sucessfully imported file {0}. Archiving.", importPath);

            return Archive(importPath, importLog) ? new ImportResult(true, importLog.Content) : new ImportResult(false, importLog.Content);
        }

        private bool InsertOrUpdate<T>(T entity, string id)
        {
            var existing = documentSession.Load<T>(id);
            if (existing == null)
            {
                documentSession.Store(entity);
                return true;
            }
            else
            {
                existing.InjectFrom(entity);
                documentSession.Store(existing);    // not strictly required. change tracking should persist on SaveChanges()
                return false;
            }
        }

        private bool Archive(string importPath, TextLog importLog)
        {
            var archivePath = Path.Combine(Path.GetDirectoryName(importPath), "archive");
            Log.DebugFormat("Archiving import file to {0}", archivePath);
            try
            {
                if (!Directory.Exists(archivePath))
                {
                    Directory.CreateDirectory(archivePath);
                }

                File.Move(importPath, archivePath);
                importLog.AppendFormat("Imported file {0} archived.", Path.GetFileName(importPath));
                return true;
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Failed to create import file archive folder at {0}. {1}", importPath, ex.Message);
                importLog.Append("Failed to create the archive folder.");
                return false;
            }
        }

        #region Support classes

        public class ExportMeta
        {
            public RecordDescriptor RecordDescriptor { get; set; }
            public List<FormDefinition> FormSet { get; set; }
            public RuleSet RuleSet { get; set; }
        }

        public class ImportCandidate
        {
            public string Name { get; set; }
            public string Created { get; set; }
        }

        public class ImportResult
        {
            public bool Ok { get; set; }
            public string Log { get; set; }

            public ImportResult(bool ok, string log)
            {
                Ok = ok;
                Log = log;
            }
        }

        #endregion
    }
}
﻿using SoftModel.Schema;

namespace SoftModel.UI.Html.Domain
{
    public class RecordHelpers
    {
        public static void AssignSchemaIdIfNotSet(SchemaBase instance, out bool idAssigned)
        {
            idAssigned = false;

            if (!instance.IsTransient)
                return;

            // generate id based on type, super complex pluralization included
            instance.Id = string.Format("{0}s/{1}", instance.GetType().Name.ToLower(), instance.Name);
            idAssigned = true;
        }
    }
}
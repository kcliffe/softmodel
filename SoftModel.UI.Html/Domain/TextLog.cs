﻿using System.Text;

namespace SoftModel.UI.Html.Domain
{
    public class TextLog
    {
        private StringBuilder builder;

        public string Content
        {
            get
            {
                return builder.ToString();
            }
        }

        public TextLog()
        {
            builder = new StringBuilder();
        }

        public void Append(string toAppend)
        {
            builder.AppendFormat("{0}{1}", toAppend, "\r\n");
        }

        public void AppendFormat(string toAppend, params string[] args)
        {
            builder.AppendFormat(toAppend, args);
            builder.Append("\r\n");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Raven.Client.Listeners;
using Raven.Json.Linq;
using SoftModel.Schema;

namespace SoftModel.UI.Html.Domain.Store
{
    public class OptimisticConcurrencyDocumentStoreListener : IDocumentStoreListener
    {
        public void AfterStore(string key, object entityInstance, RavenJObject metadata)
        {
            var schemaEntityWithETag = entityInstance as IHasETag;
            if (schemaEntityWithETag != null)
            {
                schemaEntityWithETag.Etag = metadata.Value<Guid>("@etag");
            }
        }
        
        public bool BeforeStore(string key, object entityInstance, RavenJObject metadata, RavenJObject original)
        {
            return true;
        }
    }

    public class OptimisticConcurrencyConversionListener : IDocumentConversionListener
    {
        public void DocumentToEntity(string key, object entity, RavenJObject document, RavenJObject metadata)
        {
            var schemaEntityWithETag = entity as IHasETag;
            if (schemaEntityWithETag != null)
            {
                schemaEntityWithETag.Etag = metadata.Value<Guid>("@etag");
            }
        }

        public void EntityToDocument(string key, object entity, RavenJObject document, RavenJObject metadata)
        {
            return;
        }
    }
}
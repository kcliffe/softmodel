﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Raven.Client.Listeners;
using Raven.Json.Linq;
using SoftModel.Schema;

namespace SoftModel.UI.Html.Domain.Store
{
    public class RSStoreListener : Raven.Client.Listeners.IDocumentStoreListener
    {
        public void AfterStore(string key, object entityInstance, RavenJObject metadata)
        {
            var schemaEntityWithETag = entityInstance as IHasETag;
            if (schemaEntityWithETag != null)
            {
                schemaEntityWithETag.Etag = metadata.Value<Guid>("@etag");
            }
        }
        
        public bool BeforeStore(string key, object entityInstance, RavenJObject metadata, RavenJObject original)
        {
            return true;
        }
    }    
}
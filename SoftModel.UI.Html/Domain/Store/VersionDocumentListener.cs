﻿using Raven.Client;
using Raven.Client.Listeners;
using Raven.Json.Linq;
using SoftModel.Schema;

namespace SoftModel.UI.Html.Domain.Store
{
    /// <summary>
    /// Cheap and cheerful versioning because current RavenDb versioning bundle won't install due to dll version clashes with json.net (web api)
    /// and the raven db client. I could rebuild from source, but trying not to circumvent nuget.
    /// 
    /// Currently only versioning the Record type.
    /// </summary>
    public class VersionDocumentListener : IDocumentStoreListener
    {
        private readonly IDocumentStore store;

        public VersionDocumentListener(IDocumentStore documentStore)
        {
            store = documentStore;
        }
      
        public void AfterStore(string key, object entityInstance, RavenJObject metadata)
        {
             
        }

        /// <summary>
        /// We store ONE previous version of a RECORD. 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="entityInstance"></param>
        /// <param name="metadata"></param>
        /// <param name="original"> </param>
        public bool BeforeStore(string key, object entityInstance, RavenJObject metadata, RavenJObject original)
        {
            var record = entityInstance as Record;
            if (record == null || record.IsTransient) return false;

            using (var session = store.OpenSession())
            {
                var recordHistory = session.Load<RecordHistory>("recordhistory/" + record.Id);
                if (recordHistory != null)
                {
                    recordHistory.Record = RavenJObject.FromObject(record);
                }
                else
                {                    
                    session.Store(new RecordHistory(record.Id, RavenJObject.FromObject(record)));
                }

                // this will trigger another call to VersionDocumentListener for the RecordHistory instance
                // but our guard clause will prevent processing 
                session.SaveChanges();
            }

            // signal that we have not modified the entityInstance
            return false;
        }
    }

    public class RecordHistory
    {
        public RecordHistory(string id, RavenJObject record)
        {
            this.Id = "recordhistory/" + id;
            Record = record;
        }

        public string Id { get; private set; } 
        public RavenJObject Record { get; set; }        
    }
}
﻿using Raven.Client;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace SoftModel.UI.Html.Domain
{
    public class ApiKeyHeaderHandler : DelegatingHandler
    {
        IDocumentSession documentSession;

        public ApiKeyHeaderHandler(IDocumentSession documentSession)
        {
            this.documentSession = documentSession;
        }

        protected override Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request, CancellationToken cancellationToken)
        {
            IEnumerable<string> apiKeyHeaderValues = null;
            if (request.Headers.TryGetValues("X-ApiKey", out apiKeyHeaderValues))
            {
                var apiKeyHeaderValue = apiKeyHeaderValues.First();

                var client = documentSession.Load<ApiClient>(apiKeyHeaderValue);
                if (client != null)
                {
                    var usernameClaim = new Claim(ClaimTypes.Name, client.Name, "");
                    var identity = new ClaimsIdentity(new[] { usernameClaim }, "ApiKey");
                    var principal = new ClaimsPrincipal(identity);

                    Thread.CurrentPrincipal = principal;
                }
            }

            return base.SendAsync(request, cancellationToken)
               .ContinueWith(task =>
               {
                   var response = task.Result;
                   //if (response.StatusCode == HttpStatusCode.Unauthorized
                   //    && !response.Headers.Contains(BasicAuthResponseHeader))
                   //{
                   //    response.Headers.Add(BasicAuthResponseHeader
                   //        , BasicAuthResponseHeaderValue);
                   //}
                   return response;
               });
        }
    }
}
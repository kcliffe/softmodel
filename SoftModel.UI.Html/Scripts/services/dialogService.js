﻿'use strict';

/*
    Uncermoniously ripped from https://github.com/Fundoo-Solutions/angularjs-modal-service.
    Provides a simple dialog service which doesn't depend on twitter bootstrap instead using
    Html5/css to provide the dialog functionality
*/

(function () {
    app.factory('DialogService', ["$document", "$compile", "$rootScope", "$controller", "$timeout",
	   function ($document, $compile, $rootScope, $controller, $timeout) {
	       var defaults = {
	           template: null,
	           templateUrl: null,
	           title: 'Default Title',
	           success: { label: 'OK' },
	           cancel: { label: 'OK' },
	           closeFn: null,
	           controller: null, //just like route controller declaration
	           footer: {
	               ok: { label: "Ok" },
	               cancel: { label: "Cancel" }
	           },
	           modalClass: "modalDialog"
	       };

	       var body = $document.find('body');

	       return function Dialog(options, passedInLocals) {
	           // $log.info('Dialog opened');

	           var content = '';
	           options = angular.extend({}, defaults, options);

	           var modalBody = (function () {
	               if (options.template) {
	                   if (angular.isString(options.template)) {
	                       // Simple string template
	                       return options.template;
	                   } else {
	                       // jQuery/JQlite wrapped object
	                       return options.template.html();
	                   }
	               } else {
	                   // Template url
	                   return '<div ng-include="\'' + options.templateUrl + '\'"></div>'
	               }
	           })();

	           var modalTemplate = '<div id="openModal" class="' + options.modalClass + '">' +
                                '<div><form>' +
                                '   <a title="Close" class="close" ng-click="$modalClose(false)">X</a>' +
                                '   <div class="modalHeader">' + options.title + '</div>' +
                                '   <div class="modalBody">' + modalBody + '</div>';

	           if (options.footer) {
	               modalTemplate += '   <div class="modalFooter"><button class="modalOkBtn" ng-click="$modalClose(true)">' +
                               options.footer.ok.label + '</button>';

	               if (options.footer.cancel) {
	                   modalTemplate += '<button class="modalCancelBtn" ng-click="$modalClose(false)">' +
                                   options.footer.cancel.label + '</button>';
	               }
	           }

	           modalTemplate += '</div></form></div></div>';
	           var modalEl = angular.element(modalTemplate);

	           var handleEscPressed = function (event) {
	               if (event.keyCode === 27) {
	                   scope.$modalClose();
	               }
	           };

	           var closeFn = function (isOk) {
	               body.unbind('keydown', handleEscPressed);
	               modalEl.remove();
	               if (angular.isFunction(options.closeFn)) {
	                   options.closeFn.call(this, isOk);
	               }
	           };

	           body.bind('keydown', handleEscPressed);

	           var ctrl, scope = options.scope || $rootScope.$new();

	           if (options.controller) {
	               var locals = angular.extend({ $scope: scope }, passedInLocals);
	               ctrl = $controller(options.controller, locals);
	               // Yes, ngControllerController is not a typo
	               modalEl.contents().data('$ngControllerController', ctrl);
	           }

	           $compile(modalEl)(scope);
	           body.append(modalEl);

	           scope.$modalClose = closeFn;

	           $timeout(function () {
	               modalEl.addClass('modalShow');
	           }, 200);
	       };
	   }]);
})();
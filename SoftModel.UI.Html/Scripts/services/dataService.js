﻿'use strict';

/*
    A abstraction that allows callers to get / set application data whether we are in offline mode - or not.
    All methods return promises.

    There is probably a way better way to do this in JS land...
*/

(function () {
    app.factory('DataService', ['DashboardService', 'SchemaService', 'OfflineDatasource', '$q', '$log',
        function (dashboardService, schemaService, offlineDatasource, $q, $log) {
            var svc = {
                isOffline: false,
                setOfflineContent: function (key, content) {
                    return offlineDatasource.set(key, content);
                },
                getDashboard: function () {
                    if (this.isOffline) {
                        // return the offline promise
                        return offlineDatasource.getDashboard();
                    }
                    else {
                        // return the $http service promise
                        return dashboardService.getDashboard();
                    }
                },
                putDashboard: function (userDashboard) {
                    if (this.isOffline) {
                        // not implemented for offline
                        return offlineDatasource.putDashboard(userDashboard);
                    }
                    else {
                        // return $http promise
                        return dashboardService.putDashboard(userDashboard);
                    }
                },
                putPageComponent: function(pageComponent) {
                    if (this.isOffline) {
                        // not implemented for offline
                        $log.info("not supported");
                        return 1;
                        // return offlineDatasource.putPageComponentSettings(userDashboard);
                    }
                    else {
                        // return $http promise
                        return dashboardService.putPageComponent(pageComponent);
                    }
                },
                getRecords: function (uri, queryCriteria) {
                    if (this.isOffline) {
                        // return the offline promise
                        return offlineDatasource.getRecordsBySchema(queryCriteria);
                    }
                    else {
                        // return the $http service promise
                        return dashboardService.getRecordsBySchema(uri, queryCriteria);
                    }
                },
                getRecord: function (recordSearchCriteria) {
                    if (this.isOffline) {
                        // return the offline promise
                        return offlineDatasource.getRecord(recordSearchCriteria);
                    }
                    else {
                        // return the $http service promise
                        return dashboardService.getRecord(recordSearchCriteria);
                    }
                },
                getForm: function (formName) {
                    if (this.isOffline) {
                        // not implemented for offline
                        return offlineDatasource.getForm(formName);
                    }
                    else {
                        // return $http promise
                        return dashboardService.getForm(formName);
                    }
                },
                getSchemaFields: function (filter) {
                    if (this.isOffline) {
                        return 1;
                    }
                    else {
                        // return $http promise
                        return schemaService.getSchemaFields(filter);
                    }
                },
                putRecord: function (recordToPut, options) {
                    if (this.isOffline) {
                        return offlineDatasource.putRecord(recordToPut, options);
                    }
                    else {
                        // return $http promise
                        return dashboardService.putRecord(recordToPut);
                    }
                }
            };

            return svc;
        }])
})();
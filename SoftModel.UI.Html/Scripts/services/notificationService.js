﻿'use strict';

(function () {
    app.factory('NotificationService', function ($rootScope, $log) {
          var callbacks = [];

          var hub = $.connection.notification;

          hub.client.itemUpdated = function (result) {
              $log.info(result.RecordDescriptorName + ' updated');

              var callbackRegistration = _.findWhere(callbacks, { RecordDescriptorName: result.RecordDescriptorName });
              if (callbackRegistration) {
                  $log.info('notification event triggered');
                  $rootScope.$apply(function () { callbackRegistration.Callback(result, callbackRegistration.state) });
              }
          }

          $.connection.hub.start();

          var notificationService = {
              registerItemUpdatedCallback: function (recordDescriptorName, callback, state) {
                  $log.info('register callback for record descriptor ' + recordDescriptorName);

                  if (!_.findWhere(callbacks, { RecordDescriptorName: recordDescriptorName })) {
                      callbacks.push({ RecordDescriptorName: recordDescriptorName, Callback: callback, State: state });
                  }
              }
          };

          return notificationService;
      });
})();

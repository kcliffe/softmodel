﻿'use strict';

/*
    Expose the (semi) RESTful Softmodel services.
*/

(function () {
    app.factory('AuditService', ['$http', '$q', function ($http, $q, tokenHandler) {
        var svc = {
            auditLogSearch: function (filter) {
                return $http({
                    url: apiResourceUri.auditSearch,
                    method: "GET",
                    params: filter
                });
            }
        };

        return svc;
    }])
     .factory('SchemaService', ['$http', '$q', function ($http, $q) {
         var svc = {
             search: function (query) {
                 query = query || '';
                 return $http.get(apiResourceUri.schemaSearch + query);
             },
             getSchema: function (schemaName) {
                 return $http.get(apiResourceUri.schemaGetByName + schemaName);
             },
             getSchemaFields: function (filter) {
                 return $http({
                     url: apiResourceUri.schemaGetFields,
                     method: "GET",
                     params: filter
                 });
             },
             searchEnums: function(query) {
                 return $http.get(apiResourceUri.searchEnums + query);
             },
             save: function (schema) {
                 return $http.put(apiResourceUri.schemaPut, schema);
             },
             doExport: function (schemas) {
                 return $http.put(apiResourceUri.export, schemas);
             },
             getImportFiles: function() {
                 return $http.get(apiResourceUri.getImportFileList);
             },
             doImport: function (schemas) {
                return $http.put(apiResourceUri.import, schemas);
             }
         };

         return svc;
     }])
    .factory('SurveyService', ['$http', '$q', function ($http, $q) {
        var svc = {
            search: function (query) {
                query = query || '';
                return $http.get(apiResourceUri.surveySearch + query);
            },
            getSurvey: function (surveyName) {
                return $http.get(apiResourceUri.surveyGetByName + surveyName);
            },
            save: function (survey) {
                return $http.put(apiResourceUri.surveyPut, survey);
            }
        };

        return svc;
    }])
    .factory('DashboardService', ['$http', '$q', function ($http, $q, tokenHandler) {
        var svc = {
            getRecord: function (recordSearchCriteria) {
                //recordId = toQsFromRavenId(recordSearchCriteria.recordId);
                return $http({
                    url: apiResourceUri.read,
                    method: "GET",
                    params: {
                        Id: recordSearchCriteria.RecordId,
                        Fields: recordSearchCriteria.Fields
                    }
                });
            },
            getRecordsBySchema: function (uri, queryCriteria) {
                return $http({
                    url: uri,
                    method: 'GET',
                    params: queryCriteria
                })
            },
            getForm: function (formName) {
                return $http.get(apiResourceUri.formGetByName + formName);
            },
            getDashboard: function () {
                return $http.get(apiResourceUri.userDashboard);
            },
            putDashboard: function (dashboard) {
                return $http.put(apiResourceUri.userDashboard, dashboard);
            },
            putPageComponent: function (pageComponentSettings) {
                return $http.put(apiResourceUri.pageComponentPut, pageComponentSettings);
            },
            putRecord: function (recordToPut) {
                return $http.put(apiResourceUri.instancePut, recordToPut);
            }
        };

        return svc;
    }])
    .factory('AccessService', ['$http', '$q', function ($http, $q, tokenHandler) {
        var svc = {
            ping: function (authHeader) {
                var config = {
                    headers: {
                        'Authorization': authHeader,
                        'Accept': 'application/json, text/plain, * / *',
                        'Content-Type': 'application/json'
                    }
                };
                return $http.get(apiResourceUri.accessping, config);
            }
        };

        return svc;
    }]);
})();

var apiResourceUri = function() {};
apiResourceUri.schemaSearch ='/api/v1/schema/search?q=';
apiResourceUri.schemaGetById ='/api/v1/schema/getbyid?id=';
apiResourceUri.schemaGetByName = '/api/v1/schema/getbyname?name=';
apiResourceUri.schemaGetFields = '/api/v1/schema/getschemafields?';
apiResourceUri.schemaPut = '/api/v1/schema/put';
apiResourceUri.surveySearch = '/api/v1/survey/search?q=';
apiResourceUri.surveyGetByName = '/api/v1/survey/getbyname?name=';
apiResourceUri.surveyPut = '/api/v1/survey/put';
apiResourceUri.export = '/api/v1/schema/export';
apiResourceUri.import = '/api/v1/schema/import';
apiResourceUri.pageComponentPut = '/api/v1/dashboard/pagecomponent';
apiResourceUri.formSearch ='/api/v1/forms/search?q=';
apiResourceUri.formGetByName ='/api/v1/forms/getbyname?name=';
apiResourceUri.metadataGetFieldTypes ='/api/v1/metadata/getFieldTypes';
apiResourceUri.metadataGetFieldTypeNames ='/api/v1/metadata/getFieldTypeNames';
apiResourceUri.instanceGetByRecordSchemaId ='/api/v1/instance/getbyrecordschemaid?id=';
apiResourceUri.instancePut ='/api/v1/instance/put';
apiResourceUri.instancePutChild ='/api/v1/instance/putchild/';
apiResourceUri.instancePutAsForm ='/api/v1/instance/putasform/';
apiResourceUri.readBySchemaId = '/api/v1/read/getbyrecordschema?';
apiResourceUri.readAssociated = '/api/v1/read/getassociated?';
apiResourceUri.read ='/api/v1/read/getbyid/';
apiResourceUri.userDashboard ='/api/v1/dashboard';
apiResourceUri.accessping ='/api/v1/access/auth';
apiResourceUri.auditSearch ='/api/v1/audit/search?';
apiResourceUri.searchEnums = '/api/v1/schema/searchenums?q=';
apiResourceUri.getImportFileList = '/api/v1/schema/GetExportedFileList';
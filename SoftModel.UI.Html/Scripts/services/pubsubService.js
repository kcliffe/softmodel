﻿'use strict';

(function () {
    // TODO. Before replacing $rootscope.$broadcast with this implementation, change functionality
    // 1) listen for $destroy and automatically unbind subscribers. Need controller "namespace"
    // during registration for this to happen
    // 2) get rid of jquery dependant d.each in unsub function. use foreach or _foreach
    app.factory('pubsub', function () {
        var cache = {};
        var pubsub = {
            publish: function (topic, args) {
                cache[topic] && $.each(cache[topic], function () {
                    this.apply(null, args || []);
                });
            },
            subscribe: function (topic, callback) {
                if (!cache[topic]) {
                    cache[topic] = [];
                }
                cache[topic].push(callback);
                return [topic, callback];
            },
            unsubscribe: function (handle) {
                var t = handle[0];
                cache[t] && d.each(cache[t], function (idx) {
                    if (this == handle[1]) {
                        cache[t].splice(idx, 1);
                    }
                });
            }
        }

        return pubsub;
    });
})();
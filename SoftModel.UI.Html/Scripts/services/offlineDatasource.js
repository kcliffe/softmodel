﻿'use strict';

/*
    Load dashboard data from the browsers local storage
*/

/*
TODO. 
- Top level storage container is currently pageComponent. Possible for same records to exist in multiple
 pageComponents on the dashboard. Store records once. Store references to others to save limited storage space.
*/

(function () {
    app.factory('OfflineDatasource', ['$rootScope', '$q', '$log', 'localStorageService',
        function ($rootScope, $q, $log, localStorage) {
        angularLocalStorage.prefix = 'sm';
        
        // records in offline mode don't have Id's (they're allocated by the storage mechanism behind the REST API)
        // but we'll need a temporary unique id. Instead of generating a unique Id, and since the client side is
        // "single threaded" we'll just increment a single counter
        var seqId = localStorage.get('currentSeq') || 0;

        /*
        Return a structure containing both record and page component data view id given
        a record identifier (a record can exist in many pagecomponents on a single dashboard
        */
        function recordSearch(recordId) {
            // we could index ... 
            // but for now, get the pageComponents from the user dashboard
            var userDashboard = localStorage.get('userDashboard');
            try
            {
                var recordInfo = [];
                _.each(userDashboard.Views[0].PageComponents, function (pageComp) {
                    $log.info('searching for record ' + recordId + ' in pageComp ' + pageComp.DataviewId);
                    var data = localStorage.get(pageComp.DataviewId);
                    if (data) {
                        var record = _.where(data, { id: recordId });
                        if(record) {
                            recordInfo.push({record: record, dataviewId: pageComp.DataviewId});
                        }
                    }
                });

                return recordInfo;
            }
            catch (e) {
                $log.error(e);
            }
        };

        function trackChanges(record) {
            var dirtyCol = localStorage.get('tracking') || [];
            var existingChangeRecords = _.where(dirtyCol, { id: record.id });
            if (existingChangeRecords.length) {
                // check in place update is okay
                existingChangeRecords[0].timestamp = new Date();
            }
            else {
                dirtyCol.push({ id: record.id, timestamp: new Date() });
            }

            $log.info('Changes tracked for record ' + record.id);
            localStorage.set('tracking', dirtyCol)
        };

        function addRecordToLocalStorage(putRecord, dataviewId) {
            putRecord.id = ++seqId;
            var today = new Date();
            putRecord.Created = putRecord.LastModified = today.toISOString();

            var recordsInPageComponent = localStorage.get(dataviewId);
            recordsInPageComponent.push(putRecord);

            localStorage.set(dataviewId, recordsInPageComponent);
            localStorage.set('currentSeq', seqId);
        }

        function set(key, content) {
            var deferred = $q.defer();
            try {
                $log.info('Storing value for key ' + key);
                localStorage.add(key, content);
                deferred.resolve('Key set');
                return deferred.promise;
            }
            catch (e) {
                deferred.reject(e);
            }
        };

        function getRecord(recordSearchCriteria) {
            var deferred = $q.defer();

            try {
                // lookup single record by id. As this is a subset of the data keyed by schemaname we'll need that to!
                var data = localStorage.get(recordSearchCriteria.schemaName);

                // search for record by id inside schema (underscore)
                deferred.resolve(data);
                return deferred.promise;
            }
            catch (e) {
                deferred.reject(e);
            }

            return deferred.promise;
        };

        function getRecordsBySchema(queryCriteria, page) {
            var deferred = $q.defer();

            try {
                var response = { data: {} };

                // lookup item keyed by schema name
                response.data = localStorage.get(queryCriteria.Key);
                deferred.resolve(response);

                $log.info('Successful retrieve from offline datastore for key ' + queryCriteria.Key);
                return deferred.promise;
            }
            catch (e) {
                deferred.reject(e);
            }
        };

        function getForm(formName) {
            var deferred = $q.defer();

            try {
                var response = { data: {} };

                // lookup item keyed by form name
                response.data = localStorage.get(formName);
                deferred.resolve(response);
                return deferred.promise;
            }
            catch (e) {
                deferred.reject(e);
            }
        };

        function getDashboard(userDashboard) {
            var deferred = $q.defer();
            var promise = deferred.promise;

            // Dunno. Can't seem to use .then on the promise on the dashboard controller...? So provide custom success.
            promise.success = function (fn) {
                promise.then(function (response) {
                    fn(response);
                });

                return promise;
            };

            promise.error = function (fn) {
                promise.then(null, function (response) {
                    fn(response);
                });

                return promise;
            };

            try {
                // lookup item keyed by form name
                var data = localStorage.get('userDashboard');
                deferred.resolve(data);

                $log.info('Successful retrieve from offline datastore for key ' + userDashboard);
                return deferred.promise;
            }
            catch (e) {
                deferred.reject(e);
            }
        };

        function putDashboard(userDashboard) {
            var deferred = $q.defer();

            try {
                // lookup item keyed by form name
                var data = localStorage.set('userDashboard', userDashboard);
                deferred.resolve(data);
                return deferred.promise;
            }
            catch (e) {
                deferred.reject(e);
            }
        };

        function putRecord(putRecord, options) {
            var deferred = $q.defer();

            try {
                var response = { data: {} };
                var recordInfos = [];

                // update existing records
                if (putRecord.id) {
                    var recordInfos = recordSearch(putRecord.Id);
                    if (recordInfos) {
                        _.each(recordInfos, function (recordInfo) { recordInfo.record = putRecord; });
                    }
                }
                else {
                    addRecordToLocalStorage(putRecord, options.DataviewId);
                    recordInfos.push({ record: null, dataviewId: options.DataviewId }); // we don't need the record
                }

                trackChanges(putRecord);
                response.data = putRecord;

                var dataviewIds = _.pluck(recordInfos, 'dataviewId');
                $rootScope.$broadcast('event:pageCompInvalidated', { dataviewIds: dataviewIds });

                deferred.resolve(response);
                return deferred.promise;
            }
            catch (e) {
                deferred.reject(e);
            }
        };

        return {
            set: function (key, content) { return set(key, content); },
            getRecord: function (recordSearchCriteria) { return getRecord(recordSearchCriteria); },
            getRecordsBySchema: function (queryCriteria, page) { return getRecordsBySchema(queryCriteria, page) },
            getForm: function (formName) { return getForm(formName); },
            getDashboard: function (userDashboard) { return getDashboard(userDashboard); },
            putDashboard: function (userDashboard) { return putDashboard(userDashboard); },
            putRecord: function (record, options) { return putRecord(record, options); }
        };
    }])
})();
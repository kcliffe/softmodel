﻿'use strict';

var app = angular.module('SoftModel', ['LocalStorageModule', 'ngRoute', 'ngAnimate', 'ngDropdowns'])
.config(['$routeProvider', '$httpProvider', function ($routeProvider, $httpProvider) {
    $routeProvider.
        when('/dashboard', { templateUrl: 'partials/dashboard.html', controller: "DashboardController" }).
        when('/adm/schema', { templateUrl: 'partials/schema/schema.html', controller: "SchemaController" }).
        when('/adm/survey', { templateUrl: 'partials/survey/survey.html', controller: "SurveyController" }).
        when('/adm/audit', { templateUrl: 'partials/audit.html', controller: "AuditController" }).
        otherwise({ redirectTo: '/dashboard' });
}])
.run(function ($rootScope) {
    $rootScope.offlineState = 'Online';
    $rootScope.isOffline = function () {
        return $rootScope.offlineState == 'Offline';
    }
});

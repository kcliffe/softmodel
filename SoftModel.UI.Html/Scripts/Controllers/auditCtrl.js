﻿'use strict';

app.controller("AuditController", function ($scope, AuditService, DashboardService) {
    $scope.searchResult = {};
    $scope.advancedSearch = false;
    $scope.filters = {
        EntityName: '*',
        EventType: '*',
        ItemType: '*',
        User: '*',
        Start: '1-1-2013',
        End: '10-10-2013',
        PageNum: 0,
        PageSize: 20
    };
    $scope.isSearching = false;
   
    $scope.$watch('filters.PageNum', function () {
        if ($scope.isSearching || !$scope.searchResult.AuditLogEntries) return;
        $scope.auditLogSearch();
    });

    $scope.auditLogSearch = function () {
        $scope.isSearching = true;
        AuditService.auditLogSearch($scope.filters).then(function (result) {
            $scope.searchResult = result.data;
            $scope.isSearching = false;
        });        
    }

    $scope.totalPageCount = function () {
        return Math.ceil($scope.searchResult.TotalResultCount / $scope.filters.PageSize);
    }
});
﻿'use strict';

app.controller("PageComponentController",
    ['$scope', 'DataService', 'NotificationService', 'DialogService', '$http', '$log', '$rootScope',
    function ($scope, DataService, NotificationService, DialogService, $http, $log, $rootScope) {
        $scope.Id = '';
        $scope.Data = [];
        $scope.newRecord = {};
        $scope.refresh = false;

        $scope.$watch("currentPageComp", function () {
            $log.info('Current page comp reset');
            $scope.asyncLoadPageComponent({})
            $scope.settings = $scope.currentPageComp.Visualizer.Settings;
            // todo. unregister callback!
        });

        $scope.init = function () {
            $log.info('initialising');
        };

        /*
        Refresh our data
        */
        $scope.asyncLoadPageComponent = function (options) {

            $scope.data = null;

            // If pageComp is a child in a parent - child relationship but we
            // haven't been supplied the parent (user has not had a chance to select one yet)
            // skip query.
            if ($scope.currentPageComp.ChildOf && !options.ParentId) {
                return;
            }

            // Build query criteria including page, filter etc info
            var queryCriteria = $scope.getQueryCriteria($scope.currentPageComp);
            queryCriteria = angular.extend(queryCriteria, { Page: $scope.currentPageComp.Page });

            $log.info(queryCriteria);

            var uri = apiResourceUri.readBySchemaId;

            // Build additional criteria info is we are a child component
            if ($scope.currentPageComp.ChildOf && options.ParentId) {
                $log.info('updating child pagecomp');
                uri = apiResourceUri.readAssociated;

                queryCriteria = angular.extend(queryCriteria, {
                    Id: toQsFromRavenId(options.ParentId),
                    ChildType: 'recordDescriptors-' + $scope.currentPageComp.RecordDescriptorName
                });
            }

            // Query for page component data
            DataService.getRecords(uri, queryCriteria).then(function (response) {
                $scope.Data = response.data;
            }, function () {
                toastr.error("Failed to load the data for a component", "Bugger")
            });

            if (!DataService.isOffline) {
                // Register for async callbacks to allow us to indicate when this pageComponent may be dirty
                NotificationService.registerItemUpdatedCallback($scope.currentPageComp.Visualizer.Settings.RecordDescriptorName, $scope.pageComponentDirty);
            }
        };

        /*
        Build query criteria for retrieving a list of records by page component
        */
        $scope.getQueryCriteria = function () {
            var pc = $scope.currentPageComp;

            var fields = "";
            _.map(pc.Visualizer.Settings.DisplayFields, function (value, key, list) {
                return fields += value.FieldDescriptorName + ',';
            });

            return {
                // Schema when online
                Id: 'recordDescriptors-' + pc.RecordDescriptorName,
                // Key when offline
                Key: pc.DataviewId,
                AsAtDate: pc.AsAtDate,
                Filter: pc.Visualizer.Settings.Filter || "",
                Fields: fields.substr(0, fields.length - 1),
                OrderBy: pc.Visualizer.Settings.OrderBy
            };
        };

        /*
        Callback which results in the refresh of a pageComponent when the content is considered dirty
        */
        $scope.pageComponentDirty = function (result, state) {
            $scope.IsDirty = true;

            toastr.info('Updated ' + result.ItemId);

            // todo. if child, send options
            $scope.asyncLoadPageComponent();
        }

        /*
        Respond to a user selecting a row
        */
        $scope.rowSelected = function (recordId) {
            var sourceDataViewId = $scope.currentPageComp.DataviewId;
            $log.info('Broadcasting refresh for child from parent ' + sourceDataViewId);
            $rootScope.$broadcast("event:childrefresh", { ParentPageCompId: sourceDataViewId, ParentId: toQsFromRavenId(recordId) });
        };

        /*
        Return a string uniquely identifying a field. 
        Currently just hashes field descriptor name and record descriptor name
        */
        $scope.uniqueFieldIdentifier = function (pageComp, fieldName) {
            return "fd_" + pageComp.RecordDescriptorName + "_" + fieldName;
        };

        /* ---------------------------------------------------------------------
        Message receivers
        --------------------------------------------------------------------- */
        $scope.$on("event:childrefresh", function (event, options) {
            var dataViewId = $scope.currentPageComp.ChildOf;
            if (!dataViewId || dataViewId != options.ParentPageCompId) {
                return;
            }

            $log.info('Received refresh for child page comp ' + options.ParentId);
            //$scope.$apply($scope.asyncLoadPageComponent(options));
            $scope.asyncLoadPageComponent(options);
        });

        /*
        Persist data to offline store
        */
        $scope.$on("event:capture_offline", function (event, options) {
            var thisId = $scope.currentPageComp.DataviewId;

            // TODO. Store data only?
            DataService.setOfflineContent(thisId, $scope.Data).then(function () {
                $log.info('Data now stored offline');
            },
            function (e) {
                $log.info('Error storing data offline: ' + e);
            });
        });

        /*
        */
        $scope.$on("event:pageCompInvalidated", function (event, eventData) {
            if (_.contains(eventData.dataviewIds, $scope.currentPageComp.DataviewId)) {
                $scope.asyncLoadPageComponent({});
            }
        });

        /* ---------------------------------------------------------------------
        Dialog methods
        --------------------------------------------------------------------- */

        /*
        Open a dialog containing filters for the current page component
        */
        $scope.openFilterDlg = function () {
            DialogService({
                templateUrl: "/includes/itemGridFilter.html",
                controller: 'pgFilterController',
                closeFn: function (isOk) {
                    alert(isOk);
                },
            }, {
                visualizer: $scope.currentPageComp.Visualizer.Settings,
                refresh: $scope.refresh
            });
        };

        /*
        Open a method for adding a record. TODO. Rename to OpenMethodDialog
        */
        $scope.showMethod = function () {
            var formName = $scope.currentPageComp.Visualizer.Settings.FormCreateDescriptionName;
            $log.info('Loading form ' + formName);

            DataService.getForm(formName).then(function (response) {
                $log.info('Form ' + formName + ' loaded');
                $scope.form = response.data;

                DialogService({
                    templateUrl: "/includes/form.html",
                    controller: 'formController',
                    closeFn: $scope.createRecord
                }, {
                    form: $scope.form
                });
            },
            function () { toastr.error("Failed to retrieve the form.", "Bugger!") });
        };

        /*
        Open a method for editing a record. TODO. Rename to openEditDialog
        */
        $scope.openRecordEditDlg = function (data) {
            var pageComp = $scope.currentPageComp;
            var fields = "";
            _.map(pageComp.Visualizer.Settings.DisplayFields, function (value, key, list) {
                return fields += value.FieldDescriptorName + ',';
            });
            fields = fields.substr(0, fields.length - 1);

            DataService.getRecord({ RecordId: data.item.Id, Fields: fields }).then(
                function (response) {
                    $scope.form = JSON.stringify(response.data);
                    DialogService({
                        templateUrl: "/includes/edit.html",
                        controller: 'editController',
                        closeFn: function () {
                            var appliedForm = angular.copy(settings);
                            appliedSettings.Data = null;
                            $scope.applySettings(appliedSettings);
                        }
                    }, {
                        settings: settings
                    });
                    $scope.openDialog($scope.editDlgOpts, $scope.editRecord);
                },
                function (response) { toastr.error('Failed to retrieve the record (' + data.item.Id + ').', 'Bugger!') });
        };

        /*
        Open a dialog containing settings for the current page component
        */
        $scope.openSettingsDlg = function () {
            $log.info('Loading settings');
            var settings = $scope.currentPageComp;

            var filter = {
                RecordDescriptorName: settings.RecordDescriptorName,
                Display: true
            };
            DataService.getSchemaFields(filter).then(function (response) {
                $log.info('Settings loaded');

                $scope.form = response.data;
                DialogService({
                    templateUrl: "/includes/pgCompSettings.html",
                    controller: 'settingsController',
                    closeFn: function () {
                        var appliedSettings = angular.copy(settings);
                        appliedSettings.Data = null;
                        $scope.applySettings(appliedSettings);
                    }
                }, {
                    settings: settings
                });
            });
        };

        $scope.applySettings = function (settings) {
            $log.info('applying user dashboard settings');
            var appliedSettings = settings || $scope.Data;

            DataService.putPageComponent(appliedSettings).then(
                function () {
                    $scope.asyncLoadPageComponent();
                },
                function () { toastr.error("Failed to save the settings.", "Bugger!") }
            );
        };

        /*
        Persists a record after editing
        */
        $scope.editRecord = function (result) {
            if (result) {
                var recordToPut = angular.fromJson(result);

                DataService.putRecord(recordToPut, {DataviewId: $scope.currentPageComp.DataviewId}).then(function (response) {
                    toastr.info("Record updated successfully", "Notification");
                }, function () { toastr.error("Problem saving the record.", "Bugger!"); });
            }
        };

        /*
        Persist a new record
        */
        $scope.createRecord = function (result) {
            if (result) {
                var recordToPut = {
                    RecordDescriptorRef: $scope.form.FormDefinition.RecordDescriptorReference,
                    Fields: []
                }

                // http://docs.angularjs.org/api/angular.forEach (perf)
                angular.forEach($scope.form.Fields, function (val, key) {
                    if (!val.Value) return;
                    // no translation for Enumeration!
                    var fieldValue = {
                        FriendlyType: val.FriendlyType,
                        FieldName: val.Name,
                        Value: val.Value
                    }
                    recordToPut.Fields.push(fieldValue);
                }, recordToPut);

                DataService.putRecord(recordToPut, { DataviewId: $scope.currentPageComp.DataviewId }).then(function (response) {
                    toastr.info("Record updated successfully", "Notification");
                }, function () { toastr.error("Problem saving the record.", "Bugger!"); });
            }
        }
    }]);

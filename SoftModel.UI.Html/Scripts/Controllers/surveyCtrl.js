﻿'use strict';

app.controller("SurveyController", ['$scope', 'SurveyService', '$http', 'limitToFilter',
    function ($scope, SurveyService, $http, limitToFilter, SchemaConversion) {
        $scope.isEditingSurvey = false;
        $scope.surveyFieldTypes = ['string', 'choice', 'multiple choice', 'date', 'time'];
        $scope.selectedAddType = '';
        $scope.editorUrl = '';
        $scope.questionCopy = {};
        var qId = 0;

        // type ahead 
        $scope.items = [];
        $scope.term = '';

        // model
        var templateForm = {
            LastPublished: null,
            Name: 'Title',
            Description: '',
            ETag: '',
            Questions: [
                {
                    Id: 0,
                    Name: 'Question 1',
                    HelpText: 'Hey yalll.....',
                    isEditing: false,
                    Type: 'string',
                    IsActive: true
                }
            ]
        };

        $scope.form = templateForm;

        var watch = function (curVal, newVal) {
            if (curVal !== newVal && curVal != '') {
                $scope.form.Questions.push({
                    Id: ++qId,
                    Name: 'title',
                    HelpText: 'Hey yalll.....',
                    isEditing: false,
                    Type: $scope.selectedAddType,
                    IsActive: true
                });
                // clear the watch so we can change the observable without triggering
                watch();
                $scope.selectedAddType = '';
                watch = $scope.$watch('selectedAddType', watch);
            };
        };

        $scope.$watch('selectedAddType', watch);

        $scope.addSurvey = function () {
            $scope.isEditingSurvey = true;
            $scope.form = templateForm;
        };

        $scope.remove = function (question) {
            $scope.form.Questions.splice(_.indexOf($scope.form.Questions, question));
        };

        $scope.edit = function (question, isSaving) {
            if (isSaving) {
                question.isEditing = false;
            }
            else {
                angular.copy(question, $scope.questionCopy);
                question.isEditing = true;
            }
        };

        $scope.cancel = function (question) {
            angular.copy($scope.questionCopy, question);
            question.isEditing = false;
        };

        $scope.change = function (question) {
            $scope.editorUrl = "/partials/survey/editors/" + question.Type.replace(/ /g, '') + ".html";
        };

        $scope.save = function () {
            // var schema = SchemaConversion.ToSchema($scope.form);

            // From this point, duplicate of the schemaCtrl method - not DRY
            // TemporaryHackSchema($scope.schema, false);

            SurveyService.save($scope.form).then(function (response) {
                toastr.info("Survey saved successfully.");
                reset();
            },
            function () {
                toastr.error("Failed to save the survey.");
            });
        };

        var reset = function () {
            $scope.isEditingSurvey = false;
        };

        /*
        Provide search 
        */
        $scope.surveySearch = function (term) {
            return SurveyService.search(term).then(function (response) {
                $scope.items = response.data;
            });
        };

        /*
        Load a schema into the model when the user has specified it
        */
        $scope.getSurvey = function (survey) {
            SurveyService.getSurvey(survey.Name).then(function (response) {
                $scope.form = response.data;
                $scope.isEditingSurvey = true;
                //TemporaryHackSchema($scope.form, true);
            }, function () {
                toastr.error("Failed to retrieve the schema");
            });
        };
    }]);

app.controller("SurveyChoiceCtrl", ['$scope',
    function ($scope) {
        $scope.questionCopy.Options = [
            { Name: "Option 1", Value: "Val" },
            { Name: "Option 2", Value: "Val 2" },
            { Name: "Option 3", Value: "Val 3" }];
    }]);
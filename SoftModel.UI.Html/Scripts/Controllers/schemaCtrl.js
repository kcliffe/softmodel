﻿'use strict';

app.controller("SchemaController", ['$scope', 'SchemaService', '$http', 'limitToFilter',
    function ($scope, SchemaService, $http, limitToFilter) {
        $scope.schema = {};
        var schemaTemp = {};

        $scope.isEditingSchema = false;
        $scope.isEditingSchemaDetails = false;
        $scope.isEditingFields = 0;
        $scope.isExporting = false;
        $scope.isImporting = false;

        $scope.schemaName = '';
        $scope.fieldTypes = [];
        $scope.currentFieldDescriptor = {};
        $scope.dupName = false;
        $scope.editorUrl = '';

        var friendlyTypeListener = null;

        // type ahead 
        $scope.items = [];
        $scope.term = '';

        // export
        $scope.schemaSelected = false;
        $scope.importFileList = [];
        var downloadToken = '';

        /*
        Hack we need because the api internals utilise a deepclone object which cannot deal with 
        Dict<string, string> so IEnumerable<KeyValuePair> is currently used. Which is no good to us.
        */
        var TemporaryHackSchema = function (schema, to) {
            if (to) {
                _.each(schema.FieldDescriptors, function (fd) {
                    var newOpt = {};
                    _.each(fd.Options, function (opt) {
                        newOpt[opt.Key] = opt.Value;
                    });
                    fd.Options = newOpt;
                });
            }
            else {
                _.each(schema.FieldDescriptors, function (fd) {
                    var optArray = [];
                    
                    for (var propertyName in fd.Options) {
                        var opt = {};
                        opt['Key'] = propertyName;
                        opt['Value'] = fd.Options[propertyName];
                        optArray.push(opt);
                    }
                   
                    fd.Options = optArray;
                });
            }
        }

        /*
        Provide schema search 
        */
        $scope.schemaSearch = function (term) {
            return SchemaService.search(term).then(function (response) {
                $scope.items = response.data;
            });
        };

        /*
        Load a schema into the model when the user has specified it
        */
        $scope.getSchema = function (schema) {
            SchemaService.getSchema(schema.Name).then(function (response) {
                $scope.schema = response.data;
                $scope.isEditingSchema = true;

                $scope.ensureFieldTypes();

                TemporaryHackSchema($scope.schema, true);
            }, function () {
                toastr.error("Failed to retrieve the schema");
            });
        };

        /*
        Cancel schema editing
        */
        $scope.reset = function () {
            $scope.schema = {};
            $scope.isEditingSchema = false;
        };

        /*
        Create a new schema type
        */
        $scope.addSchema = function () {
            $scope.schema = { "Name": "", "Label": "", "FieldDescriptors": [], IsActive: true };
            $scope.schemaTemp = $scope.schema;

            $scope.ensureFieldTypes();

            $scope.isEditingSchema = true;
            $scope.isEditingSchemaDetails = true;
        };

        $scope.schemaDetailsValid = function () {
            return $scope.schema.Name.length && $scope.schema.Label.length;
        };

        /*
        Close the schema details editor
        */
        $scope.closeSchemaDetails = function () {
            $scope.isEditingSchemaDetails = false;
        };

        /*
        Cancel editing of schema details
        */
        $scope.cancelSchemaDetails = function () {
            $scope.schema = schemaTemp;
            $scope.isEditingSchemaDetails = false;
            if (!$scope.schema.Id) { $scope.isEditingSchema = false; }
        }

        /*
        Calculate the url to load the partial Html for a property editor
        */
        var setEditorUrl = function (friendlyType) {
            var friendlyType = friendlyType.replace(/ /g, '');
            $scope.editorUrl = 'partials/schema/editors/' + friendlyType + '.html';
        };

        /*
        Present the edit ui when an existing field selected
        */
        $scope.selectField = function (fieldDescriptor) {
            // clone fieldDesc to allow for cancel
            angular.copy(fieldDescriptor, $scope.currentFieldDescriptor);
            setEditorUrl(fieldDescriptor.FriendlyType);
            $scope.isEditingFields = 2;
        }

        /*
        Add a single field to a schema and indicate the editor should become active
        */
        $scope.addField = function () {
            $scope.currentFieldDescriptor = { "Name": "", "FriendlyType": "String", "RelatedRecordDescriptorId": null };
            $scope.isEditingFields = 1;
            friendlyTypeListener = $scope.$watch('currentFieldDescriptor.FriendlyType', function () {
                setEditorUrl($scope.currentFieldDescriptor.FriendlyType);
            });
        }

        /*
        Update the schema with the new field def
        */
        $scope.updateFieldDef = function () {
            var existingField = _.findWhere($scope.schema.FieldDescriptors, { Name: $scope.currentFieldDescriptor.Name });

            // this should be done via form validation
            if ($scope.isEditingFields == 1 && existingField) {
                $scope.dupName = true;
                return;
            }

            // clone the currentFieldDescriptor (leaving it as a by ref object .. nope)
            if (existingField) {
                angular.copy($scope.currentFieldDescriptor, existingField);
            } else {
                $scope.schema.FieldDescriptors.push(angular.copy($scope.currentFieldDescriptor));
            }

            $scope.isEditingFields = 0;

            if (friendlyTypeListener) friendlyTypeListener();
        }

        /*
        Cancel the property edit operation
        */
        $scope.resetEditable = function () {
            $scope.isEditingFields = $scope.isEditingSchemaDetails = false;
            if (friendlyTypeListener) friendlyTypeListener();
        }

        /*
        Save the schema
        */
        $scope.save = function () {
            TemporaryHackSchema($scope.schema, false);

            SchemaService.save($scope.schema).then(function (response) {
                toastr.info("Schema saved successfully.");
                $scope.reset();
            }, 
            function () {
                toastr.error("Failed to save the schema.");
            });
        };

        /*
        Load field types if we haven't already
        */
        $scope.ensureFieldTypes = function () {
            if (!$scope.fieldTypes.length) {
                return $http.get(apiResourceUri.metadataGetFieldTypes).then(function (response) {
                    $scope.fieldTypes = response.data;
                });
            }
        };

        /*
        Export / Import below to be moved
        */
        $scope.export = function () {
            SchemaService.search().then(function (response) {
                $scope.items = response.data;
                $scope.isExporting = true;
            });
        }

        $scope.import = function () {
            SchemaService.getImportFiles().then(function (response) {
                $scope.items = response.data;
                $scope.isImporting = true;
            });
        };

        $scope.selectSchema = function () {
           $scope.schemaSelected = _.any($scope.items, function (item) { return item.selected; });
        }

        $scope.doExport = function () {
            var selected = _.pluck(_.filter($scope.items, function (item) { if (item.selected) return item.Name; }), "Name");

            SchemaService.doExport(selected).then(function (response) {
                toastr.info('The export was successful. (' + response.data + ')');
            });
        }

        $scope.doImport = function () {
            var selected = _.pluck(_.filter($scope.items, function (item) { if (item.selected) return item.Name; }), "Name");

            SchemaService.doImport(selected).then(function (response) {
                toastr.info('The import was successful. (' + response.data + ')');
                $scope.isImporting = false;
            });
        }
    }]);

app.controller("OneToManyEditor", ['$scope', 'SchemaService', '$log',
    function ($scope, SchemaService, $log) {
        $scope.availableSchema = [];
        $scope.selectedType = {};
        $log.info($scope.currentFieldDescriptor);

        SchemaService.search('').then(function (response) {
            var schema = _.reject(response.data, function(s) { return s.Name === $scope.schema.Name });
            // filter the current type
            $scope.selectedType = schema[0].Name;
            $scope.availableSchema = schema;
        },
        function () {
            toastr.error("Failed to retrieve the schema");
        });
    }]);

app.controller("DateController", ['$scope',
    function ($scope) {
        $scope.availableSchema = [];
        $scope.selectedType = {};
        if (!$scope.currentFieldDescriptor.Options) {
            $scope.currentFieldDescriptor.Options = {
                mindate: new Date(),
                maxDate: new Date(),
                default: new Date()
            };
        }
        $scope.options = $scope.currentFieldDescriptor.Options;
    }]);

app.controller("StringController", ['$scope',
    function ($scope) {
        $scope.availableSchema = [];
        $scope.selectedType = {};
        $scope.options = $scope.currentFieldDescriptor.Options || {
            minlength: 2,
            maxlength: 2
        }
    }]);

app.controller("EnumerationFldController", ['$scope', 'SchemaService',
    function ($scope, SchemaService) {
        $scope.enums = [];
        $scope.selectedEnum = {};
        $scope.term = '';

        $scope.enumSearch = function (term) {
            SchemaService.searchEnums(term).then(function (response) {
                var enums = response.data;
                $scope.enums = enums;
            },
            function () {
                toastr.error("Failed to retrieve the enums");
            });
        }

        $scope.getEnum = function (item) {
            $scope.selectedEnum = item;
            $scope.term = item.Name
        }
    }]);
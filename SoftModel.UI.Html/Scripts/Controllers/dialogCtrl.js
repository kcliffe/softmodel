﻿'use strict';

app.controller('pgSettingsController',
    ['$scope', '$log', function ($scope, $log) {
    $scope.visualizer = visualizer;
}]);

app.controller('pgFilterController', 
    ['$scope', '$log', 'DataService', function ($scope, $log, DataService) {
        var numericOpts = ['=', '>', '<', '>=', '<=', '!=', 'in ()'];

        $scope.filterOpts = [
        { type: 'integer', opts: numericOpts },
        { type: 'decimal', opts: numericOpts },
        { type: 'date', opts: numericOpts },
        { type: 'string', opts: ['starts with', 'ends with', 'contains', 'is'] },
        { type: 'boolean', opts: ['true', 'false'] },
        { type: 'enumeration', opts: ['is', 'in'] },
        ];

        $scope.recordDescriptor = {
            name: 'Risk',
            fields: [
                { name: 'name', type: 'string' },
                { name: 'numCtrls', type: 'integer' },
                { name: 'avg ctrl cost', type: 'decimal' },
                { name: 'last modified', type: 'date' },
                { name: 'risk', type: 'enumeration' },
                { name: 'enabled', type: 'boolean' }
            ]
        };

        $scope.filters = [];
        $scope.selectedField = {};

        $scope.addNewFilter = function () {
            $scope.filters.push({ field: 'name', filter: 'starts with', value: '' });
        };

        $scope.availFilterTypes = function (filter) {
            if (filter.field.name) {
                var type = $scope.recordDescriptor.fields.filter(function (f) { return f.name === filter.field.name; })[0].type;
                var filterOpts = $scope.filterOpts.filter(function (el) { return el.type === type; });
                return filterOpts ? filterOpts[0].opts : [];
            }
            return ['select field'];
        };
    }]);

app.controller('formController',
    ['$scope', 'form', function ($scope,form) {
        $scope.form = form;
    }]);

app.controller('settingsController',
    ['$scope', 'settings', function ($scope, settings) {
        $scope.settings = settings;
    }]);

app.controller('editController',
    ['$scope', 'settings', function ($scope, form) {
        $scope.form = form;
    }]);
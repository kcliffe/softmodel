﻿'use strict';

app.controller("DashboardController",
    ['$scope', '$rootScope', 'DataService', 'NotificationService', 'SchemaService', '$http', '$log',
    function ($scope, $rootScope, DataService, NotificationService, SchemaService, $http, $log) {
        $scope.userDashboard = {};
        $scope.currentView = {};
        $scope.settings = false;

        /*
        Activate a dashboard
        */
        $scope.viewDashboard = function (index) {
            $log.info('view change');
            $scope.currentView = null;
            $scope.currentView = $scope.userDashboard.Views[index];
            // 1.2rc doesn't destroy child scopes (optimisation?). So broadcast...
            //$scope.$broadcast("view_selected", { newPageComp: $scope.currentView });
        }

        /*
        Capture current dashboard state for offline mode
        */
        $scope.snapOffline = function () {
            try {
                // TODO. This would not happen here. It would simply reload the dashboard (the Dataservice would then source the
                // dashboard from offline storage).
                DataService.setOfflineContent('userDashboard', $scope.userDashboard);
            }
            catch (e) {
                toastr.error('Failed to perist the dashboard');
            }

            /*
            iteration 1: take *current* page component data and archive it
            */
            $rootScope.$broadcast("event:capture_offline");
        };

        /*
        Toggle offline / online status. Will eventually be triggered by event.
        */
        $scope.toggleOffline = function () {
            DataService.isOffline = !DataService.isOffline;
            $scope.loadDashboard();

            $rootScope.offlineState = DataService.isOffline ? 'Offline' : 'Online';
            $log.info('Dashboard is now ' + $scope.offlineState);
        };

        /*
        Retrieve the dashboard metadata containing views and layouts
        */
        $scope.loadDashboard = function () {
            // Load dashboard data
            DataService.getDashboard()
                .success(function (data, status, headers, config) {
                    $scope.userDashboard = data;
                    $scope.currentView = data.Views[0];
                    $log.info('Dashboard loaded.');
                })
                .error(function (data, status, headers, config) {
                    toastr.error("Failed to load the dashboard", "Bugger");
                });
        };

        /*
        Initialise a dasboard
        */
        $scope.init = (function () {
            $scope.loadDashboard();
        })();

        /*
        Callback which occurs when a user has sucessfully authenticated
        */
        $scope.$on('event:auth-loginConfirmed', function () {
            $scope.loadDashboard();
        });

        /*
        Callback that occurs when the user renames a dashboard. We get the opportunity to persist the new userDashboard.
        */
        $scope.dashboardNameUpdated = function () {
            return $scope.PersistDashboard(true);
        };

        $scope.persistDashboard = function (showUser) {
            // TODO. Whole method should retrun promise allowing us to chain

            // in this case we used two way data binding to the editable directive so the userdashboard obj is
            // already updated. We just need to triger a save operation
            return DataService.putDashboard($scope.userDashboard).then(function (data) {
                if (showUser) {
                    toastr.info("Dashboard updated");
                }
                return true;
            },
            function () {
                toastr.info("Dashboard update failed");
                return false;
            });
        };
    }]);
﻿FriendlyTypes = {
    String: "String",
    Date: "Date",
    Decimal: "Decimal",
    OneToMany: "OneToMany",
    Boolean: "Boolean",
    Integer: "Integer"
};

/* todo - extend angular? or stick on base controller and inherit controllers  */
var toQsFromRavenId = function (ravenFormattedId) {
    return ravenFormattedId.replace(/\//, '-');
};

$(function() {
    toastr.options = { positionClass: 'toast-bottom-left' };
});
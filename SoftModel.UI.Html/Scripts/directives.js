﻿angular.module("SoftModel")   
  // creates a div which becomes a text input on double click - it also exposes a commit event so the controller 
  // can take appropriate action
  .directive("editable", function ($compile) {
      var editTemplate = '<input type="text" ng-model="__x__" ng-show="isEditing" ng-dblclick="switchToDisplay()" bg-blur="switchToDisplay()" /><span ng-show="isEditing" ng-click="fireCallback()">[ok]</span>';
      var displayTemplate = '<div ng-hide="isEditing" ng-dblclick="switchToEdit()">{{__x__}}</div>';

      return {
          restrict: 'E',
          compile: function (tElement, tThing, tTransclude) {
              tElement.html(editTemplate);
              tElement.append(displayTemplate);

              // return link function
              return function (scope, element, attrs) {
                  scope.isEditing = false;

                  var n = element.html().replace(/__x__/g, attrs.bindto);
                  element.html(n);
                  $compile(element.contents())(scope);

                  scope.switchToEdit = function () {
                      scope.isEditing = true;
                  };

                  scope.switchToDisplay = function () {
                      scope.isEditing = false;
                  };

                  scope.fireCallback = function () {
                      if (attrs.commitfn) {
                          var func = scope[attrs.commitfn];
                          if (func()) {
                              scope.switchToDisplay();
                          };
                      }
                  }
              }
          }
      };
  })
.directive('paginate', ['$timeout', function (timer) {
    return {
        restrict: 'C',
        replace: true,
        scope: {
            pages: '@paginatePages',
            currentPage: '=currentPage'
        },
        template: '<div class="pagination-holder">' + '</div>',
        controller: function ($scope, $element, $attrs) {
            var halfDisplayed = 1.5,
                displayedPages = 3,
                edges = 2;

            $scope.getInterval = function () {
                return {
                    start: Math.ceil($scope.currentPage > halfDisplayed ? Math.max(Math.min($scope.currentPage - halfDisplayed, ($scope.pages - displayedPages)), 0) : 0),
                    end: Math.ceil($scope.currentPage > halfDisplayed ? Math.min($scope.currentPage + halfDisplayed, $scope.pages) : Math.min(displayedPages, $scope.pages))
                };
            }

            $scope.selectPage = function (pageIndex) {
                $scope.currentPage = pageIndex;
                $scope.draw();
                $scope.$apply();
            }

            $scope.appendItem = function (pageIndex, opts) {
                var options, link;

                pageIndex = pageIndex < 0 ? 0 : (pageIndex < $scope.pages ? pageIndex : $scope.pages - 1);

                options = $.extend({
                    text: pageIndex + 1,
                    classes: ''
                }, opts || {});

                if (pageIndex == $scope.currentPage) {
                    link = $('<span class="current">' + (options.text) + '</span>');
                } else {
                    link = $('<a href="javascript:void(0)" class="page-link">' + (options.text) + '</a>');
                    link.bind('click', function () {
                        $scope.selectPage(pageIndex);
                    });
                }

                if (options.classes) {
                    link.addClass(options.classes);
                }

                $element.append(link);
            }

            $scope.draw = function () {
                $($element).empty();
                var interval = $scope.getInterval(),
                    i;

                // Generate Prev link
                if (true) {
                    $scope.appendItem($scope.currentPage - 1, {
                        text: '&lt;',
                        classes: 'prev'
                    });
                }

                // Generate start edges
                if (interval.start > 0 && edges > 0) {
                    var end = Math.min(edges, interval.start);
                    for (i = 0; i < end; i++) {
                        $scope.appendItem(i);
                    }
                    if (edges < interval.start) {
                        $element.append('<span class="ellipse">...</span>');
                    }
                }

                // Generate interval links
                for (i = interval.start; i < interval.end; i++) {
                    $scope.appendItem(i);
                }

                // Generate end edges
                if (interval.end < $scope.pages && edges > 0) {
                    if ($scope.pages - edges > interval.end) {
                        $element.append('<span class="ellipse">...</span>');
                    }
                    var begin = Math.max($scope.pages - edges, interval.end);
                    for (i = begin; i < $scope.pages; i++) {
                        $scope.appendItem(i);
                    }
                }

                // Generate Next link
                if (true) {
                    $scope.appendItem($scope.currentPage + 1, {
                        text: '&gt;',
                        classes: 'next'
                    });
                }
            }
        },
        link: function (scope, element, attrs, controller) {
            timer(function () {
                scope.draw();
            }, 2000);
            scope.$watch(scope.paginatePages, function () { scope.draw(); })
        }

    }
}]).directive('typeahead', ["$timeout", "$log", function ($timeout, $log) {
    return {
        restrict: 'E',
        transclude: true,
        replace: true,
        template: '<div><form><label for="input">{{label}}</label><input name="input" ng-model="term" ng-change="query()" type="text" autocomplete="off" /></form><div ng-transclude></div></div>',
        scope: {
            label: "@",
            search: "&",
            select: "&",
            items: "=",
            term: "="
        },
        controller: ["$scope", function ($scope) {
            $scope.items = [];
            $scope.hide = true;

            this.activate = function (item) {
                $scope.active = item;
            };

            this.activateNextItem = function () {
                var index = $scope.items.indexOf($scope.active);
                this.activate($scope.items[(index + 1) % $scope.items.length]);
            };

            this.activatePreviousItem = function () {
                var index = $scope.items.indexOf($scope.active);
                this.activate($scope.items[index === 0 ? $scope.items.length - 1 : index - 1]);
            };

            this.isActive = function (item) {
                return $scope.active === item;
            };

            this.selectActive = function () {
                this.select($scope.active);
            };

            this.select = function (item) {
                $scope.hide = true;
                $scope.focused = true;
                $scope.select({ item: item });
            };

            $scope.isVisible = function () {
                return !$scope.hide && $scope.items.length && ($scope.focused || $scope.mousedOver);
            };

            $scope.query = function () {
                $scope.hide = false;
                $scope.search({ term: $scope.term });
            }
        }],

        link: function (scope, element, attrs, controller) {

            var $input = element.find('form > input');
            var $list = element.find("> div");

            $input.bind('focus', function () {
                scope.$apply(function () { scope.focused = true; });
            });

            $input.bind('blur', function () {
                scope.$apply(function () { scope.focused = false; });
            });

            $list.bind('mouseover', function () {
                scope.$apply(function () { scope.mousedOver = true; });
            });

            $list.bind('mouseleave', function () {
                scope.$apply(function () { scope.mousedOver = false; });
            });

            $input.bind('keyup', function (e) {
                if (e.keyCode === 9 || e.keyCode === 13) {
                    scope.$apply(function () { controller.selectActive(); });
                }

                if (e.keyCode === 27) {
                    scope.$apply(function () { scope.hide = true; });
                }
            });

            $input.bind('keydown', function (e) {
                if (e.keyCode === 9 || e.keyCode === 13 || e.keyCode === 27) {
                    e.preventDefault();
                };

                if (e.keyCode === 40) {
                    e.preventDefault();
                    scope.$apply(function () { controller.activateNextItem(); });
                }

                if (e.keyCode === 38) {
                    e.preventDefault();
                    scope.$apply(function () { controller.activatePreviousItem(); });
                }
            });

            scope.$watch('items', function (items) {
                controller.activate(items.length ? items[0] : null);
            });

            scope.$watch('focused', function (focused) {
                if (focused) {
                    $timeout(function () { $input.focus(); }, 0, false);
                }
            });

            scope.$watch('isVisible()', function (visible) {
                if (visible) {
                    var pos = $input.position();
                    var height = $input[0].offsetHeight;

                    $list.css({
                        top: pos.top + height,
                        left: pos.left,
                        position: 'absolute',
                        display: 'block'
                    });
                } else {
                    $list.css('display', 'none');
                    $input.css('backgroundColor: #ccfee')
                }
            });
        }
    };
}]).directive('typeaheadItem', ["$log", function () {
    return {
        require: '^typeahead',
        link: function (scope, element, attrs, controller) {

            var item = scope.$eval(attrs.typeaheadItem);

            scope.$watch(function () { return controller.isActive(item); }, function (active) {
                if (active) {
                    element.addClass('active');
                } else {
                    element.removeClass('active');
                }
            });

            element.bind('mouseenter', function (e) {
                scope.$apply(function () { controller.activate(item); });
            });

            element.bind('click', function (e) {
                scope.$apply(function () { controller.select(item); });
            });
        }
    };
}]);
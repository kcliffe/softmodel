﻿using System.Linq;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;
using SoftModel.Schema;

namespace SoftModel.UI.Html.Indexes
{
// ReSharper disable InconsistentNaming
    public class RecordByRecordSchemaId : AbstractIndexCreationTask<Record>
// ReSharper restore InconsistentNaming
    {
        public RecordByRecordSchemaId()
        {
            Map = records => from record in records
                             select new 
                             {    
                                 record.Id,
                                 record.Fields,
                                 RecordSchemaRef_RecordDescriptorId = record.RecordDescriptorRef.RecordDescriptorId
                             };

            Index(x => x.RecordDescriptorRef.RecordDescriptorId, FieldIndexing.Analyzed);
        }
    }     
}
﻿using System.Linq;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;
using SoftModel.Schema;

namespace SoftModel.UI.Html.Indexes
{
    public class Enum_Search : AbstractIndexCreationTask<Enumeration, EnumerationSearchResult>
    {
        public Enum_Search()
        {
            Map = enums => from s in enums
                             select new EnumerationSearchResult { Id = s.Id, Name = s.Name };

            Index(x => x.Name, FieldIndexing.Analyzed);
        }
    }

    public class EnumerationSearchResult
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
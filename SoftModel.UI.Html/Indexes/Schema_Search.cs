﻿using System.Linq;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;
using SoftModel.Schema;

namespace SoftModel.UI.Html.Indexes
{
    public class Schema_Search : AbstractIndexCreationTask<RecordDescriptor, SchemaSearchResult>
    {
        public Schema_Search()
        {
            Map = schemas => from s in schemas
                            select new SchemaSearchResult { Id=s.Id, Name = s.Name, Label=s.Label };

            Index(x => x.Label, FieldIndexing.Analyzed);
        }
    }

    public class SchemaSearchResult
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Label { get; set; }
    }
}
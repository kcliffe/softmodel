﻿using System.Linq;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;
using SoftModel.Schema.Survey;

namespace SoftModel.UI.Html.Indexes
{
    public class Survey_Search : AbstractIndexCreationTask<Survey, SurveySearchResult>
    {
        public Survey_Search()
        {
            Map = schemas => from s in schemas
                            select new SurveySearchResult { Id=s.Id, Name = s.Name };

            Index(x => x.Name, FieldIndexing.Analyzed);
        }
    }

    public class SurveySearchResult
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;
using SoftModel.Schema;

namespace SoftModel.UI.Html.Indexes
{
    public class Form_Search : AbstractIndexCreationTask<FormDefinition, FormDefinitionSearchResult>
    {
        public Form_Search()
        {
            Map = formDefinitions => from f in formDefinitions
                                     select new FormDefinitionSearchResult
                                        {
                                            Id = f.Id,
                                            Name = f.Name
                                        };

            Index(x => x.Name, FieldIndexing.Analyzed);
        }
    }

    public class FormDefinitionSearchResult
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
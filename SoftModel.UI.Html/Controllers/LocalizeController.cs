﻿using log4net;
using Raven.Client;
using SoftModel.UI.Html.Domain;
using SoftModel.UI.Html.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace SoftModel.UI.Html.Controllers
{
    //[Authorize]
    [ExceptionHandlingAttribute]
    public class LocalizeController : BaseApiController
    {
        const string DefaultLocale = "en";
        private static ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public LocalizeController(IDocumentSession documentSession) : base(documentSession) 
        { 
            // TODO. Query for global settings (could use app config. RavenDb is just as easy)
        }

        [HttpGet]
        public List<LanguageEntry> Get(string id)
        {
            // Retrieve resources
            var langResources = Retrieve<LanguageResource>(id);
            if (langResources == null)
            {
                // Fallback to language neutral
                var localeSeperator = id.Substring(0, id.IndexOf('-'));
                if (!string.IsNullOrWhiteSpace(localeSeperator))
                {
                    // So. If the fallback fails - if's a qaundry. We either fallback to default lang (en)
                    // or we throw 404. Opt for fallback so the site at least presents content (whether the user
                    // will understand it is another issue)
                    langResources = Retrieve<LanguageResource>(localeSeperator);
                    if (langResources == null)
                    {
                        langResources = Retrieve<LanguageResource>(DefaultLocale);
                        if (langResources == null)
                        {
                            var error = string.Format("The default resources ({0}) were not found!", DefaultLocale);
                            Log.Fatal(error);

                            base.NotFound(error);
                        }
                    }
                }
            }

            return langResources.Entries.ToList();
        }
    }
}
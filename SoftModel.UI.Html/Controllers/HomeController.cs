﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SoftModel.UI.Html.Controllers
{
    /// <summary>
    /// Controller required as the existing (.html) homepage was not
    /// subject to the Authorization flow and cannot participate in the 
    /// simple SSO implementation.
    /// </summary>
    public class HomeController : Controller
    {
        // GET: /Home/
        public ActionResult Index()
        {
            return View();
        }

    }
}

﻿using log4net;
using Raven.Client;
using SoftModel.Query;
using SoftModel.Schema;
using SoftModel.Schema.Query;
using SoftModel.UI.Html.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Http;

namespace SoftModel.UI.Html.Controllers
{
    //[Authorize]
    [ExceptionHandlingAttribute]
    public class ReadController : BaseApiController
    {
        private IQuery query;
        private ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ReadController(IDocumentSession documentSession, IQuery query) : base(documentSession) {
            this.query = query;
        }

        [HttpGet]
        public Record GetById([FromUri] GetCriteria criteria)
        {
            // TODO. Validate criteria

            // We currently retrieve the record from the op store as we need to provide the callers
            // with the latest eTag for optimistic concurrency, and to retrieve the recordSchema below.
            var record = RetrieveOr404IfNotFound<Record>(criteria.Id, "Record not found");
            // var eTag = DocumentSession.Advanced.GetEtagFor(record);
            var eTag = new Guid(DocumentSession.Advanced.GetEtagFor(record).ToByteArray());

            // We also need to retrieve the full record schema for the readModel
            var recordDescriptor = RetrieveOr404IfNotFound<RecordDescriptor>(record.RecordDescriptorRef.RecordDescriptorId,
                "RecordSchema not found");

            // Don't return inactive / deleted records
            if (!record.Queryable)
            {
                BadRequest("Invalid record.", Properties.Resources.error_record_not_available);
            }

            // We're using the read model to retrieve even single records. Without be so pure about CQRS
            // we could actually decide to serve single record fetches from the op store and save "query" 
            // operations from the read model
            var recordToReturn = query.Get(criteria.Id, MergeSystemFields(criteria.Fields, recordDescriptor), recordDescriptor);
            recordToReturn.Etag = eTag;

            return recordToReturn;
        }

        /// <summary>
        /// Query the readmodel for one or more records identified by their record descriptor
        /// </summary>
        /// <param name="id">The record descriptor id</param>
        /// <param name="page">Current page number</param>
        /// <param name="asAtDate">Point in time filter</param>
        /// <returns></returns>
        [HttpGet]
        public List<Record> GetByRecordSchema([FromUri]ByRecordSchemaCriteria criteria)
        {            
            // TODO. Validate criteria. Important - need to validate the fields passed in via the Fields param
            var asAtDate = criteria.AsAtDate.HasValue ? criteria.AsAtDate : DateTime.Now;

            // Validate record schema
            var recordSchema = Retrieve<RecordDescriptor>(criteria.Id.ToRavenIdFromQs());
            if (recordSchema == null)
            {
                BadRequest("RecordSchemaId was invalid", "invalid schema");
            }

            var recordTypeQuery = new RecordTypeQuery
            {
                RecordDescriptor = recordSchema,
                PageNum = criteria.Page,
                PageSize = criteria.PageSize,
                Filter = criteria.Filter,
                // TODO. Batshit crazy workaround for json serializer string array issue
                Fields = MergeSystemFields(criteria.Fields, recordSchema),
                AsAtDate = asAtDate,
                OrderBy = criteria.OrderBy
            };

            return query.ForRecordsOfType(recordTypeQuery);
        }

        /// <summary>
        /// Impl note: See below, we return 404 when we request a single item by id
        /// We return BadRequest when criteria are invalid.
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [HttpGet]
        public List<Record> GetAssociated([FromUri]AssociatedRecordCriteria criteria)
        {
            // string id, string childType, int page, DateTime? asAtDate
            // TODO. Validate criteria. Important - need to validate the fields passed in via the Fields param
            var asAtDate = criteria.AsAtDate.HasValue ? criteria.AsAtDate : DateTime.Now;
            var parentId = criteria.Id;//.ToRavenIdFromQs();

            // Validate record instance
            var record = Retrieve<Record>(parentId);
            if (record == null)
            {
                BadRequest("Record id was invalid", "invalid record id");
            }

            // Validate parent record schema
            var childRecordDescriptor = Retrieve<RecordDescriptor>(criteria.ChildType.ToRavenIdFromQs());
            if (childRecordDescriptor == null)
            {
                BadRequest("Record descriptor was invalid", "invalid record descriptor");
            }

            // TODO. Inject Query instance
            var recordTypeQuery = new AssociatedRecordsQuery
            {
                RecordDescriptor = DocumentSession.Load<RecordDescriptor>(record.RecordDescriptorRef.RecordDescriptorId),
                AssociatedRecordDescriptor = childRecordDescriptor,
                ParentId = parentId,
                PageNum = criteria.Page,
                PageSize = criteria.PageSize,
                Filter = criteria.Filter,
                Fields = MergeSystemFields(criteria.Fields, childRecordDescriptor),
                AsAtDate = asAtDate
            };

            return query.AssociatedRecords(recordTypeQuery);
        }

        /// <summary>
        /// Fields we need returned are:
        /// 1) Those specified in the DisplayFields (extracted from the users dashboard settings
        /// 2) Others required by the system (underlying operational record id)
        /// </summary>
        /// <param name="recordTypeQuery"></param>
        private string MergeSystemFields(string fields, RecordDescriptor recordDescriptor)
        {
            var systemFields = "opId,lastModified";
            StringBuilder sb = new StringBuilder();
            
            if (string.IsNullOrWhiteSpace(fields))
            {
                sb.AppendFormat("{0}", systemFields);
                
                // no fields? default to all displayable fields
                var all = string.Join(",", recordDescriptor.AllDisplayableFieldNames());
                if (!string.IsNullOrWhiteSpace(all))
                {
                    sb.AppendFormat(",{0}", all);
                }
            }
            else
            {
                sb.AppendFormat("{0},", systemFields);
                sb.Append(fields);
            }

            return sb.ToString();
        }
    }
}

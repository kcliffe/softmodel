﻿using System.Net.Http;
using Raven.Client;
using SoftModel.UI.Html.Validation;
using SoftModel.UI.Html.ViewModels.UI;
using System.Web.Http;
using SoftModel.UI.Html.ViewModels;
using System.Linq;
using SoftModel.UI.Html.Domain;

namespace SoftModel.UI.Html.Controllers
{
    //[Authorize]
    [ExceptionHandlingAttribute]
    public class DashboardController : BaseApiController
    {
        public DashboardController(IDocumentSession documentSession) : base(documentSession) { }

        // GET (by id)
        public UserDashboard Get()
        {
            var userName = "kellyc";
            var userDashboard = RetrieveOr404IfNotFound<UserDashboard>(userName, 
                string.Format(Properties.Resources.error_userdash_not_found, userName));

            return userDashboard;
        }

        // PUT  
        public HttpResponseMessage Put(UserDashboard userDashboard)
        {
            var instanceValidator = new UserDashboardValidator(DocumentSession);
            var results = instanceValidator.Validate(userDashboard);
            if (!results.IsValid)
            {
                BadRequest(results);
            }

            // Optimistic concurrency not required on UserDashboard
            DocumentSession.Store(userDashboard);

            var pathToResource = string.Format("dashboard/get", userDashboard.Id);
            return SaveChanges(userDashboard, pathToResource);
        }

        [HttpPut]
        public HttpResponseMessage PageComponent(PageComponent pageComponent)
        {
            var instanceValidator = new PageComponentValidator();
            var results = instanceValidator.Validate(pageComponent);
            if (!results.IsValid)
            {
                BadRequest(results);
            }

            string userName = "kellyc";
            var userDashboard = RetrieveOr404IfNotFound<UserDashboard>(userName,
                string.Format(Properties.Resources.error_userdash_not_found, userName));

            userDashboard.UpdatePageComponent(pageComponent);

            // Optimistic concurrency not required on UserDashboard
            DocumentSession.Store(userDashboard);

            var pathToResource = string.Format("dashboard/get", userDashboard.Id);
            return SaveChanges(userDashboard, pathToResource);
        }
    }
}

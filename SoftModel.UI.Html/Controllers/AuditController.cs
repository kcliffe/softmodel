﻿using Raven.Client;
using SoftModel.Query.Audit;
using SoftModel.UI.Html.Domain;
using System.Web.Http;

namespace SoftModel.UI.Html.Controllers
{
    //[Authorize]
    [ExceptionHandlingAttribute]
    public class AuditController : BaseApiController
    {
        private IAuditQuery auditQuery;

        public AuditController(IDocumentSession documentSession, IAuditQuery auditQuery) : base(documentSession) {
            this.auditQuery = auditQuery;
        }

        [HttpGet]
        public AuditSearchResult Search([FromUri]AuditFilter filter)
        {
            var results = auditQuery.AuditLogSearch(filter.OrderBy, filter.User, filter.Start, 
                filter.End, filter.PageNum, filter.PageSize, filter.EventType, filter.EntityName);
            return results;
        }
    }
}
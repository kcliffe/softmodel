﻿using log4net;
using Raven.Abstractions.Exceptions;
using Raven.Client;
using SoftModel.Schema;
using SoftModel.UI.Html.Plugins;
using System;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace SoftModel.UI.Html.Controllers
{
    /// <summary>
    /// SoftModel APIController classes should inherit from this.
    /// </summary>
    public class BaseApiController : ApiController
    {
        private static ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private const string Ver = "0.01a"; // TODO. Proper semantic version #
        private static string dbTennantName;

        protected static string defaultLang = "en-US";
        protected IDocumentSession DocumentSession;
        protected string preferredLanguage = defaultLang;

        public string Version
        {
            get { return Ver; }
        }

        public string CurrentUserName
        {
            get
            {
                return string.IsNullOrWhiteSpace(Thread.CurrentPrincipal.Identity.Name) ? "Guest" : Thread.CurrentPrincipal.Identity.Name;
            }
        }

        public bool CurrentLanguageIsDefault
        {
            get
            {
                return string.Compare(defaultLang, preferredLanguage, true, CultureInfo.InvariantCulture) == 0;
            }
        }

        /// <summary>
        /// Construct a new ApiController
        /// </summary>
        /// <param name="documentSession"></param>
        public BaseApiController(IDocumentSession documentSession)
        {
            if (string.IsNullOrEmpty(dbTennantName))
                dbTennantName = ConfigurationManager.AppSettings["tennantName"];

            DocumentSession = documentSession;
        }

        /// <summary>
        /// Execute a request asynchronously
        /// </summary>
        /// <param name="controllerContext"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override Task<HttpResponseMessage> ExecuteAsync(HttpControllerContext controllerContext, CancellationToken cancellationToken)
        {
            EnsurePreferredLanguage(controllerContext.Request);

            return base.ExecuteAsync(controllerContext, cancellationToken);
        }

        #region Protected methods

        /// <summary>
        /// Retrieve a record given an Id
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        protected T Retrieve<T>(string id)
        {
            var retrieved = DocumentSession.Load<T>(id);

            return retrieved;
        }

        /// <summary>
        /// Retrieve a record given an Id. Return response code 404 if resource not found
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        protected T RetrieveOr404IfNotFound<T>(string id, string customMessage = "")
        {
            var retrieved = DocumentSession.Load<T>(id);

            Http404IfNotFound(retrieved, customMessage);
            if (retrieved is IHasETag)
            {
                var eTag = new Guid(DocumentSession.Advanced.GetEtagFor(retrieved).ToByteArray());
                (retrieved as IHasETag).Etag = eTag; // DocumentSession.Advanced.GetEtagFor(retrieved).Value
            }

            return retrieved;
        }

        /// <summary>
        /// Retrieve a record given a predicate. Return response code 404 if resource not found
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="where"></param>
        /// <returns></returns>
        protected T RetrieveOr404IfNotFound<T>(Func<T, bool> where, string customMessage = "")
        {
            var retrieved = DocumentSession.Query<T>().Where(where).FirstOrDefault();

            Http404IfNotFound(retrieved, customMessage);
            if (retrieved is IHasETag)
            {
                var eTag = new Guid(DocumentSession.Advanced.GetEtagFor(retrieved).ToByteArray());
                (retrieved as IHasETag).Etag = eTag;
            }

            return retrieved;
        }

        /// <summary>
        /// Return exception with status code 404 if the provided resource is null 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="retrieved"></param>
        /// <param name="customMessage"></param>
        protected void Http404IfNotFound<T>(T retrieved, string customMessage = "")
        {
            if (retrieved == null)
            {
                var resp = new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.NotFound,
                    ReasonPhrase = string.IsNullOrEmpty(customMessage) ? string.Format(Properties.Resources.error_not_found, typeof(T)) : customMessage
                };
                throw new HttpResponseException(resp);
            }
        }

        /// <summary>
        /// Throw an exception which results in an Http Conflict (409) if a resource is found
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="conflictFunc"></param>
        /// <param name="customMessage"></param>
        protected void ConflictIfExists<T>(Func<T, bool> conflictFunc, string customMessage = "")
        {
            var retrieved = DocumentSession.Query<T>().Where(conflictFunc).FirstOrDefault();

            if (retrieved != null)
            {
                var resp = new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.Conflict,
                    ReasonPhrase = string.IsNullOrEmpty(customMessage) ? string.Format(Properties.Resources.error_resource_exists, typeof(T)) : customMessage
                };
                throw new HttpResponseException(resp);
            }
        }

        /// <summary>
        /// Throw an exception resulting in an Http Not Found (404)
        /// </summary>
        /// <param name="message"></param>
        /// <param name="reason"></param>
        protected void NotFound(string message, string reason = "Not Found")
        {
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound)
            {
                Content = new StringContent(message),
                ReasonPhrase = reason
            });
        }

        /// <summary>
        /// Throw an exception resulting in an Http Bad Request (400) if the supplied function returns false
        /// </summary>
        /// <param name="badRequestFunc"></param>
        /// <param name="reason"></param>
        protected void BadRequest(Func<bool> badRequestFunc, string reason = "Bad Request")
        {
            if (!badRequestFunc())
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    ReasonPhrase = reason
                });
            }
        }

        /// <summary>
        /// Throw an exception resulting in an Http Bad Request (400) 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="reason"></param>
        protected void BadRequest(string message, string reason = "Bad Request")
        {
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent(message),
                ReasonPhrase = reason
            });
        }

        /// <summary>
        /// Throw an HttpException (bad request) where the HttpResponseException.Content contains a collection of validation errors
        /// </summary>
        /// <param name="message"></param>
        /// <param name="reason"></param>        
        protected void BadRequest(FluentValidation.Results.ValidationResult results)
        {
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("Validation failed: " + string.Join(",", results.Errors)),
                ReasonPhrase = "Critical Exception"
            });
        }

        /// <summary>
        /// Persist an object to the operational store. Results in an Http conflict (409) response if the resource etag does not match.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="resource"></param>
        /// <param name="pathToResource"></param>
        /// <returns></returns>
        protected HttpResponseMessage SaveChanges<T>(T resource, string pathToResource)
        {
            try
            {
                DocumentSession.SaveChanges();

                // Http PUT response must return URI to created / updated resource                                
                var response = Request.CreateResponse(HttpStatusCode.Created, resource);
                response.Headers.Location = new Uri(Request.RequestUri, pathToResource);
                return response;
            }
            catch (ConcurrencyException)
            {
                var response = Request.CreateResponse(HttpStatusCode.Conflict, resource);
                return response;
            }
        }

        /// <summary>
        /// Return the PluginExecutionContext for the current request
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="targetBefore"></param>
        /// <param name="targetAfter"></param>
        /// <returns></returns>
        protected PluginExecutionContext<T> GetExecutionContext<T>(T targetBefore, T targetAfter)
        {
            return new PluginExecutionContext<T>(targetBefore, targetAfter, this.DocumentSession)
            {
                Log = Log
            };
        }

        #endregion

        #region Private Methods

        private void EnsurePreferredLanguage(HttpRequestMessage request)
        {
            if (request.Headers.AcceptLanguage != null && request.Headers.AcceptLanguage.Any())
            {
                var lang = request
                    .Headers
                    .AcceptLanguage
                    .Where(header => header.Value != null)
                    .OrderByDescending(x => x.Quality);

                if (lang.Any())
                {
                    preferredLanguage = lang.First().Value;
                }
            }
        }

        #endregion
    }
}
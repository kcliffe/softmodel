﻿using Raven.Client;
using SoftModel.Schema.Security;
using SoftModel.UI.Html.Domain;
using System.Net.Http;
using System.Web.Http;

namespace SoftModel.UI.Html.Controllers
{
    //[Authorize]
    [ExceptionHandlingAttribute]
    public class AccessController : BaseApiController
    {
        //public AccessController() {}

        public AccessController(IDocumentSession documentSession) : base(documentSession) { }

        [HttpGet]
        public void Auth()
        {
            // method currntly a No-op but used a trigger for basic auth challenge
        }

        [HttpGet]
        public string Ping()
        {
            return base.Version; 
        }

        // api/v1/access/user/put
        [HttpPut]
        public HttpResponseMessage AddUser(User user)
        {
            return null;
        }
    }
}
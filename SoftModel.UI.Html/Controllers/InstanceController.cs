﻿using Newtonsoft.Json.Linq;
using Omu.ValueInjecter;
using Raven.Abstractions.Linq;
using Raven.Client;
using SoftModel.Messages;
using SoftModel.Schema;
using SoftModel.UI.Html.Domain;
using SoftModel.UI.Html.Plugins;
using SoftModel.UI.Html.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Raven.Abstractions.Data;
using log4net;

namespace SoftModel.UI.Html.Controllers
{
    //[Authorize]
    [ExceptionHandlingAttribute]
    [InstanceControllerConfig]
    public class InstanceController : BaseApiController
    {
        private RecordEventListener recordEventListener;
        private static ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public InstanceController(IDocumentSession documentSession,
            RecordEventListener recordEventListener)
            : base(documentSession)
        {
            this.recordEventListener = recordEventListener;
        }

        /// <summary>
        /// Saves an instance of a record.
        /// </summary>
        /// <param name="record"></param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Put(Record record)
        {
            // Validate. May throw BadRequest
            ValidateRecordInstance(record);

            var isCreate = true;
            Record recordBefore = record;
            if (record.HasId)
            {
                recordBefore = DocumentSession.Load<Record>(record.Id);
                isCreate = recordBefore == null;
            }

            var recordDescriptor = DocumentSession.Load<RecordDescriptor>(record.RecordDescriptorRef.RecordDescriptorId);
            record.RecordDescriptorRef.EnsureValid(recordDescriptor);

            // if update then merge
            if (!isCreate)
            {
                // Merge field values. Guess this is the same as UpdateFromModel in MVC.
                record = recordBefore.InjectFrom(record) as Record;
            }
            else
            {
                // TODO. Could move to the Record class.
                EnsureDefaultFieldValues(record, recordDescriptor);
            }

            // Execute before save plugins
            if (BeforeSave(recordBefore, record, isCreate))
            {
                var eTag = new Etag(record.Etag.ToString());
                DocumentSession.Store(record, eTag);

                Log.DebugFormat("Record {0} {1}", record.Id, isCreate ? "Created" : "Updated");
            }
            else
            {
                // TODO. According to HTTP spec error should be?
                base.BadRequest("Plugin execution failed", "Plugin execution error");
            }

            // Save and create resource response
            var pathToResource = string.Format("instance/getbyid?id={0}", record.Id);
            var responseMessage = SaveChanges(record, pathToResource);

            // Execute after save plugins
            AfterSave(recordBefore, record, isCreate);

            // TODO. Move to plugin (async) execution
            // SendItemUpdatedMessage(record.RecordDescriptorRef, record.Id);

            return responseMessage;
        }

        [HttpPut]
        public HttpResponseMessage PutChild(Record childRecord)
        {
            var record = childRecord;
            var parentRefName = childRecord.ParentRecordRefFieldName;

            var instanceValidator = new RecordValidator(DocumentSession);
            var results = instanceValidator.Validate(record);
            if (!results.IsValid)
            {
                BadRequest(results);
            }

            // TODO. We need to validate a whole bunch of stuff here. Including that value not null, documentlinkfield is not empty..
            // Add validation for ChildRecord and move it all there.
            // Validate the existance of the parent.
            var parentRefField = record.FieldValue(parentRefName);
            if (parentRefField == null)
            {
                BadRequest("The parent relationship field was not found on the child record.");
            }

            var isCreate = true;
            Record recordBefore = record;
            if (record.HasId)
            {
                recordBefore = DocumentSession.Load<Record>(record.Id);
                isCreate = recordBefore == null;
            }

            var recordDescriptor = DocumentSession.Load<RecordDescriptor>(record.RecordDescriptorRef.RecordDescriptorId);
            record.RecordDescriptorRef.EnsureValid(recordDescriptor);

            // Massive HACK. Any dynamic value is deserializing as a JObject regardless of the type info we
            // place into the serialized copy. Need to possibly look at strongly typed fields! WE can't
            // use a generic, there are more serious serialization issues then.
            Record parent = DocumentSession.Load<Record>(parentRefField.Value);
            if (parent == null)
                BadRequest("Parent reference is invalid");

            // if update then merge
            if (!isCreate)
            {
                // Merge field values. Guess this is the same as UpdateFromModel in MVC.
                record = recordBefore.InjectFrom(record) as Record;

                // Complete hack while we are using Json and strongly typed entities
                HackRecordReferenceValue(record, parentRefName);
            }
            else
            {
                // TODO. Could move to the Record class.
                EnsureDefaultFieldValues(record, recordDescriptor);
            }

            if (BeforeSave(recordBefore, record, isCreate))
            {
                record.LastModified = DateTime.Now; // possibly won't have to set this on the op model. It's a column on the read model.
                DocumentSession.Store(record, new Etag(record.Etag.ToString()));

                #region  Add a pointer to the child to the parents reference collection
                FieldValue relationshipField = parent.FieldValue(parentRefField.ParentFieldName);
                var fieldValueList = relationshipField.Value as DynamicList;

                if (relationshipField.Value != null)
                {
                    if (relationshipField.Value is JArray)
                    {
                        var list = relationshipField.Value.ToObject<List<string>>();
                        list.Add(record.Id);
                        relationshipField.Value = list;
                    }
                    else
                    {
                        List<string> list = new List<string>();
                        fieldValueList.ToList().ForEach(x =>
                        {
                            list.Add((string)x);
                        });
                        list.Add(record.Id);
                        relationshipField.Value = list;
                    }
                }
                #endregion

                DocumentSession.Store(parent);
            }

            var pathToResource = string.Format("read/getbyid?id={0}", record.Id);
            var responseMessage = SaveChanges(record, pathToResource);

            AfterSave(recordBefore, record, isCreate, true);

            return responseMessage;
        }

        #region Private methods

        /// <summary>
        /// Validate the correctness of a record instance supplied to the API. Throws HTTP BadRequest if validation fails.
        /// </summary>
        /// <param name="record"></param>
        private void ValidateRecordInstance(Record record)
        {
            var instanceValidator = new RecordValidator(DocumentSession);
            var results = instanceValidator.Validate(record);
            if (!results.IsValid)
            {
                BadRequest(results);
            }
        }

        /// <summary>
        /// Notify of changes to the entity before save. Synchronous handlers can prevent the store operation.
        /// </summary>
        /// <param name="record"></param>
        /// <param name="isCreate"></param>
        /// <returns></returns>
        private bool BeforeSave(Record recordBefore, Record recordAfter, bool isCreate)
        {
            var executionContext = this.GetExecutionContext<Record>(recordBefore, recordAfter);

            return recordEventListener.NotifySyncBefore<Record>(executionContext, CurrentUserName);
        }

        /// <summary>
        /// Notify of changes to the entity after save (store)
        /// </summary>
        /// <param name="record"></param>
        /// <param name="isCreate"></param>
        private void AfterSave(Record recordBefore, Record recordAfter, bool isCreate, bool isChild = false)
        {
            SoftModelEventBase recordCreatedEvent;
            var executionContext = this.GetExecutionContext<Record>(recordBefore, recordAfter);

            if (isCreate)
            {
                recordCreatedEvent = new SoftModelRecordCreated(recordAfter, CurrentUserName, isChild);
            }
            else
            {
                recordCreatedEvent = new SoftModelRecordUpdated(recordAfter, CurrentUserName);
            }

            if (recordEventListener.NotifySyncAfter<Record>(executionContext, CurrentUserName))
            {
                recordEventListener.NotifyAsync(recordCreatedEvent);
            }
        }

        /// <summary>
        /// Ensure at least default values are present on the record for all fields. This is a requirement of the 
        /// read models query generator currently.
        /// </summary>
        /// <param name="record"></param>
        private void EnsureDefaultFieldValues(Record record, RecordDescriptor recordDescriptor)
        {
            // TODO .. pre this we need to resolve id if it's there using Query<> by name ... 
            // TODO EnsureDefaultFieldValues on instancecontroller should be unit testable component.            
            recordDescriptor.FieldDescriptors.ToList().ForEach(fd =>
            {
                if (record.FieldValue(fd.Name) == null)
                {
                    var defaultValue = Type.GetType(fd.SystemTypeName).GetDefaultValue(fd.FriendlyType);

                    record.Fields.Add(new FieldValue
                    {
                        FieldName = fd.Name,
                        Value = defaultValue ?? null // Any value not currently returned by GetDefaultValue above will be .... null
                    });
                }
            });
        }

        private void HackRecordReferenceValue(Record record, string parentRefFieldName)
        {
            var parentRefField = record.FieldValue(parentRefFieldName);
            if (parentRefField != null)
            {
                parentRefField.Value = parentRefField.Value;
            }
        }

        #endregion
    }
}
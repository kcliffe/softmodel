﻿using SoftModel.Schema;
using SoftModel.UI.Html.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace SoftModel.UI.Html.Controllers
{
    [ExceptionHandlingAttribute]
    public class MetadataController : ApiController
    {
        public List<FieldType> GetFieldTypes()
        {
            return Global.FieldTypes.ToList();
        }
        
        public List<string> GetFieldTypeNames()
        {
            var fieldTypes = Global.FieldTypes.Select(fieldType => fieldType.FriendlyTypeName.ToString());
            return fieldTypes.ToList();
        }
    }
}

﻿using DeepCloning;
using NServiceBus;
using Omu.ValueInjecter;
using Raven.Abstractions.Data;
using Raven.Client;
using Raven.Client.Linq;
using SoftModel.Messages;
using SoftModel.Schema;
using SoftModel.UI.Html.Domain;
using SoftModel.Schema.Survey;
using SoftModel.UI.Html.Indexes;
using SoftModel.UI.Html.Plugins;
using SoftModel.UI.Html.Validation;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;

namespace SoftModel.UI.Html.Controllers
{
    /// <summary>
    /// A survey is a simply a RecordSchema .. ya know... in the end this controller may be the RecordSchemaController
    /// </summary>
    [ExceptionHandlingAttribute]
    public class SurveyController : BaseApiController
    {
        private RecordEventListener recordEventListener;
        private IBus bus;

        public SurveyController(IDocumentSession documentSession,
            RecordEventListener recordEventListener) 
            : base(documentSession)
        {
            this.recordEventListener = recordEventListener;

            // TODO. Setup IOC container
            Configure.Serialization.Json();
            bus = Configure.With()
            .DefaultBuilder()
            .DefiningMessagesAs(msg => msg.Namespace == "SoftModel.Messages")
            .UseTransport<Msmq>()
            .UnicastBus()
            .SendOnly();
        }

        [HttpGet]
        public RecordDescriptor GetById(string surveyId)
        {
            // nothing special here
            return RetrieveOr404IfNotFound<RecordDescriptor>(surveyId);
        }

        [HttpGet]
        public Survey GetByName(string name)
        {
            return RetrieveOr404IfNotFound<Survey>(survey => survey.Name == name);
        }

        [HttpGet]
        public List<SurveySearchResult> Search(string q)
        {
            List<SurveySearchResult> schemas;

            if (string.IsNullOrWhiteSpace(q))
            {
                schemas = DocumentSession.Query<SurveySearchResult, Survey_Search>()
                .Where(s => 1 == 1)
                .AsProjection<SurveySearchResult>()
                .ToList();
            }
            else
            {
                schemas = DocumentSession.Query<SurveySearchResult, Survey_Search>()
                .Where(s => s.Name.StartsWith(q))
                .AsProjection<SurveySearchResult>()
                .ToList();
            }

            return schemas;
        }

        [HttpPut]
        public HttpResponseMessage Put(Survey survey)
        {
            var surveyValidator = new SurveyValidator(DocumentSession);
            var results = surveyValidator.Validate(survey);
            if (!results.IsValid)
            {
                BadRequest(results);
            }

            // Default an Id if one one not provided
            bool idAssigned;
            RecordHelpers.AssignSchemaIdIfNotSet(survey, out idAssigned);

            var surveyBefore = DocumentSession.Load<Survey>(survey.Id);
            if (idAssigned && surveyBefore != null)
                BadRequest("An id was not set, so one was assigned. But a schema with that Id exists.");

            var isCreate = surveyBefore == null;
            Survey existing = null;

            // Update internal ItemType field (could do in RavenDb behavior)            
            // survey.UpdateFieldTypes();

            if (!isCreate)
            {
                // Deep clone the exsting record. We need a copy for change tracking later
                // and the copy we receive from the session above is merged with the incoming record
                existing = new Survey().InjectFrom<FastDeepCloneInjection>(surveyBefore) as Survey;

                // Merge field values. Guess this is the same as UpdateFromModel in MVC.
                survey = surveyBefore.InjectFrom(survey) as Survey;
            }
            else
            {
                // Protect against duplicates. This could be a validation thing but it's more
                // business logic and we'll want to throw a specific exception
                ConflictIfExists<Survey>(x => x.Name == survey.Name);
            }

            //if (BeforeSave(recordDescriptor, null))
            //{
            DocumentSession.Store(survey, new Etag(survey.Etag.ToString()));
            //}

            var pathToResource = string.Format("surveys/getbyname?name={0}", survey.Name);
            var responseMessage = SaveChanges(survey, pathToResource);

            if (isCreate)
            {
                AfterSave(survey, survey, isCreate);
            }
            //else
            //{
            //    AfterSave(existing, recordDescriptor, isCreate);
            //}

            return responseMessage;
        }

        [HttpPut]
        public bool Publish(SurveyPublishInfo publishInfo)
        {
            // We won't be creating survey responses until users login... 
            // Just do a fake "notify" via the bus, yeah baby, instant async
            // For now break messages into a per group scope..because...actually we'd probably 
            // want a much finer breakdown of recipients so we don't overload the mail server
            publishInfo.GroupIdentifiers.ForEach( grp=>
            bus.Send(
                new SurveyPublishedEvent { 
                    GroupIdentifier = grp,
                    SurveyId = publishInfo.SurveyId
                })
            );

            return true;
        }

        /// <summary>
        /// Notify of changes to the entity after save (store)
        /// </summary>
        /// <param name="record"></param>
        /// <param name="isCreate"></param>
        private void AfterSave(Survey surveyBefore, Survey surveyAfter, bool isCreate)
        {
            SoftModelEventBase schemaEvent;

            var executionContext = this.GetExecutionContext<Survey>(surveyBefore, surveyAfter);

            if (isCreate)
            {
                schemaEvent = new SurveyCreatedEvent(surveyBefore, CurrentUserName);
                recordEventListener.NotifyAsync(schemaEvent);
            }
            //else
            //{
                //List<SchemaChange> schemaUpdates = null;
                //ListComparer<Question> fieldDescriptorComparer = new ListComparer<Question>();
                //if (!fieldDescriptorComparer.Equals(surveyBefore.Questions.ToList(), surveyAfter.Questions.ToList()))
                //{
                //    // The field descriptor collection has changed. We need to determine what changes have been made
                //    schemaUpdates = GenerateSchemaUpdateCollection(surveyBefore, surveyAfter);
                //}

                //schemaEvent = new SurveyCreatedEvent(surveyAfter, CurrentUserName);
            //}

            //if (recordEventListener.NotifySyncBefore<Survey>(executionContext, CurrentUserName))
            //{
                
            //}
        }
    }
}
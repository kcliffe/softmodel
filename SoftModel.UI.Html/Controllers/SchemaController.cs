﻿using DeepCloning;
using Omu.ValueInjecter;
using Raven.Abstractions.Data;
using Raven.Client;
using Raven.Imports.Newtonsoft.Json;
using SoftModel.Messages;
using SoftModel.Schema;
using SoftModel.UI.Html.Domain;
using SoftModel.UI.Html.Indexes;
using SoftModel.UI.Html.Plugins;
using SoftModel.UI.Html.Validation;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Http;

namespace SoftModel.UI.Html.Controllers
{
    //[Authorize]
    [ExceptionHandlingAttribute]
    public class SchemaController : BaseApiController
    {
        private RecordEventListener recordEventListener;

        public SchemaController(IDocumentSession documentSession,
            RecordEventListener recordEventListener)
            : base(documentSession)
        {
            this.recordEventListener = recordEventListener;
        }

        [HttpGet]
        public RecordDescriptor GetById(string id)
        {
            return RetrieveOr404IfNotFound<RecordDescriptor>(id);
        }

        [HttpGet]
        public RecordDescriptor GetByName(string name)
        {
            return RetrieveOr404IfNotFound<RecordDescriptor>(recordSchema => recordSchema.Name == name);
        }

        [HttpGet]
        public List<SchemaSearchResult> Search(string q)
        {
            List<SchemaSearchResult> schemas;

            if (string.IsNullOrWhiteSpace(q))
            {
                schemas = DocumentSession.Query<SchemaSearchResult, Schema_Search>()
                .AsProjection<SchemaSearchResult>()
                .ToList();
            }
            else
            {
                schemas = DocumentSession.Query<SchemaSearchResult, Schema_Search>()
                .Where(s => s.Label.StartsWith(q))
                .AsProjection<SchemaSearchResult>()
                .ToList();
            }

            return schemas;
        }

        [HttpGet]
        public List<string> GetSchemaFields([FromUri]FieldCriteria filter)
        {
            var instanceValidator = new FieldCriteriaValidator();
            var results = instanceValidator.Validate(filter);
            if (!results.IsValid)
            {
                BadRequest(results);
            }

            var recordDescriptor = RetrieveOr404IfNotFound<RecordDescriptor>(recordSchema
                => recordSchema.Name == filter.RecordDescriptorName);

            Func<FieldDescriptor, bool> displayPred;
            if (filter.Display.HasValue)
            {
                displayPred = (fd) => { return fd.Display == filter.Display; };
            }
            else
            {
                displayPred = (fd) => { return true; };
            }

            var fields = recordDescriptor.FieldDescriptors.Where(displayPred);
            return fields.Select(x => x.Name).ToList();
        }

        [HttpGet]
        public List<EnumerationSearchResult> SearchEnums(string q)
        {
            // TODO. Paging... not a problem at the backend... but need something reusable on the
            // front end.
            List<EnumerationSearchResult> enums;

            if (string.IsNullOrWhiteSpace(q))
            {
                enums = DocumentSession.Query<EnumerationSearchResult, Enum_Search>()
                .Where(s => 1 == 1)
                .AsProjection<EnumerationSearchResult>()
                .ToList();
            }
            else
            {
                enums = DocumentSession.Query<EnumerationSearchResult, Enum_Search>()
                .Where(s => s.Name.StartsWith(q))
                .AsProjection<EnumerationSearchResult>()
                .ToList();
            }

            return enums;
        }

        /// <summary>
        /// According to HTML1.1, PUT can CREATE an entity at a URI (return 201) or UPDATE (return 200)                
        /// We'll also return 409 (conflict) where schema is new and resource exists.  
        /// 
        /// Note: without an explicit transaction:
        /// If before save plugins fail, the schema is not saved
        /// If the schema save fails, the plugin changes are not rolled back
        /// As above if the after save plugins fail.
        /// </summary>    
        [HttpPut]
        public HttpResponseMessage Put(RecordDescriptor recordDescriptor)
        {
            var instanceValidator = new RecordSchemaValidator(DocumentSession);
            var results = instanceValidator.Validate(recordDescriptor);
            if (!results.IsValid)
            {
                BadRequest(results);
            }

            // Default an Id if one one not provided
            bool idAssigned;
            RecordHelpers.AssignSchemaIdIfNotSet(recordDescriptor, out idAssigned);

            var recordDescriptorBefore = DocumentSession.Load<RecordDescriptor>(recordDescriptor.Id);
            if (idAssigned && recordDescriptorBefore != null)
                BadRequest("An id was not set, so one was assigned. But a schema with that Id exists.");

            var isCreate = recordDescriptorBefore == null;
            RecordDescriptor existing = null;

            // Update internal ItemType field (could do in RavenDb behavior)            
            recordDescriptor.UpdateFieldTypes();

            if (!isCreate)
            {
                // Deep clone the exsting record. We need a copy for change tracking later
                // and the copy we receive from the session above is merged with the incoming record
                existing = new RecordDescriptor().InjectFrom<FastDeepCloneInjection>(recordDescriptorBefore) as RecordDescriptor;

                // Merge field values. Guess this is the same as UpdateFromModel in MVC.
                recordDescriptor = recordDescriptorBefore.InjectFrom(recordDescriptor) as RecordDescriptor;
            }
            else
            {
                // Protect against duplicates. This could be a validation thing but it's more
                // business logic and we'll want to throw a specific exception
                ConflictIfExists<RecordDescriptor>(x => x.Name == recordDescriptor.Name);
            }

            if (BeforeSave(recordDescriptor, null))
            {
                DocumentSession.Store(recordDescriptor, new Etag(recordDescriptor.Etag.ToString()));
            }

            var pathToResource = string.Format("schema/getbyname?name={0}", recordDescriptor.Name);
            var responseMessage = SaveChanges(recordDescriptor, pathToResource);

            if (isCreate)
            {
                AfterSave(recordDescriptor, recordDescriptor, isCreate);
            }
            else
            {
                AfterSave(existing, recordDescriptor, isCreate);
            }

            return responseMessage;
        }

        /// <summary>
        /// Export selected record descriptors and all related entities. Note. This a PUT operation 
        /// although it's debatable as we don't modify a resource that we can return the address of.
        /// </summary>
        /// <param name="schemaNames">The names of the RecorDescriptors to export.</param>
        /// <returns></returns>
        [ActionName("Export")]
        [HttpPut]
        public async Task<string> Export(List<string> schemaNames)
        {
            // Validate schema names
            BadRequest(() => { return schemaNames != null && schemaNames.Any(); }, "");

            // Create a unique temporary filename 
            string exportFileName = DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss-ff");
            string exportFolder = ConfigurationManager.AppSettings["SchemaImportExportPath"];

            using (var exportFile = File.CreateText(Path.Combine(HostingEnvironment.MapPath(exportFolder), string.Format("{0}.sme", exportFileName))))
            {
                var exporter = new ImportExport(DocumentSession);

                var invalidNames = new List<string>();
                var content = exporter.GetRecordDescriptorsForExport(schemaNames, out invalidNames);

                if (invalidNames.Any())
                {
                    BadRequest(string.Join(",", invalidNames));
                }

                await exportFile.WriteAsync(JsonConvert.SerializeObject(content));
            }

            return exportFileName;
        }

        [HttpGet]
        public async Task<List<ImportExport.ImportCandidate>> GetExportedFileList()
        {
            var t = Task<List<ImportExport.ImportCandidate>>.Factory.StartNew(() =>
            {
                string exportFolder = ConfigurationManager.AppSettings["SchemaImportExportPath"];

                var export = new ImportExport(DocumentSession);
                return export.GetFilesForImport(HostingEnvironment.MapPath(exportFolder));
            });

            await t;
            return t.Result;
        }

        [ActionName("Import")]
        [HttpPut]
        public async Task<ImportExport.ImportResult> Import(List<string> fileNames)
        {
            BadRequest(() => fileNames.Any(), Properties.Resources.error_filenames_required);

            // For now, handle only the first import file
            var importer = new ImportExport(DocumentSession);
            var content = string.Empty;

            var importPath = Path.Combine(HostingEnvironment.MapPath(ConfigurationManager.AppSettings["SchemaImportExportPath"]), fileNames.First());
            using (var file = File.OpenText(importPath))
            {
                content = await file.ReadToEndAsync();
            }

            return importer.Import(importPath, content, false);
        }

        #region Private methods

        /// <summary>
        /// Notify of changes to the entity before save. Synchronous handlers can prevent the store operation.
        /// </summary>
        /// <param name="record"></param>
        /// <param name="isCreate"></param>
        /// <returns></returns>
        private bool BeforeSave(RecordDescriptor recordDescriptorBefore, RecordDescriptor recordDescriptorAfter)
        {
            var executionContext = this.GetExecutionContext<RecordDescriptor>(recordDescriptorBefore, recordDescriptorAfter);
            return recordEventListener.NotifySyncBefore<RecordDescriptor>(executionContext, CurrentUserName);
        }

        /// <summary>
        /// Notify of changes to the entity after save (store)
        /// </summary>
        /// <param name="record"></param>
        /// <param name="isCreate"></param>
        private void AfterSave(RecordDescriptor recordDescriptorBefore, RecordDescriptor recordDescriptorAfter, bool isCreate)
        {
            SoftModelEventBase schemaEvent;

            var executionContext = this.GetExecutionContext<RecordDescriptor>(recordDescriptorBefore, recordDescriptorAfter);

            if (isCreate)
            {
                schemaEvent = new SoftModelSchemaCreatedEvent(recordDescriptorBefore, CurrentUserName);
            }
            else
            {
                List<SchemaChange> schemaUpdates = null;
                ListComparer<FieldDescriptor> fieldDescriptorComparer = new ListComparer<FieldDescriptor>();
                if (!fieldDescriptorComparer.Equals(recordDescriptorBefore.FieldDescriptors.ToList(), recordDescriptorAfter.FieldDescriptors.ToList()))
                {
                    // The field descriptor collection has changed. We need to determine what changes have been made
                    schemaUpdates = GenerateSchemaUpdateCollection(recordDescriptorBefore, recordDescriptorAfter);
                }

                schemaEvent = new SoftModelSchemaUpdatedEvent(recordDescriptorAfter, schemaUpdates, CurrentUserName);
            }

            if (recordEventListener.NotifySyncBefore<RecordDescriptor>(executionContext, CurrentUserName))
            {
                recordEventListener.NotifyAsync(schemaEvent);
            }
        }

        protected List<SchemaChange> GenerateSchemaUpdateCollection(RecordDescriptor recordDescriptorBefore, RecordDescriptor recordDescriptorAfter)
        {
            var result = new List<SchemaChange>();

            foreach (var afterFieldDescriptor in recordDescriptorAfter.FieldDescriptors)
            {
                var beforeFieldDescriptor = recordDescriptorBefore
                    .FieldDescriptors
                    .FirstOrDefault(x => x.Name == afterFieldDescriptor.Name);

                if (beforeFieldDescriptor == null)
                {
                    result.Add(new SchemaChange(SchemaChangeType.AddField, afterFieldDescriptor));
                    continue;
                }

                #region TODO
                // TODO. Do we care about Label change? Name is immutable on FieldDescriptor. If we create the 
                // read model sql column to match (so it's also immutable) and then map name -> label on query
                // we're sweet.
                //if (beforeFieldDescriptor.Label != afterFieldDescriptor.Label)
                //{
                //    result.Add(new SchemaChange(SchemaChangeType.RenameField
                //}
                #endregion

                if (beforeFieldDescriptor.IsActive != afterFieldDescriptor.IsActive)
                {
                    result.Add(new SchemaChange(SchemaChangeType.DisableField, beforeFieldDescriptor.Name));
                }
            }

            return result;
        }

        #endregion
    }
}

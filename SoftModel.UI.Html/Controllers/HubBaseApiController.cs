﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Raven.Client;
using SoftModel.UI.Html.Domain;
using System;

namespace SoftModel.UI.Html.Controllers
{
    /// <summary>
    /// Base for api controllers that expose async operations via signalr
    /// </summary>
    [ExceptionHandlingAttribute]
    public class HubBaseApiController<THub> : BaseApiController where THub : IHub
    {
        public HubBaseApiController(IDocumentSession documentSession) : base(documentSession) { }

        Lazy<IHubContext> hub = new Lazy<IHubContext>(
            () => GlobalHost.ConnectionManager.GetHubContext<THub>()
        );

        protected IHubContext Hub
        {
            get { return hub.Value; }
        }
    }
}
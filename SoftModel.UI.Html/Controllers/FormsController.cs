﻿using Raven.Client;
using Raven.Client.Linq;
using SoftModel.Schema;
using SoftModel.UI.Html.Domain;
using SoftModel.UI.Html.Indexes;
using SoftModel.UI.Html.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace SoftModel.UI.Html.Controllers
{
    //[Authorize]
    [ExceptionHandlingAttribute]
    public class FormsController : BaseApiController
    {
        //public FormsController() { }

        public FormsController(IDocumentSession documentSession) : base(documentSession) { }

        [HttpGet]
        public List<FormDefinitionSearchResult> Search(string q)
        {
            var schemas = DocumentSession.Query<FormDefinitionSearchResult, Form_Search>()
               .Where(f => f.Name.StartsWith(q))
               .AsProjection<FormDefinitionSearchResult>()
               .ToList();

            return schemas;
        }

        [HttpGet]
        public RuntimeFormDefinitionViewModel GetByName(string name)
        {
            var formDef = DocumentSession.Query<FormDefinition>()
                .Where(fd => fd.Name == name)
                .Include(x => x.RecordDescriptorReference.RecordDescriptorId)
                .FirstOrDefault();

            Http404IfNotFound(formDef);

            // Load recordSchema
            // ReSharper disable PossibleNullReferenceException
            var recordDescriptor = DocumentSession.Load<RecordDescriptor>(formDef.RecordDescriptorReference.RecordDescriptorId);
            // ReSharper restore PossibleNullReferenceException

            var viewModel = new RuntimeFormDefinitionViewModel(formDef, recordDescriptor);

            // Post processing on the ViewModel. 
            // TODO: Move this functionality to a Factory. Also, possibly instead of hardcoding post proc actions like this
            // look at adding "dependancies" to the form description. Dependancies would be added during form design.
            foreach(var fd in recordDescriptor.FieldDescriptorsOfType(FriendlyTypeName.Enumeration))
            {              
                var enums = DocumentSession.Load<Enumeration>(fd.EnumerationId);
                var field = viewModel.Fields.FirstOrDefault(vfd => vfd.Name == fd.Name);
                if (field != null)
                    field.Enum = enums;
            };

            return viewModel;
        }
    }
}

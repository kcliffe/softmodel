﻿using System;

namespace SoftModel.UI.Html.Controllers
{
    public class AuditFilter
    {
        public string User { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public int PageNum { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
        public string EventType { get; set; }
        public string EntityName { get; set; }
    }
}

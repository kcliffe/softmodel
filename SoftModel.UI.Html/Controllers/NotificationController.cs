﻿using log4net;
using Microsoft.AspNet.SignalR;
using Raven.Client;
using SoftModel.UI.Html.Domain;
using SoftModel.UI.Html.ViewModels;
using System.Web.Http;

namespace SoftModel.UI.Html.Controllers
{
    /// <summary>
    /// Notify interested clients of state changes in our data model via signalr.
    /// </summary>
    [ExceptionHandlingAttribute]
    public class NotificationController : HubBaseApiController<NotificationHub>
    {
        private static ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public NotificationController(IDocumentSession documentSession) : base(documentSession) { }

        [HttpPut]
        public string Notify(NotificationMsg itemUpdatedMessage)
        {
            Hub.Clients.All.itemUpdated(itemUpdatedMessage);
            Log.DebugFormat("Sending notification {0}", itemUpdatedMessage);

            return "ok";
        }
    }
}

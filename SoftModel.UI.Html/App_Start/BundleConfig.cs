﻿using System.Web;
using System.Web.Optimization;

namespace SoftModel.UI.Html
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/scripts")
                .Include("~/Scripts/jquery/jquery-1.9.1.js")
                .Include("~/Scripts/angular/angular.js")
                .Include("~/Scripts/angular/angular-ui.js")
                .Include("~/Scripts/angular/ui-bootstrap-0.2.0.js")
                .Include("~/Scripts/toastr.js")
                .Include("~/Scripts/beautify.js")
                .Include("~/Scripts/services/localize.js")
                .Include("~/Scripts/services/service.js")
                .Include("~/Scripts/codemirror-3.12/lib/codemirror.js")
                .Include("~/Scripts/codemirror-3.12/mode/javascript/javascript.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery/jquery.unobtrusive*",
                        "~/Scripts/jquery/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css")
                .Include("~/content/custom.css")
                .Include("~/content/bootstrap.css")
                .Include("~/content/bootstrap-responsive.css")
                .Include("~/Content/toastr.css")
                .Include("~/Content/site.css")
                .Include("~/Content/angular-ui.css")
                .Include("~/Scripts/codemirror-3.12/theme/monokai.css")
                .Include("~/Scripts/codemirror-3.12/lib/codemirror.css"));
        }
    }
}
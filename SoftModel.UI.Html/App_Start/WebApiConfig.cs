﻿using SoftModel.UI.Html.Auth;
using SoftModel.UI.Html.Domain;
using System.IdentityModel.Tokens;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;
using System.Web.Mvc;

namespace SoftModel.UI.Html
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "DefaultApiPutDefault",
                routeTemplate: "api/v1/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional, action = "Put" },
                constraints: new { id = @"\d+", httpMethod = new HttpMethodConstraint(HttpMethod.Put) }
            );

            // Modified to allow for non standard http verbs
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/v1/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional, action = RouteParameter.Optional }
            );
        }       
    }
}
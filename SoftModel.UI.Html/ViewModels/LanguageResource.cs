﻿using System.Collections.Generic;

namespace SoftModel.UI.Html.ViewModels
{
    public class LanguageResource
    {        
        /// <summary>
        /// Localized strings
        /// </summary>
        public HashSet<LanguageEntry> Entries { get; set; }
    }
}
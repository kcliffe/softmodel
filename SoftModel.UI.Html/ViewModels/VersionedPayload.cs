﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoftModel.UI.Html.ViewModels
{
    /// <summary>
    /// A dto which contains an object that has a version used in optimistic concurrency scenario's.
    /// </summary>
    /// <remarks>
    /// This is currently required because RavenDb and the Web API both use json.net for serialization. RavenDb attributes (like ETag)
    /// we don't want to serialize are marked up with [JsonIgnore]. This however prevents the attribute being round-tripped to the client
    /// and back to the controller.
    /// 
    /// Hence we need to extract the etag from the entity into the payload. 
    /// </remarks>
    /// <typeparam name="T"></typeparam>
    public class VersionedPayload<T>
    {
        private T Payload;
        private Guid ETag;

        public VersionedPayload(T payload, Guid etag)
        {
            this.Payload = payload;
            this.ETag = etag;
        }        
    }
}
﻿namespace SoftModel.UI.Html.Controllers
{
    public class FieldCriteria
    {
        public string RecordDescriptorName { get; set; }

        /// <summary>
        /// Filter to Display fields.
        /// </summary>
        public bool? Display { get; set; }
    }
}

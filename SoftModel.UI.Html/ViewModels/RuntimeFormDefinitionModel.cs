﻿using SoftModel.Schema;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;

namespace SoftModel.UI.Html.ViewModels
{
    /// <summary>
    /// UI friendly representation of a form definition.
    /// </summary>
    public class RuntimeFormDefinitionViewModel
    {
        public FormDefinition FormDefinition { get; set; }

        public List<dynamic> Fields { get; set; }

        public string RecordType { get; set; }

        public RuntimeFormDefinitionViewModel() { }

        public RuntimeFormDefinitionViewModel(FormDefinition formDefinition,
            Schema.RecordDescriptor recordDescriptor)
        {
            FormDefinition = formDefinition;
            RecordType = FormDefinition.RecordDescriptorReference.RecordDescriptorName;

            Fields = new List<dynamic>();
            recordDescriptor.FieldDescriptors.ToList().ForEach(fd => {
                dynamic field = new ExpandoObject(); 
                field.Name = fd.Name;
                field.Label = fd.Label;
                field.Description = fd.Description;
                field.FriendlyType = fd.FriendlyType;
                field.Display = fd.Display;
                field.ReadOnly = fd.ReadOnly;
               
                Fields.Add(field);
            });
        }
    }
}
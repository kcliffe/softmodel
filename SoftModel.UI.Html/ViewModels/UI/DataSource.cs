﻿using System.Collections.Generic;
using SoftModel.Schema;

namespace SoftModel.UI.Html.ViewModels.UI
{
    public class Datasource
    {
        /// <summary>
        /// TODO. Could be derived from the Record/RecordSchemaRef
        /// </summary>
        public string RecordSchemaId { get; set; }

        /// <summary>
        /// The records comprising the data set.
        /// </summary>
        public List<Record> Data { get; set; }
    }
}

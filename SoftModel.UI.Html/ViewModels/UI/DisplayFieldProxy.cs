﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoftModel.UI.Html.ViewModels.UI
{
    public class DisplayFieldProxy
    {
        public string FieldDescriptorName { get; set; }
        public string DisplayField { get; set; }
    }
}
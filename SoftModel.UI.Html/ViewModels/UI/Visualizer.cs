﻿namespace SoftModel.UI.Html.ViewModels.UI
{
    public class Visualizer
    {
        /// <summary>
        /// Settings for this type of Visualizer 
        /// </summary>
        public Settings Settings { get; set; }

        /// <summary>
        /// Type e.g. Tabular, PieChart // TODO Maybe enum
        /// </summary>
        public string Type { get; set; }
    }
}

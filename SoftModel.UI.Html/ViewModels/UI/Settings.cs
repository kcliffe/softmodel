﻿using System.Collections.Generic;

namespace SoftModel.UI.Html.ViewModels.UI
{
    public class Settings
    {                
        /// <summary>
        /// Logical order clause eg. "{col desc}"
        /// </summary>
        public string OrderBy { get; set; }

        /// <summary>
        /// The filter to be applied to the underlying records
        /// </summary>
        public string Filter { get; set; }

        /// <summary>
        /// A list of FieldDefinition Id's / names representing the columns to display 
        /// </summary>
        public List<DisplayFieldProxy> DisplayFields { get; set; }

        /// <summary>
        /// The type of records that will be displayed
        /// </summary>
        public string RecordDescriptorName { get; set; }

        /// <summary>
        /// Form shown on create
        /// </summary>
        public string FormCreateDescriptionName { get; set; }
    }
}

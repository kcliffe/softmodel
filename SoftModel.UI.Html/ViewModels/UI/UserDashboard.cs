﻿using System.Collections.Generic;
using System.Linq;
using Omu.ValueInjecter;

namespace SoftModel.UI.Html.ViewModels.UI
{
    public class UserDashboard
    {
        // Username
        public string Id { get; set; }

        // Each view represents a "tab" in the dashboard. Create as many as you like...
        public List<DashboardView> Views { get; set; }

        public enum LayoutType
        {
            Grid2By2,
            Grid4By2
        }

        public LayoutType DashboardLayoutType { get; set; }

        internal void UpdatePageComponent(PageComponent pageComponent)
        {
            var view = this.Views.First(v => v.PageComponents.Any(pg => pg.Equals(pageComponent.DataviewId)));
            var pageComponentToUpdate = view.PageComponents.First(pg => pg.Equals(pageComponent.DataviewId));
            pageComponentToUpdate.InjectFrom(pageComponent);
        } 
    }
}

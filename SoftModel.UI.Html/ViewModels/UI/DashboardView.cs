﻿using System.Collections.Generic;

namespace SoftModel.UI.Html.ViewModels.UI
{
    public class DashboardView
    {
        /// <summary>
        /// Displayed in the dashboard tab
        /// </summary>
        public string DashboardName { get; set; }

        /// <summary>
        /// The PageComponents which render information into the UserDashboard. 
        /// </summary>
        public List<PageComponent> PageComponents { get; set; }
    }
}

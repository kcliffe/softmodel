﻿using System;
using System.Linq;

namespace SoftModel.UI.Html.ViewModels.UI
{
    public class PageComponent
    {
        /// <summary>
        /// Id of the dataview the PageComponent is rendered within
        /// </summary>
        public string DataviewId { get; set; }

        /// <summary>
        /// Is this the parent of another pageComponent?
        /// </summary>
        public bool HasChild { get; set; }

        /// <summary>
        /// Id of the parent dataview of the PageComponent
        /// </summary>
        public string ChildOf { get; set; }

        /// <summary>
        /// Title of the tab containing the PageComponent
        /// </summary>
        public string TabTitle { get; set; }

        /// <summary>
        /// The component which will render the data in the Datasource, e.g. Tabular, pie etc
        /// </summary>
        public Visualizer Visualizer { get; set; }

        /// <summary>
        /// Placeholder for data which will be downloaded asynchronously
        /// </summary>
        public string Data { get; set; }

        /// <summary>
        /// If set, denotes the created / lastModified date of the data presented
        /// </summary>
        public DateTime? AsAtDate { get; set; }

        /// <summary>
        /// Determines if the pagecomponent data is dirty. Will be set by the SignalR Notification hub.
        /// </summary>
        public bool IsDirty { get; set; }

        /// <summary>
        /// The type of records that will be displayed
        /// </summary>
        public string RecordDescriptorName { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            PageComponent p = obj as PageComponent;
            if (p == null)
            {
                return false;
            }

            return p.DataviewId == DataviewId;
        }

        public bool Equals(PageComponent pageComp)
        {
            if (pageComp == null)
            {
                return false;
            }

            return pageComp.DataviewId == DataviewId;
        }
    }
}
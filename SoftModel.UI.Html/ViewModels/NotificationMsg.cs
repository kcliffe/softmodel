﻿namespace SoftModel.UI.Html.ViewModels
{
    public class NotificationMsg
    {
        public string RecordDescriptorName { get; set; }
        public string ItemId { get; set; }

        public override string ToString()
        {
            return string.Format("Descriptor {0}, Item Id {1}", RecordDescriptorName, ItemId);
        }
    }
}
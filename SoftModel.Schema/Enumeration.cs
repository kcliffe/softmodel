﻿using System.Collections.Generic;

namespace SoftModel.Schema
{
    public class Enumeration : SchemaBase, IHasETag
    {
        public string Name { get; set; }
        public List<Option> Options { get; set; }
        public System.Guid Etag { get; set; }

        public Enumeration(string name)
        {
            Name = name;
            Options = new List<Option>();
        }
    }
}
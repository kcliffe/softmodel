﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftModel.Schema
{
    public class FieldTypeRef
    {
        public string FieldTypeName { get; set; }
        public Type ItemType { get; set; }
    }
}

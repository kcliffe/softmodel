﻿using System;
using System.Collections.Generic;

namespace SoftModel.Schema.Security
{
    public class Group : SchemaBase, IHasETag
    {
        public string Name { get; set; }
        public List<UserRef> UserRefs { get; set; }
        public Guid Etag { get; set; }
    }
}

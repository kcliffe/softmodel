﻿using System;
using System.Collections.Generic;

namespace SoftModel.Schema.Security
{
    public class User : SchemaBase, IHasETag
    {
        public string Name { get; set; }
        public string UserName { get; set; }
        public List<GroupRef> GroupRefs { get; set; }
        public Guid Etag { get; set; }
    }
}

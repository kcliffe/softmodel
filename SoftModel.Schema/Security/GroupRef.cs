﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftModel.Schema.Security
{
    public class GroupRef
    {
        public string GroupId { get; set; }
        public string GroupName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftModel.Schema.Security
{
    public class Role : SchemaBase, IHasETag
    {
        public Guid Etag {get;set;}
        public string Name { get; set; }
    }
}

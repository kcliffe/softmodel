﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftModel.Schema.Security
{
    public class UserRef
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using RavenIgnore = Raven.Imports.Newtonsoft.Json.JsonIgnoreAttribute;

namespace SoftModel.Schema
{
    /// <summary>
    /// A collection of FieldValues. Equivalent to a row in a relational store.
    /// </summary>        
    public class Record : SchemaBase, IHasETag
    {
        public DateTime Created { get; set; }

        public DateTime LastModified { get; set; }

        /// <summary>
        /// Reference to the "Schema" that defines the record
        /// </summary>
        public RecordDescriptorRef RecordDescriptorRef { get; set; }

        /// <summary>
        /// Record is included in queries
        /// </summary>
        [JsonIgnore]
        public bool IsActive { get; set; }

        /// <summary>
        /// Record is deleted (soft) and could be physically removed.
        /// </summary>
        [JsonIgnore]
        public bool IsDeleted { get; set; }

        [JsonIgnore]
        [RavenIgnore]
        public bool HasId
        {
            get
            {
                return !string.IsNullOrWhiteSpace(Id);
            }
        }

        [JsonIgnore]
        public bool Queryable
        {
            get
            {
                return !IsDeleted && IsActive;
            }
        }

        /// <summary>
        /// One or more runtime field values
        /// </summary>
        public List<FieldValue> Fields { get; set; }

        /// <summary>
        /// Version
        /// </summary> 
        [RavenIgnore]
        public Guid Etag { get; set; }

        public Record()
        {
            Created = DateTime.Now;
            Fields = new List<FieldValue>();
            // trailing / means RavenDb should generate an Id in the form "records/UniqueId"
            // Id = recordIdPrefix;
            IsActive = true;
            IsDeleted = false;
        }

        public bool HasFieldValue(string fieldName)
        {
            return Fields.FirstOrDefault(f => f.FieldName == fieldName) != null;
        }

        public FieldValue FieldValue(string fieldName)
        {
            var field = Fields.FirstOrDefault(f => f.FieldName == fieldName);
            return field;
        }

        public T FieldValue<T>(string fieldName)
        {
            var field = Fields.FirstOrDefault(f => f.FieldName == fieldName);
            return (T)field.Value;
        }

        public int FieldValueAsInt(string fieldName)
        {
            var field = Fields.FirstOrDefault(f => f.FieldName == fieldName);
            if (field == null)
            {
                throw new Exception(string.Format("Field {0} not found on {1}", fieldName, RecordDescriptorRef.RecordDescriptorName));
            }

            if (!field.GetType().IsAssignableFrom(typeof(int)))
            {
                throw new Exception(String.Format("Cannot cast {0} to {1}", field.GetType().Name, typeof(int).Name));
            }

            return (int)field.Value;
        }

        public string ParentRecordRefFieldName { get; set; }

        public string ParentRecordDescriptorName { get; set; }

        public string GetParentId()
        {
            return Fields.First(r => r.FieldName == ParentRecordRefFieldName).Value;
        }
    }
}
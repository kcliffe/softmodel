﻿namespace SoftModel.Schema.Expressions
{
    public class Rule
    {
        public string Expression { get; set; }

        public string SourceFieldName { get; set; }

        public string TargetRecord { get; set; }

        public string TargetField { get; set; }

        public Rule(string expression)
        {
            Expression = expression;
        }
    }
}

﻿using System.Collections.Generic;

namespace SoftModel.Schema.Expressions
{
    public class RuleSet : SchemaBase
    {
        public List<Rule> Rules { get; set; }

        public RuleSet()
        {
            Rules = new List<Rule>();
        }

        public void AddRule(Rule rule)
        {
            Rules.Add(rule);
        }
    }
}

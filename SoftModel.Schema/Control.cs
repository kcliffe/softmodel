﻿using System.Collections.Generic;

namespace SoftModel.Schema
{
    public class Control
    {
        public string FieldName { get; set; }
        public Dictionary<string, dynamic> Properties { get; set; }
    }
}

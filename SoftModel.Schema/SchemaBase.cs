﻿using Newtonsoft.Json;
using System;
using RavenIgnore = Raven.Imports.Newtonsoft.Json.JsonIgnoreAttribute;

namespace SoftModel.Schema
{
    public class SchemaBase
    {
        protected const string recordIdPrefix = "records/";

        /// <summary>
        /// Unique identifier
        /// </summary>        
        public string Id { get; set; }

        /// <summary>
        /// Name of the record. Immutable
        /// </summary>
        public string Name { get; set; }

        [JsonIgnore]
        [RavenIgnore]
        public bool IsTransient
        {
            get { return String.IsNullOrWhiteSpace(Id) || Id.Equals(recordIdPrefix); }
        }   
    }
}

﻿using System.Collections.Generic;

namespace SoftModel.Schema
{
    /// <summary>
    /// From: http://social.msdn.microsoft.com/Forums/vstudio/en-US/f86cbd38-3672-4d66-a317-8d626165d125/how-do-i-generate-a-hashcode-from-a-object-array-in-c
    /// Compares two arrays to see if the values inside of the array are the same. This is
    /// dependent on the type contained in the array having a valid Equals() override.
    /// </summary>
    /// <typeparam name="T">The type of data stored in the array</typeparam>
    public class ListComparer<T> : IEqualityComparer<List<T>>
    {
        /// <summary>
        /// Gets the hash code for the contents of the array since the default hash code
        /// for an array is unique even if the contents are the same.
        /// </summary>
        /// <remarks>
        /// See Jon Skeet (C# MVP) response in the StackOverflow thread 
        /// http://stackoverflow.com/questions/263400/what-is-the-best-algorithm-for-an-overridden-system-object-gethashcode
        /// </remarks>
        /// <param name="list">The array to generate a hash code for.</param>
        /// <returns>The hash code for the values in the array.</returns>
        public int GetHashCode(List<T> list)
        {
            // if non-null array then go into unchecked block to avoid overflow
            if (list != null)
            {
                unchecked
                {
                    int hash = 17;

                    // get hash code for all items in array
                    foreach (var item in list)
                    {
                        hash = hash * 23 + ((item != null) ? item.GetHashCode() : 0);
                    }

                    return hash;
                }
            }

            // if null, hash code is zero
            return 0;
        }

        /// <summary>
        /// Compares the contents of both arrays to see if they are equal. This depends on 
        /// typeparameter T having a valid override for Equals().
        /// </summary>
        /// <param name="firstList">The first array to compare.</param>
        /// <param name="secondList">The second array to compare.</param>
        /// <returns>True if firstArray and secondArray have equal contents.</returns>
        public bool Equals(List<T> firstList, List<T> secondList)
        {
            // if same reference or both null, then equality is true
            if (object.ReferenceEquals(firstList, secondList))
            {
                return true;
            }

            // otherwise, if both arrays have same length, compare all elements
            if (firstList != null && secondList != null &&
                (firstList.Count == secondList.Count))
            {
                for (int i = 0; i < firstList.Count; i++)
                {
                    // if any mismatch, not equal
                    if (!object.Equals(firstList[i], secondList[i]))
                    {
                        return false;
                    }
                }

                // if no mismatches, equal
                return true;
            }

            // if we get here, they are not equal
            return false;
        }
    }
}

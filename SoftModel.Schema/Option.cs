﻿namespace SoftModel.Schema
{
    public class Option
    {
        public int Value { get; set; }
        public string Label { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace SoftModel.Schema
{
    public class ImportHistory
    {
        public string FilePath { get; set; }
        public DateTime Impored { get; set; }
    }
}

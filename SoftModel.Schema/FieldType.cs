﻿using System;

namespace SoftModel.Schema
{
    /// <summary>
    /// FieldDefinitions are constrained to using a set of these types. 
    /// </summary>
    public class FieldType
    {
        public string FriendlyTypeName { get; private set; }
        public string SystemTypeName { get; private set; }

        public FieldType(string friendlyTypeName, string systemType)
        {
            FriendlyTypeName = friendlyTypeName;
            SystemTypeName = systemType;
        }
    }
}

﻿using System.Linq;

namespace SoftModel.Schema
{
    /// <summary>
    /// Not using inheritance as we don't want a sepeate Raven collection for query purposes.
    /// </summary>
    public class ChildRecord
    {
        public Record Record { get; set; }

      
    }
}

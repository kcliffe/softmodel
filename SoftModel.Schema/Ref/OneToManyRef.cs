﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftModel.Schema.Ref
{
    public class OneToManyRef : IRef
    {
        public Type SoftModelType { get; set; }
        public List<string> Ids { get; set; }        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftModel.Schema.Ref
{
    interface IRef
    {
        /// <summary>
        /// Referenced  type
        /// </summary>
        Type SoftModelType { get; set; }

        /// <summary>
        /// Id's of the referenced types
        /// </summary>
        List<string> Ids { get; set; }
    }
}

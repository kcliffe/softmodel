﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SoftModel.Schema;

namespace SoftModel.Schema
{
    public class Global
    {
        private static List<FieldType> fieldTypes;

        static Global()
        {
            if (fieldTypes == null)
            {
                fieldTypes = new List<FieldType> 
                {
                    new FieldType(FriendlyTypeName.String,typeof(string).ToString()),
                    new FieldType(FriendlyTypeName.Integer,typeof(int).ToString()),
                    new FieldType(FriendlyTypeName.Date,typeof(DateTime).ToString()),
                    new FieldType(FriendlyTypeName.Decimal,typeof(decimal).ToString()),
                    new FieldType(FriendlyTypeName.Double,typeof(double).ToString()),
                    new FieldType(FriendlyTypeName.Boolean,typeof(bool).ToString()),
                    new FieldType(FriendlyTypeName.Enumeration,typeof(Enumeration).ToString()),
                    new FieldType(FriendlyTypeName.OneToMany,typeof(string).ToString()),
                    new FieldType(FriendlyTypeName.OneToOne,typeof(string).ToString()),
                    new FieldType(FriendlyTypeName.ManyToMany,typeof(IList<string>).ToString())
                };
            }
        }

        public static List<FieldType> FieldTypes
        {
            get { return fieldTypes; }
        }
    }

    public static class FriendlyTypeName
    {
        public const string Date = "Date";
        public const string Integer = "Integer";
        public const string String = "String";
        public const string Decimal = "Decimal";
        public const string Double = "Double";
        public const string Boolean = "Boolean";
        public const string Enumeration = "Enumeration";
        public const string OneToOne = "One To One";
        public const string OneToMany = "One To Many";
        public const string ManyToMany = "Many To Many";
    }
}
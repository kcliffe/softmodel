﻿using System;

namespace SoftModel.Schema
{
    public interface IHasETag
    {        
        Guid Etag { get; set; } 
    }
}

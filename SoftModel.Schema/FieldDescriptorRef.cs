﻿using System;

namespace SoftModel.Schema
{    
    public class FieldDescriptorRef
    {
        public string FieldName { get; set; }
        
        // Can probably remove this. The type is Value.GetType()!
        public string SystemTypeName { get; set; }

        public string FriendlyType { get; set; }
    }
}

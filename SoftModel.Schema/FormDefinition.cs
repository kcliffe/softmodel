﻿using System;
using System.Collections.Generic;

namespace SoftModel.Schema
{
    public class FormDefinition : SchemaBase, IHasETag
    {        
        public string Name { get; set; }

        public RecordDescriptorRef RecordDescriptorReference { get; set; }

        public List<Control> Controls { get; set; }

        public Guid Etag { get; set; }
    }
}

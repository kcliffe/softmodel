﻿using System;
using System.Collections.Generic;

namespace SoftModel.Schema.Query
{
    public class GetCriteria
    {
        public string Id { get; set; }

        public string Fields { get; set; }
    }

    public class ByRecordSchemaCriteria
    {
        public string Id {get;set;}
            
        public int Page {get;set;}

        public DateTime? AsAtDate { get; set; }

        public string Fields { get; set; }

        public string Filter { get; set; }

        public string OrderBy { get; set; }

        public int PageSize { get; set; }
    }

    public class AssociatedRecordCriteria : ByRecordSchemaCriteria
    {
        public string ChildType { get; set; }
    }
}

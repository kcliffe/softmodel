﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SoftModel.Schema
{
    public enum SchemaType
    {
        Schema,
        Survey
    }

    /// <summary>
    /// Defines the "schema" of a "record". (Design Time). We could (almost) get away with using runtime type checking 
    /// but there are places in the application where it's simpler to check the static information recorded here.
    /// </summary>
    public class RecordDescriptor : SchemaBase, IHasETag
    {
        // public bool Published { get; set; }

        /// <summary>
        /// Display label
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// Fields that comprise the record
        /// </summary>
        public List<FieldDescriptor> FieldDescriptors { get; set; } 

        /// <summary>
        /// Concurrency check
        /// </summary>
        public Guid Etag { get; set; }

        /// <summary>
        /// RecordDescriptor is available to be used. TODO. In read model we will need to check this flag.
        /// </summary>
        public bool IsActive { get; set; }

        public SchemaType Type { get; set; }

        /// <summary>
        /// Create an insance of a RecordDescriptor
        /// </summary>
        public RecordDescriptor()
        {
            FieldDescriptors = new List<FieldDescriptor>();
        }

        /// <summary>
        /// Update the internal field types - typically after a client PUT operation.
        /// </summary>
        /// <remarks>
        /// Note the lack of error handling. This operation expects that the API has performed
        /// this validation.</remarks>
        public void UpdateFieldTypes()
        {
            this.FieldDescriptors.ToList().ForEach(fd => fd.SystemTypeName = Global.FieldTypes.First(ft => ft.FriendlyTypeName == fd.FriendlyType).SystemTypeName);
        }

        /// <summary>
        /// Return all field decriptors matching the supplied friendlyType
        /// </summary>
        /// <param name="friendlyType"></param>
        /// <returns></returns>
        public List<FieldDescriptor> FieldDescriptorsOfType(string friendlyType)
        {
            return new List<FieldDescriptor>(this.FieldDescriptors.Where(fd => fd.FriendlyType == friendlyType));
        }

        public RecordDescriptorRef ToRef()
        {
            return new RecordDescriptorRef
            {
                RecordDescriptorId = Id,
                RecordDescriptorName = Name
            };
        }

        public FieldDescriptor GetFieldDescriptor(string fieldName)
        {
            if (this.FieldDescriptors.Any())
            {
                var desc = this.FieldDescriptors.FirstOrDefault(x => x.Name == fieldName);
                if (desc != null)
                {
                    return desc;
                }
            }

            // TODO. Custom
            throw new Exception(string.Format("Field {0} does not exist on {1}", fieldName, Name));
        }

        public IEnumerable<string> AllDisplayableFieldNames()
        {
            return this.FieldDescriptors.Where(fd => fd.DisplayInGrid).Select(fd => fd.Name);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftModel.Schema
{
    public class RecordSchemaReference
    {
        public string RecordSchemaId { get; set; }

        public string RecordSchemaName { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace SoftModel.Schema
{
    /// <summary>
    /// Store all forms related to an record type as a convenience.
    /// The operational model uses this for example when exporting forms related to a record type.
    /// 
    /// The Id of the entity is the FormSet/RecordDescriptorId
    /// </summary>
    public class FormSet : SchemaBase
    {
        public IList<FormDefinition> Forms { get; set; }
    }
}

﻿using Raven.Imports.Newtonsoft.Json;
using System;

namespace SoftModel.Schema
{    
    public class RecordDescriptorRef
    {
        // In theory we don't need here Id, name should be unique
        public string RecordDescriptorId { get; set; }
        // TODO. Get rid...
        public string RecordDescriptorName { get; set; }

        public void EnsureValid(RecordDescriptor recordDescriptor)
        {
            this.RecordDescriptorId = recordDescriptor.Id;
            this.RecordDescriptorName = recordDescriptor.Name;
        }
    }
}

﻿using System.Collections.Generic;

namespace SoftModel.Schema
{
    /// <summary>
    /// When an item is imported, it is "staged" or unpublished, until an admistrator publishes
    /// the changes. There is only ever one unpublished image for a single RecordDescriptor.
    /// </summary>
    public class Unpublished
    {
        List<RecordDescriptor> RecordDescritpors { get; set; }
    }
}

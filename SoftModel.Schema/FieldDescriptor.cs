﻿using Raven.Imports.Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace SoftModel.Schema
{
    /// <summary>
    /// Describes a single "field" belonging to a <see cref="Record"/>
    /// </summary>
    public class FieldDescriptor
    {
        // Unique per recordSchema -> TODO. prepend the recordschema or store internal id to get system wide unique.
        public string Name { get; set; }

        // Default label shown on forms. TODO. Allow override on form defs 
        public string Label { get; set; }

        /// <summary>
        /// Denotes the type of the field. Can be inferred from FieldValue.Value property but having it here 
        /// is useful for type checking and other scenario's.
        /// </summary>
        public string SystemTypeName { get; set; }

        /// <summary>
        /// The friendly type name. Used while designing RecordDescriptors .e.g System.String -> String
        /// </summary>
        public string FriendlyType { get; set; }

        /// <summary>
        /// Can be used as a "prompt" on forms. E.g. What is your age?
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Does this display in method / list. TODO. Bitfield enum
        /// </summary>
        public bool Display { get; set; }

        /// <summary>
        /// Is the field editable in Forms?
        /// </summary>
        public bool ReadOnly { get; set; }

        public bool IsActive { get; set; }

        /// <summary>
        /// Should item be displayed in grids? Same as Display when we have bitfield enum
        /// </summary>
        public bool DisplayInGrid { get; set; }

        /// <summary>
        /// Field should belong to a subclass like EnumerationFieldDescriptor. Json deserialization issues.
        /// </summary>
        public string EnumerationId { get; set; }

        /// <summary>
        /// Field should belong to a subclass. Here due to Json deserialization issues.
        /// </summary>
        public string ParentFieldName { get; set; }

        /// <summary>
        /// Field should belong to a subclass. Here due to Json deserialization issues.
        /// </summary>
        public string RelatedRecordDescriptorId { get; set; }

        /// <summary>
        /// Options for all field types e.g.
        /// string -> min length, max length
        /// date -> min date, max date, default date
        /// 
        /// We could end up storing fields such as EnumerationId, ParentFieldName, RelatedRecordDescriptorId
        /// in here (the only reason they exist on this class seeing they only apply to specific 
        /// types is because of json's lack of type support)
        /// </summary>
        public IEnumerable<KeyValuePair<string, string>> Options { get; set; }

        public FieldDescriptor()
        {
            IsActive = true;
            ReadOnly = false;
            DisplayInGrid = false;
            Display = true;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is FieldDescriptor))
                return false;

            if (object.ReferenceEquals(this, obj))
            {
                return true;
            }

            var fd = (FieldDescriptor)obj;
            return this.Name == fd.Name && this.Label == fd.Label;
        }

        public override int GetHashCode()
        {
            if (string.IsNullOrEmpty(Name) || string.IsNullOrEmpty(Label))
                return base.GetHashCode();

            // TODO. Need containing schema to be unique!
            return Name.GetHashCode() + Label.GetHashCode();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftModel.Schema.Survey
{
    /// <summary>
    /// A survey *could* be modelled as a Record. With either:
    /// a) At least one FieldValue of type "survey"
    /// b) One or more FieldValue's that comprise a survey.
    /// c) A standard c# class with properties representing a typical survey
    /// d) As above with the addition of a collection of FieldValue objects allowing extension (if required)
    /// 
    /// Start with approach c since we probably want specialized processing and we want to illustrate
    /// that it's very simple to store any .net type using RavenDb.    
    /// 
    /// The current downside is that the currnet "publish" pipeline supports only the Record type - we'll need 
    /// to create a marker interface...
    /// </summary>
    public class Survey : SchemaBase, IHasETag
    {
        public string Description { get; set; }

        public DateTime Created { get; set; }

        public DateTime LastPublished { get; set; }

        public List<Question> Questions { get; set; }

        public Guid Etag { get; set; }

    }
}

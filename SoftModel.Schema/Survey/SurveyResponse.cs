﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftModel.Schema.Survey
{
    public class SurveyResponse
    {
        public string Id { get; set; }

        public Record Recipient { get; set; }

        public List<FieldValue> Responses { get; set; }
    }
}

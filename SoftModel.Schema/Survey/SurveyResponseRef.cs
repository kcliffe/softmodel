﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftModel.Schema.Survey
{
    /// <summary>
    /// A pointer to a survey response along with additional data which may be useful to API callers
    /// without requiring a read of the actual SurveyResponse.
    /// </summary>
    public class SurveyResponseRef
    {
        public string SurveyResponseId { get;set; }

        public string RecipientName { get; set; }

        public DateTime ResponseDate { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace SoftModel.Schema.Survey
{
    public class SurveyPublishInfo
    {
        public List<string> GroupIdentifiers { get; set; }

        public string SurveyId { get; set; }
    }
}

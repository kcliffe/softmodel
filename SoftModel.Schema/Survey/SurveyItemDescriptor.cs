﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftModel.Schema.Survey
{
    /// <summary>
    /// Represents a survey "question". Should probably be constrained to int, string, datetime etc or list of... e.g. no references to records.
    /// </summary>
    public class SurveyItemDescriptor : FieldDescriptor
    {
        /// <summary>
        /// The description displayed to the user
        /// </summary>
        public string Prompt { get; set; }
    }
}

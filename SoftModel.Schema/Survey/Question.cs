﻿namespace SoftModel.Schema.Survey
{
    public class Question
    {
        public string Name { get; set; }
        public string HelpText { get; set; }
        public string Type { get; set; }
        public bool IsActive { get; set; }
    }
}

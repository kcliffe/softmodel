﻿using Raven.Imports.Newtonsoft.Json;
using System;

namespace SoftModel.Schema
{
    /// <summary>
    /// No unique id 
    /// </summary>   
    public class FieldValue
    {
        public string FieldName { get; set; }
        public string ParentFieldName { get; set; }
        public dynamic Value { get; set; }
    }
}
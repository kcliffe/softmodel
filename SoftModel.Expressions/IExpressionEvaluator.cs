﻿using System;

namespace SoftModel.Expressions
{
    public interface IExpressionEvaluator
    {
        void Evaluate(ExpressionRecordProxy recordProxy);
    }
}

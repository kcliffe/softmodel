﻿using Microsoft.Scripting.Hosting;
using NLog;
using Raven.Client;
using SoftModel.Schema.Expressions;
using System;
using System.Runtime.Caching;

namespace SoftModel.Expressions
{
    /// <summary>
    /// Evaluates expressions attached to a record type using IronPython.
    /// </summary>
    public sealed class ExpressionEvaluator : IExpressionEvaluator, IDisposable
    {
        private static Logger Log = LogManager.GetCurrentClassLogger();
        private readonly ScriptEngine scriptEngine;
        private readonly IDocumentSession documentSession;
        private MemoryCache cache;
        private CacheItemPolicy cacheItemPolicy;

        /// <summary>
        /// ScriptEngine not an interface but injection means we can use the container
        /// to manage lifetime. May turn out we don't need this...
        /// </summary>
        /// <param name="scriptEngine"></param>
        /// <param name="documentSession"></param>
        public ExpressionEvaluator(IDocumentSession documentSession)
        {
            this.scriptEngine = IronPython.Hosting.Python.CreateEngine();
            this.documentSession = documentSession;
            this.cache = new MemoryCache("RuleSetCache");

            cacheItemPolicy = new CacheItemPolicy();
            cacheItemPolicy.SlidingExpiration = TimeSpan.FromMinutes(5);
        }

        /// <summary>
        /// TODO.... the expression will probably not contain the assignment..
        /// </summary>
        /// <param name="recordProxy"></param>
        /// <param name="expression"></param>
        public void Evaluate(ExpressionRecordProxy recordProxy)
        {
            var ruleSet = RuleSetFromCache(recordProxy.Record.RecordDescriptorRef.RecordDescriptorName);

            if (ruleSet == null)
                return;

            foreach (var rule in ruleSet.Rules)
            {
                Evaluate(recordProxy, rule);
            }
        }

        private void Evaluate(ExpressionRecordProxy recordProxy, Rule rule)
        {
            var localName = recordProxy.Record.RecordDescriptorRef.RecordDescriptorName;
            if (string.IsNullOrWhiteSpace(localName))
            {
                throw new ArgumentException("Record descriptor name is not set.");
            }

            var scope = scriptEngine.CreateScope();
            scope.SetVariable(localName, recordProxy);

            Log.Debug("Executing expression {0}", rule.Expression);

            var result = scriptEngine.Execute(rule.Expression, scope);
        }

        private RuleSet RuleSetFromCache(string recordDescriptorName)
        {
            var ruleSetId = string.Format("RuleSets/{0}", recordDescriptorName);
            var ruleSet = (RuleSet)cache.Get(ruleSetId);

            if (ruleSet == null)
            {
                ruleSet = documentSession.Load<RuleSet>(ruleSetId);
                if (ruleSet != null)
                {
                    cache.Add(new CacheItem(ruleSetId, ruleSet), cacheItemPolicy);

                    Log.Debug("RuleSet {0} retrieved and added to the cache", ruleSetId);
                }
            }
            
            return ruleSet;
        }

        public void Dispose()
        {
            this.cache.Dispose();
        }
    }
}

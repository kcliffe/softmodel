﻿using SoftModel.Schema;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftModel.Expressions
{
    /// <summary>
    /// A proxy between the expression evaluator and a Record.
    /// </summary>
    public class ExpressionRecordProxy : DynamicObject
    {
        private readonly Record record;

        public Record Record
        {
            get 
            {
                return record;
            }
        }

        public ExpressionRecordProxy(Record record)
        {
            this.record = record;
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            var field = Record.Fields.FirstOrDefault(x => x.FieldName == binder.Name);
            if (field != null)
            {
                // No type check. Exception will occur anyway we'll just trap it
                field.Value = value;
                return true;
            }

            return false;
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            if (binder.Name == "RecordName")
            {
                result = Record.RecordDescriptorRef.RecordDescriptorName;
                return true;
            }

            var field = Record.FieldValue(binder.Name);
            if (field == null)
            {
                result = null;
                return false;
            }

            result = field.Value;
            return true;
        }

        public int Count(string collectionName)
        {
            return 10;
        }
    }
}

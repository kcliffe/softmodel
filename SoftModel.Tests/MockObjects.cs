﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftModel.Schema;

namespace SoftModel.Tests
{
    public class MockObjects
    {
        public static RecordDescriptor GetRecordSchema(string recordSchemaId = null, string recordSchemaName = null)
        {
            var stringDef = new FieldDescriptor { SystemTypeName = typeof(string).ToString(), Name = "StringField", FriendlyType = "String" };
            var intDef = new FieldDescriptor { SystemTypeName = typeof(int).ToString(), Name = "IntField", FriendlyType = "Integer" };
            var intDef2 = new FieldDescriptor { SystemTypeName = typeof(int).ToString(), Name = "IntField2", FriendlyType = "Integer" };
            var decDef2 = new FieldDescriptor { SystemTypeName = typeof(decimal).ToString(), Name = "DecField", FriendlyType = "Decimal" };

            return new RecordDescriptor
            {
                Id = recordSchemaId ?? "RecordSchemaDescriptor/1",
                Name = recordSchemaName ?? "IntegratonTest",
                FieldDescriptors = new List<FieldDescriptor> { stringDef, intDef, intDef2, decDef2 }
            };           
        }

        public static Record GetRecord(string id = null)
        {
            var record = new Record() { Id = id ?? "record/1" };

            record.RecordDescriptorRef = new RecordDescriptorRef
            {
                RecordDescriptorId = "RecordSchemaDescriptor/1",
                RecordDescriptorName = "Order"
            };

            record.Fields.Add(new FieldValue
            {
                FieldDescriptorRef = new FieldDescriptorRef
                {
                    FieldName = "StringField",
                    FriendlyType = FriendlyTypeName.String,
                    SystemTypeName = typeof(String).ToString()
                },
                Value = "A String"
            });

            record.Fields.Add(new FieldValue
            {
                FieldDescriptorRef = new FieldDescriptorRef
                {
                    FieldName = "IntField",
                    FriendlyType = FriendlyTypeName.Integer,
                    SystemTypeName = typeof(int).ToString()
                },
                Value = 1
            });

            record.Fields.Add(new FieldValue
            {
                FieldDescriptorRef = new FieldDescriptorRef
                {
                    FieldName = "DecField",
                    FriendlyType = FriendlyTypeName.Decimal,
                    SystemTypeName = typeof(decimal).ToString()
                },
                Value = 1
            });

            return record;
        }

        public static List<Record> Records(int num)
        {
            var r = new List<Record>();

            for (int i = 0; i < num; i++)
            {
                var record = MockObjects.GetRecord();
                record.Id = i.ToString();
                r.Add(record);
            }

            return r;
        }

        public static RecordDescriptor ParentRecordDescriptor()
        {
            /* Create a record with a RecordRef */
            var parentFieldDescriptors = new List<FieldDescriptor> 
            {
                new FieldDescriptor 
                {                    
                    SystemTypeName = typeof(string).ToString(),
                    Name = "Name",
                    Description = "What is your name?"
                },
                new FieldDescriptor
                {
                    FriendlyType = "String",
                    SystemTypeName = typeof(List<RecordRef>).ToString(),
                    Name = "List of children",
                    Description = ""
                }
            };

            var parentRecordDescriptor = new RecordDescriptor
            {
                FieldDescriptors = parentFieldDescriptors,
                Id = "recordDescriptors/Parent",
                Name = "parent"
            };

            return parentRecordDescriptor;
        }

        public static Record ParentRecord()
        {
            // create a record instance
            var parent = new Record
            {
                Id = "records/Parent1",               
                RecordDescriptorRef = new RecordDescriptorRef
                {
                    RecordDescriptorId = "recordDescriptors/Parent",
                    RecordDescriptorName = "parent"
                },
                Fields = new List<FieldValue> 
                {
                    new FieldValue 
                    {
                        FieldDescriptorRef = new FieldDescriptorRef { FieldName = "Name", SystemTypeName=typeof(string).ToString()},
                        Value = "Bob"
                    },
                    new FieldValue 
                    {
                        FieldDescriptorRef = new FieldDescriptorRef { FieldName = "Parent", SystemTypeName=typeof(RecordRef).ToString()},
                        Value = new List<RecordRef> { new RecordRef { LinkedDocumentId = "records/Parent1" }}
                    }
                }
            };

            return parent;
        }

        public static RecordDescriptor ChildRecordDescriptor()
        {
            /* Create a record with a RecordRef */
            var childFieldDescriptors = new List<FieldDescriptor> 
            {
                new FieldDescriptor 
                {                    
                    SystemTypeName = typeof(string).ToString(),
                    Name = "Name",
                    Description = "What is your name?"
                },
                new FieldDescriptor
                {
                    FriendlyType = "Relationship",
                    SystemTypeName = typeof(RecordRef).ToString(),
                    Name = "Parent",
                    Description = ""
                }
            };

            var childRecordDescriptor = new RecordDescriptor
            {
                FieldDescriptors = childFieldDescriptors,
                Id = "recordDescriptors/Child1",
                Name = "child"
            };

            return childRecordDescriptor;
        }

        public static Record ChildRecord()
        {
            // create a record instance
            var childRecord = new Record
            {
                Id = "records/Child1",
                RecordDescriptorRef = new RecordDescriptorRef
                {
                    RecordDescriptorId = "recordDescriptors/Child1",
                },
                Fields = new List<FieldValue> 
                {
                    new FieldValue 
                    {
                        FieldDescriptorRef = new FieldDescriptorRef { FieldName = "Name", SystemTypeName=typeof(string).ToString()},
                        Value = "Bob"
                    },
                    new FieldValue 
                    {
                        FieldDescriptorRef = new FieldDescriptorRef { FieldName = "Parent", SystemTypeName=typeof(RecordRef).ToString()},
                        Value = new RecordRef{ LinkedDocumentId = "records/Parent1" }
                    }
                }
            };

            return childRecord;
        }

        public static Record GetRecordForExpressionTest()
        {
            var record = new Record() { Id = "record/1" };

            record.RecordDescriptorRef = new RecordDescriptorRef
            {
                RecordDescriptorId = "RecordSchemaDescriptor/1"
            };

            record.Fields.Add(new FieldValue
            {
                FieldDescriptorRef = new FieldDescriptorRef
                {
                    FieldName = "StringField",
                    SystemTypeName = typeof(String).ToString()
                },
                Value = "A String"
            });

            var temp = new FieldValue
            {
                FieldDescriptorRef = new FieldDescriptorRef
                {
                    FieldName = "Children",
                    SystemTypeName = typeof(List<Record>).ToString()
                },
                Value = new List<Record>()
            };
            temp.Value = Records(10);

            record.Fields.Add(temp);
            
            return record;
        }
    }
}

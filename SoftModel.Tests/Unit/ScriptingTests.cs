﻿using Ciloci.Flee;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roslyn.Scripting.CSharp;
using SoftModel.Expressions;

namespace SoftModel.Tests.Unit
{
    [TestClass]
    public class MyTestClass
    {
        [TestMethod]
        public void CanExecuteSimpleScriptWithRoslyn()
        {
            var engine = new ScriptEngine();
            engine.AddReference(typeof(MyTestClass).Assembly);
            engine.ImportNamespace("SoftModel.Tests.Unit");

            dynamic ep = new ExpressionRecordProxy(MockObjects.GetRecord());
            ep.IntField = 1;
            ep.Result = 5;

            var session = engine.CreateSession(ep);

            var ret = (int)session.Execute(@"RecordProxy.IntField = 1 + 2;");

            Assert.AreEqual<int>(ep.IntField, 3);
        }

        [TestMethod]
        public void CanExecuteSimpleScriptWithFlee()
        {
            dynamic ep = new ExpressionRecordProxy(MockObjects.GetRecord());
            ep.IntField = 1;

            ExpressionContext context = new ExpressionContext();
            // Allow the expression to use all static public methods of System.Math
            context.Imports.AddType(typeof(MyTestClass));
            context.Imports.AddType(typeof(System.Dynamic.DynamicObject));
            context.Variables["RecordProxy"] = ep;

            // Create a dynamic expression that evaluates to an Object
            IDynamicExpression eDynamic = context.CompileDynamic("RecordProxy.IntField = 1 + 2;");
            eDynamic.Evaluate();

            Assert.AreEqual<int>(ep.IntField, 3);
        }

        [TestMethod]
        public void CanExecuteSimpleScriptWithPython()
        {
            dynamic ep = new ExpressionRecordProxy(MockObjects.GetRecord());
            ep.IntField = 1;
            //ep.Count(1);

            var engine = IronPython.Hosting.Python.CreateEngine();
            var scope = engine.CreateScope();
            scope.SetVariable("Risk", ep);

            // return value
           // var result = engine.Execute("Risk.IntField + 5", scope);

            // alter state
            engine.Execute("Risk.DecField = 5.5", scope);

            // type check
            try
            {
                engine.Execute("Risk.IntField = \"5\"", scope);
            }
            catch { }

            // invoke
            engine.Execute("Risk.IntField = Risk.Count(\"Numbers\")", scope);

            engine.Execute("Risk.IntField = Risk.Count(1)", scope);
        }
    }
}

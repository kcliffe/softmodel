﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Raven.Client;
using Raven.Client.Embedded;
using SoftModel.Integration.Etl;
using SoftModel.Query;
using SoftModel.Schema;
using SoftModel.Integration.Etl.Output;
using SoftModel.Integration.Etl.Formatter;

namespace SoftModel.Tests.Integration
{
    [TestClass]
    public class ReadModel_should
    {
        private string connString;
        private SqlCeOutput output;
        private SqlCeFormatter formatter;
        private IDocumentStore store;
        private IDocumentSession session;

        [TestInitialize]
        public void Init()
        {
            connString =
                  @"Data Source=C:\Users\Us\Documents\Visual Studio 2010\Projects\SoftModel\SoftModel.Tests\Integration\integration.sdf;Persist Security Info=False;";

            store = new EmbeddableDocumentStore { RunInMemory = true };
            store.Initialize();
            session = store.OpenSession();

            output = new SqlCeOutput(connString);
            formatter = new SqlCeFormatter(session);

            try
            {
                output.ExecuteNonQuery("drop table PersonSchema");
            }
            catch {}
        }

        [TestCleanup]
        public void CleanUp()
        {
            session.Dispose();
            store.Dispose();
        }
       
        private void InitialiseSurveySchema()
        {
            /* Survey */
            var surveyFieldDescriptors = new List<FieldDescriptor> 
            {
                new FieldDescriptor 
                {                    
                    SystemTypeName = typeof(string).ToString(),
                    Name = "Name",
                    Description = "What is the name of the survey?"
                },
                new FieldDescriptor
                {
                    FriendlyType = "OneToMany",
                    SystemTypeName = typeof(List<RecordRef>).ToString(),
                    Name = "Responses",
                    Description = ""
                }
            };

            var surveyDescriptor = new RecordDescriptor
            {
                FieldDescriptors = surveyFieldDescriptors,
                Id = "recordSchema/Survey1",
                Name = "Survey"
            };

            session.Store(surveyDescriptor);
           
            /* Response */
            var surveyResponseFieldDescriptors = new List<FieldDescriptor> 
            {
                new FieldDescriptor 
                {                    
                    SystemTypeName = typeof(string).ToString(),
                    Name = "Q1",
                    Description = "Do surveys make a difference?"
                },
                new FieldDescriptor
                {
                    SystemTypeName = typeof(int).ToString(),
                    Name = "Q2",
                    Description = "How many survey's have you filled out in the last hour?"
                },
                 new FieldDescriptor
                {
                    SystemTypeName = typeof(string).ToString(),
                    Name = "RecipientId",
                    Description = "The recipient"
                },
                new FieldDescriptor
                {
                    SystemTypeName = typeof(RecordRef).ToString(),
                    Name = "ParentReference",
                    Description = "ParentRef"
                },
            };

            var surveyResponseDescriptor = new RecordDescriptor
            {
                FieldDescriptors = surveyResponseFieldDescriptors,
                Id = "recordSchema/SurveyResponse",
                Name = "SurveyResponse"
            };

            session.Store(surveyResponseDescriptor);            
        }

        private Record InitialiseSurvey()
        {
            var survey = new Record
            {
                Id = "record/Survey1",
                RecordDescriptorRef = new RecordDescriptorRef
                {
                    RecordDescriptorId = "recordSchema/Survey1",
                },
                Fields = new List<FieldValue> 
                {
                    new FieldValue 
                    {
                        FieldDescriptorRef = new FieldDescriptorRef { FieldName = "Name", SystemTypeName=typeof(string).ToString()},
                        Value = "Survey 1"
                    }                    
                }
            };

            session.Store(survey);

            return survey;
        }

        private Record InitialiseSurveyResponses()
        {
            // A single response survey response from a single "user". 
            var surveyResponse = new Record()
            {
                RecordDescriptorRef = new RecordDescriptorRef
                {
                    RecordDescriptorId = "recordSchema/SurveyResponse",
                },
                Fields = new List<FieldValue> 
                {
                    new FieldValue 
                    {
                        FieldDescriptorRef = new FieldDescriptorRef { FieldName = "Q1", SystemTypeName=typeof(string).ToString()},
                        Value = "Nope"
                    },
                    new FieldValue 
                    {
                        FieldDescriptorRef = new FieldDescriptorRef { FieldName = "Q2", SystemTypeName=typeof(int).ToString()},
                        Value = 1                      
                    },
                    new FieldValue 
                    {
                        FieldDescriptorRef = new FieldDescriptorRef { FieldName = "RecipientId", SystemTypeName=typeof(string).ToString()},
                        Value = "Guy 1"                      
                    },
                     new FieldValue 
                    {
                        FieldDescriptorRef = new FieldDescriptorRef { FieldName = "ParentReference", SystemTypeName=typeof(RecordRef).ToString()},
                        Value = "record/Survey1"                      
                    },
                }
            };

            session.Store(surveyResponse);
            return surveyResponse;
        }
    }
}

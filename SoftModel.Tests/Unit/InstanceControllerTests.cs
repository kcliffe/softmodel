﻿using System.Messaging;
using System.Net.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Raven.Client;
using Raven.Client.Document;
using Raven.Client.Embedded;
using SoftModel.Messages;
using SoftModel.UI.Html.Controllers;
using SoftModel.UI.Html.Plugins;

// ReSharper disable CheckNamespace
namespace SoftModel.Tests
// ReSharper restore CheckNamespace
{
    [TestClass]
    public class InstanceApiControllerTests
    {
        private IDocumentSession session;
        private static DocumentStore ds;
               
        [TestInitialize]
        public void Init()
        {
            if (ds == null)
            {
                ds = new EmbeddableDocumentStore
                {
                    RunInMemory = true                          
                };

                ds.Initialize();
            }
            
            session = ds.OpenSession();
            session.Advanced.UseOptimisticConcurrency = false;

            session.Store(MockObjects.GetRecordSchema());
            session.SaveChanges();            
        }
           
        [TestMethod]
// ReSharper disable InconsistentNaming
        public void Put_valid_record_returns_record_URI()
// ReSharper restore InconsistentNaming
        {
            var record = MockObjects.GetRecord();
            var mockMessageQueue = new Mock<IMessageQueue>().Object;

            var controller = new InstanceController(session, null);
            Helpers.SetupControllerContext(controller, HttpMethod.Put);

            ServiceBusFactory.SetQueue(mockMessageQueue);
            //RecordEventListener.Register(typeof(SoftModelRecordEventBase), new EventRegistration<SoftModelRecordEventBase>(Execute.Asynchronously));

            var response = controller.Put(record);

            Assert.IsNotNull(response.Headers.Location.AbsoluteUri);
        }

        [TestMethod]        
// ReSharper disable InconsistentNaming
        public void Put_allows_pre_save_event_to_be_handled()
// ReSharper restore InconsistentNaming
        {
            var record = MockObjects.GetRecord();
            var mockMessageQueue = new Mock<IMessageQueue>();

            var controller = new InstanceController(session, null);
            Helpers.SetupControllerContext(controller, HttpMethod.Put);

            ServiceBusFactory.SetQueue(mockMessageQueue.Object);
            //ecordEventListener.Register(typeof(SoftModelRecordEventBase), new EventRegistration<SoftModelRecordEventBase>(Execute.Asynchronously));

            controller.Put(record);

            mockMessageQueue.Verify(x => x.Send(It.IsAny<Message>()), Times.Exactly(2), "Message was not published during record save");
        }

        /// <summary>
        /// TODO. Move
        /// </summary>
        [TestMethod]
// ReSharper disable InconsistentNaming
        public void Send_integration_messages()
// ReSharper restore InconsistentNaming
        {
            var record = MockObjects.GetRecord();
            record.RecordDescriptorRef.RecordDescriptorId = "personSchema";

            var controller = new InstanceController(session, null);
            Helpers.SetupControllerContext(controller, HttpMethod.Put);

            ServiceBusFactory.SetQueue(new WindowsMessageQueue(new MessageQueue(@".\Private$\softmodel.receiver", QueueAccessMode.Send)));
            //RecordEventListener.Register(typeof(SoftModelRecordEventBase), new EventRegistration<SoftModelRecordEventBase>(Execute.Asynchronously));

            controller.Put(record);            
        }
    }
}

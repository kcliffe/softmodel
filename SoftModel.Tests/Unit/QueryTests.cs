﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Raven.Client;
using System.Linq;
using Raven.Client.Embedded;
using SoftModel.Integration.Etl;
using SoftModel.Query;
using SoftModel.Integration.Etl.Output;
using SoftModel.Integration.Etl.Formatter;

namespace SoftModel.Tests.Unit
{
    [TestClass]
    public class Query_should
    {
        private string connString;
        private SqlCeOutput output;
        private SqlCeFormatter formatter;
        private IDocumentStore store;
        private IDocumentSession session;

        [TestInitialize]
        public void Init()
        {
            connString =
                  @"Data Source=C:\Users\Us\Documents\Visual Studio 2010\Projects\SoftModel\SoftModel.Tests\Integration\integration.sdf;Persist Security Info=False;";

            store = new EmbeddableDocumentStore { RunInMemory = true };
            store.Initialize();
            session = store.OpenSession();

            output = new SqlCeOutput(connString);
            formatter = new SqlCeFormatter(session);

            try
            {
                output.ExecuteNonQuery("drop table ChildRecord");
            }
            catch { }
        }

        [TestCleanup]
        public void CleanUp()
        {
            session.Dispose();
            store.Dispose();
        }

        [TestMethod]
        public void Return_a_list_of_records_by_recordtype()
        {
            int numRecords = 50;
            var schema = MockObjects.GetRecordSchema();
            session.Store(schema);

            // Create test data and schema
            var records = MockObjects.Records(numRecords);
            records.ForEach(record => session.Store(record));
            var createTableStatement = formatter.GetCreateTableStatement(MockObjects.GetRecordSchema());
            output.ExecuteNonQuery(createTableStatement);

            // Insert test data into read model            
            records.ForEach(record =>
            {
                record.Id = "";

                // GetInsertTableStatement could cache statement and swap out using replace
                var insertStatement = formatter.GetInsertTableStatement(record);
                output.ExecuteNonQuery(insertStatement);
            });

            SqlCeQuery query = new SqlCeQuery(connString);
            var retrievedRecords = query.ForRecordsOfType(new RecordTypeQuery { RecordDescriptor = schema });

            Assert.AreEqual(retrievedRecords.Count, numRecords + "Records should be returned");

            // TODO. Assert fields / values ...
        }

        [TestMethod]
        public void Return_a_child_with_reference_to_a_parent()
        {
            // descriptor
            var desc = MockObjects.ChildRecordDescriptor();
            session.Store(desc);
            // record
            var childRecord = MockObjects.ChildRecord();
            session.Store(childRecord);
            // create table schema
            var createTableStatement = formatter.GetCreateTableStatement(MockObjects.GetRecordSchema());
            output.ExecuteNonQuery(createTableStatement);
            // create record
            var insert = formatter.GetInsertTableStatement(childRecord);
            output.ExecuteNonQuery(insert);

            // read
            SqlCeQuery q = new SqlCeQuery(connString);
            var results = q.ForRecordsOfType(new RecordTypeQuery { RecordDescriptor = desc });
        }
    }
}

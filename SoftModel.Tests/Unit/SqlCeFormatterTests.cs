﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Raven.Client;
using Raven.Client.Embedded;
using SoftModel.Integration.Etl;
using SoftModel.Schema;
using SoftModel.Integration.Etl.Output;
using SoftModel.Integration.Etl.Formatter;

namespace SoftModel.Tests.Unit
{
    [TestClass]
    public class SqlCeFormatter_should
    {
        private string connString;
        private SqlCeOutput output;
        private SqlCeFormatter formatter;
        private IDocumentStore store;
        private IDocumentSession session;

        [TestInitialize]
        public void Init()
        {
            connString =
                  @"Data Source=C:\Users\Us\Documents\Visual Studio 2010\Projects\SoftModel\SoftModel.Tests\Integration\integration.sdf;Persist Security Info=False;";

            store = new EmbeddableDocumentStore { RunInMemory = true };
            store.Initialize();
            session = store.OpenSession();

            output = new SqlCeOutput(connString);
            formatter = new SqlCeFormatter(session);

            try
            {
                output.ExecuteNonQuery("drop table PersonSchema");
            }
            catch { }
        }

        [TestCleanup]
        public void CleanUp()
        {
            session.Dispose();
            store.Dispose();
        }
       
        // Sanity
        [TestMethod]
        public void Create_correct_insert_statement_for_parent()
        {
            // descriptor
            session.Store(MockObjects.ParentRecordDescriptor());

            // record
            var parentRecord = MockObjects.ParentRecord();
            session.Store(parentRecord);

            var insertStatement = formatter.GetInsertTableStatement(parentRecord);

            const string expectedInsertStatement = "insert parent (opId,Name) values ('records/Parent1','Bob')";
            Assert.AreEqual(insertStatement, expectedInsertStatement);
        }

        // Sanity
        [TestMethod]
        public void Create_correct_insert_statement_for_child()
        {
            // descriptor
            session.Store(MockObjects.ChildRecordDescriptor());

            // record
            var childRecord = MockObjects.ChildRecord();
            session.Store(childRecord);

            var insertStatement = formatter.GetInsertTableStatement(childRecord);

            const string expectedInsertStatement = "insert child (opId,Name,ParentId) values ('records/Child1','Bob','records/Parent1')";
            Assert.AreEqual(insertStatement, expectedInsertStatement);
        }
    }
}

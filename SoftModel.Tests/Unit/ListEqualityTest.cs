﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SoftModel.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftModel.Tests.Unit
{
    [TestClass]
    public class ListEqualityTest
    {
        [TestMethod]
        public void True_when_lists_are_equal()
        {
            var a = MockObjects.GetRecordSchema();
            var b = MockObjects.GetRecordSchema();

            var listComparer = new ListComparer<FieldDescriptor>();
            var result = listComparer.Equals(a.FieldDescriptors, b.FieldDescriptors);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void False_when_elements_are_not_equal()
        {
            var a = MockObjects.GetRecordSchema();
            var b = MockObjects.GetRecordSchema();
            b.FieldDescriptors.First().Name = "_";

            var listComparer = new ListComparer<FieldDescriptor>();
            var result = listComparer.Equals(a.FieldDescriptors, b.FieldDescriptors);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void False_when_element_count_differs()
        {
            var a = MockObjects.GetRecordSchema();
            var b = MockObjects.GetRecordSchema();
            b.FieldDescriptors.Remove(b.FieldDescriptors.Last());

            var listComparer = new ListComparer<FieldDescriptor>();
            var result = listComparer.Equals(a.FieldDescriptors, b.FieldDescriptors);
            Assert.IsFalse(result);
        }

    }
}

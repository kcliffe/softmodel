﻿using Raven.Client;
using Raven.Client.Document;
using SoftModel.Schema;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftModel.Tests.Api
{
    public class IntegrationTestBase
    {
        private IDocumentStore store;
        protected IDocumentSession session;

        public IntegrationTestBase()
        {
            store = new DocumentStore
            {
                Url = ConfigurationManager.AppSettings["SoftmodelOperationalStoreUrl"],
                DefaultDatabase = "SoftModel"
            }
            .Initialize();

            session = store.OpenSession();
        }

        protected void CreateMockRecordDescriptor()
        {
            session.Store(MockObjects.GetRecordSchema());

            session.SaveChanges();
        }

        protected string CreateMockRecordInstance(string id = "")
        {
            var record = MockObjects.GetRecord(id);
            session.Store(record);

            session.SaveChanges();

            return record.Id;
        }

        protected void RemoveMockRecordSchema()
        {
            var descriptor = session.Load<RecordDescriptor>("RecordSchemaDescriptor/1");
              
            if (descriptor != null)
                session.Delete(descriptor);

            session.SaveChanges();
        }

        protected void RemoveMockRecordInstance()
        {
            var record = session.Query<Record>()
               .FirstOrDefault(r => r.RecordDescriptorRef.RecordDescriptorId == "RecordSchemaDescriptor/1");

            if (record != null)
                session.Delete(record);

            session.SaveChanges();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Messaging;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Raven.Imports.Newtonsoft.Json;
using SoftModel.Schema;
using SoftModel.UI.Html.Controllers;
using SoftModel.UI.Html.Plugins;
using RestSharp;
using System.Linq;

namespace SoftModel.Tests.Api
{
    [TestClass]
    public class InstanceIntegrationTests : IntegrationTestBase
    {
        Uri softModelBaseUrl = new Uri("http://localhost:50000/api/v1/");

        [TestInitialize]
        public void Setup()
        {
            var records = session.Query<Record>()
               .Where(r => r.RecordDescriptorRef.RecordDescriptorId == "RecordSchemaDescriptor/1").ToList();

            if (records.Any())
            {
                records.ForEach(r => { session.Delete(r); });
            }

            var descriptors = session.Load<RecordDescriptor>("RecordSchemaDescriptor/1");
            if(descriptors != null)
                session.Delete(descriptors);

            session.SaveChanges();
        }

        [TestCleanup]
        public void cleanup()
        {
            RemoveMockRecordInstance();
            RemoveMockRecordSchema();
        }

        [TestMethod]
        public void Get_returns_a_single_record()
        {
            var recordId = CreateMockRecordInstance("records/999");
            var r = session.Load<Record>("records/999");
            var r2 = session.Load<Record>("records-999");

            var client = new RestClient(softModelBaseUrl + "/instance/put");
            var request = new RestRequest("/resource/", Method.PUT);

            // Overwrite a field value
            var record = session.Load<Record>(recordId);
            record.FieldValue("StringField").Value = "replaced";

            string jsonToSend = JsonConvert.SerializeObject(record);

            request.AddParameter("application/json; charset=utf-8", jsonToSend, ParameterType.RequestBody);
            request.RequestFormat = DataFormat.Json;

            client.Execute(request);
        }

        //[TestMethod]
        //public void GetByRecordSchemaId_returns_a_page_of_records()
        //{
        //    var client = new HttpClient();
        //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //    var response = client.GetAsync("http://localhost:50000/api/v1/instance/GetByRecordSchemaId?id=personschema").Result;
        //    response.EnsureSuccessStatusCode();

        //    var instances = JsonConvert.DeserializeObject<List<Record>>(response.Content.ReadAsStringAsync().Result);
        //}

        [TestMethod]
        public void Put_creates_a_record()
        {
            CreateMockRecordDescriptor();

            var client = new RestClient(softModelBaseUrl + "/instance/put");
            var request = new RestRequest("/resource/", Method.PUT);

            string jsonToSend = JsonConvert.SerializeObject(MockObjects.GetRecord(string.Empty));

            request.AddParameter("application/json; charset=utf-8", jsonToSend, ParameterType.RequestBody);
            request.RequestFormat = DataFormat.Json;

            client.ExecuteAsync(request, response =>
            {
                Assert.IsTrue(response.StatusCode == HttpStatusCode.OK);
            });

            Assert.IsNotNull(session.Query<Record>().FirstOrDefault(r => r.RecordDescriptorRef.RecordDescriptorId == "RecordSchemaDescriptor/1"),
                "Should have saved a record with Put.");
        }

        [TestMethod]
        public void Put_a_valid_record_instance_returns_status_code_200()
        {
            var client = new RestClient(softModelBaseUrl + "/instance/put");
            var request = new RestRequest("/resource/", Method.PUT);

            string jsonToSend = JsonConvert.SerializeObject(MockObjects.GetRecord(string.Empty));

            request.AddParameter("application/json; charset=utf-8", jsonToSend, ParameterType.RequestBody);
            request.RequestFormat = DataFormat.Json;
                        
            client.ExecuteAsync(request, response =>
            {
                Assert.IsTrue(response.StatusCode == HttpStatusCode.OK);
            });
        }

        [TestMethod]
        public void Put_Throws_Bad_Request_If_Record_Has_No_FieldValues()
        {
            var client = new RestClient(softModelBaseUrl + "/instance/put");
            var request = new RestRequest("/resource/", Method.PUT);

            var record = new Record();// MockObjects.GetRecord();
            var recordJson = JsonConvert.SerializeObject(record);

            request.AddParameter("application/json; charset=utf-8", recordJson, ParameterType.RequestBody);
            request.RequestFormat = DataFormat.Json;

            client.ExecuteAsync(request, response =>
            {
                Assert.IsTrue(response.StatusCode == HttpStatusCode.BadRequest);
            });
        }

        [TestMethod]
        public void Updating_a_record_with_new_fieldvalues_overwrites_existing_fieldvalues()
        {
            CreateMockRecordDescriptor();
            var recordId = CreateMockRecordInstance();

            var client = new RestClient(softModelBaseUrl + "/instance/put");
            var request = new RestRequest("/resource/", Method.PUT);

            // Overwrite a field value
            var record = session.Load<Record>(recordId);
            record.FieldValue("StringField").Value = "replaced";

            string jsonToSend = JsonConvert.SerializeObject(record);

            request.AddParameter("application/json; charset=utf-8", jsonToSend, ParameterType.RequestBody);
            request.RequestFormat = DataFormat.Json;

            client.Execute(request);

            var recordAfter = session.Query<Record>().FirstOrDefault(r => r.RecordDescriptorRef.RecordDescriptorId == "RecordSchemaDescriptor/1");
            Assert.AreEqual(recordAfter.FieldValue("StringField").Value, "replaced" ,"String field value should have been updated.");
            Assert.AreEqual(recordAfter.FieldValue("IntField").Value, 1, "Int field value should not have been updated.");

        }

        [TestMethod]
        public void Adding_a_record_creates_default_values_for_all_fieldvalues()
        {
            // TODO EnsureDefaultFieldValues on instancecontroller should be unit testable component.
            // so we can test for all types of fields (including enums, relationships)
            CreateMockRecordDescriptor();

            var client = new RestClient(softModelBaseUrl + "/instance/put");
            var request = new RestRequest("/resource/", Method.PUT);

            string jsonToSend = JsonConvert.SerializeObject(MockObjects.GetRecord(string.Empty));

            request.AddParameter("application/json; charset=utf-8", jsonToSend, ParameterType.RequestBody);
            request.RequestFormat = DataFormat.Json;

            client.Execute(request);

            var recordAfter = session.Query<Record>().FirstOrDefault(r => r.RecordDescriptorRef.RecordDescriptorId == "RecordSchemaDescriptor/1");
            Assert.AreEqual(recordAfter.FieldValue("IntField2").Value, 0, "Int field 2 should have been updated.");
        }
    }
}

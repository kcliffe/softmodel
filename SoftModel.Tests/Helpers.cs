﻿using SoftModel.UI.Html.Controllers;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Hosting;
using System.Web.Http.Routing;

namespace SoftModel.Tests
{
    internal class Helpers
    {
        public static void SetupControllerContext(BaseApiController controller, HttpMethod method)
        {
            var request = new HttpRequestMessage(method, "http://localhost/api/v1/Test/Validate");
            // controller.Request = new HttpRequestMessage(method, "http://localhost");
            //controller.Request.Headers.Add("Accept", "application/json");            
            //var configuration = new System.Web.Http.HttpConfiguration(new System.Web.Http.HttpRouteCollection());
            var configuration = new HttpConfiguration();

            var route = configuration.Routes.MapHttpRoute("DefaultApi", "api/v1/{controller}/{action}");
            var routeData = new HttpRouteData(route, new HttpRouteValueDictionary(new { controller = "Test", action = "Validate" }));
            
            controller.ControllerContext = new HttpControllerContext(configuration, routeData, request);
            controller.Request = request;
            controller.Request.Properties[HttpPropertyKeys.HttpConfigurationKey] = configuration;
        }
    }
}

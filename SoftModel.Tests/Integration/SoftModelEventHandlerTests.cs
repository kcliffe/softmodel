﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NServiceBus.Testing;
using Raven.Client.Embedded;
using SoftModel.Integration.Etl;
using SoftModel.Integration.Etl.Formatter;

namespace SoftModel.Tests.Integration
{
    /// <summary>
    /// NSB handlers are testable like any other class. With a litte help from using NServiceBus.Testing.
    /// </summary>
    [TestClass]
    public class When_outputting_record
    {
        [TestMethod]
        public void The_generated_statement_is_correct_for_a_simple_record()
        {
            Test.Initialize();

            const string expectedOutput = "insert personschema (stringdef,intdef) values ('a string',1)";

            var store = new EmbeddableDocumentStore { RunInMemory = true };
            store.Initialize();

            var session = store.OpenSession();

            var schema = MockObjects.GetRecordSchema();
            session.Store(schema);

            var record = MockObjects.GetRecord();
            session.Store(record);

            var dbFormatter = new SqlCeFormatter(session);

            var output = dbFormatter.GetInsertTableStatement(record);

            Assert.AreEqual(output.ToLower(), expectedOutput);
        }
    }
}

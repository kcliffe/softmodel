﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SoftModel.Integration.Etl.Formatter;
using Raven.Client.Document;
using Raven.Client;
using System.Configuration;

namespace SoftModel.Tests.Integration
{
    [TestClass]
    public class PostGreSchemaTests
    {
        [TestMethod]
        public void Can_create_a_table()
        {
            IDocumentStore docStore = new DocumentStore();
            docStore.Initialize();

            IDocumentSession documentSession = docStore.OpenSession(ConfigurationManager.AppSettings["readModel"]);
            PostGreFormatter postGreFormatter = new PostGreFormatter(documentSession);

            postGreFormatter.GetCreateTableStatement(MockObjects.GetRecordSchema());
        }
    }
}

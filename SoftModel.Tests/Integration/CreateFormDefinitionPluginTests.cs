﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestSharp;
using Raven.Imports.Newtonsoft.Json;
using Raven.Client;
using Raven.Client.Document;
using SoftModel.Schema;

namespace SoftModel.Tests.Integration
{
    [TestClass]
    public class CreateFormDefinitionPluginTests
    {
        IDocumentSession documentSession;

        [TestInitialize]
        public void Init()
        {
            var documentStore = new DocumentStore
            {
                Url = "http://localhost:8080",
                DefaultDatabase = "SoftModel"
            }
           .Initialize();

            documentSession = documentStore.OpenSession();
        }

        [TestMethod]
        public void When_a_record_schema_is_created_a_default_form_definition_is_created()
        {
            var schemaId = "recordSchemas/TestSchema";
            var client = new RestClient("http://localhost:50000/api/v1/schema/put");
            var request = new RestRequest("/resource/", Method.PUT);

            string jsonToSend = JsonConvert.SerializeObject(MockObjects.GetRecordSchema(schemaId, "TestSchema"));

            request.AddParameter("application/json; charset=utf-8", jsonToSend, ParameterType.RequestBody);
            request.RequestFormat = DataFormat.Json;

            client.Execute(request);

            var createdFormDefinition = documentSession.Load<FormDefinition>("FormDefinitions/TestSchema_Default");
            Assert.IsNotNull(createdFormDefinition);

            documentSession.Delete<FormDefinition>(createdFormDefinition);
        }
    }
}

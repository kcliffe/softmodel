﻿using Newtonsoft.Json;
using RestSharp;
using RestSharp.Contrib;
using SoftModel.Schema;
using SoftModel.Schema.Query;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ApiUtil
{
    public class ApiHelper
    {
        private string baseUrl = "";
        private string apiKey = "";
        private const string ApiKeyName = "X-ApiKey";

        public ApiHelper(string baseUrl, string apiKey)
        {
            this.baseUrl = baseUrl;
            this.apiKey = apiKey;
        }

        // TODO. Query by id list

        // TODO. Get single record by id

        public List<Record> QueryRecords(ByRecordSchemaCriteria query)
        {
            var queryUrl2 = "read/getbyrecordschema?" + UrlFormat(query);

            var request = new RestRequest(queryUrl2, Method.GET);
            var response = Execute(request);

            return JsonConvert.DeserializeObject<List<Record>>(response.Content);
        }

        public IRestResponse Put(IRestRequest request)
        {
            return Execute(request);
        }

        public IRestResponse PutRecord(Record record)
        {
            var request = new RestRequest("instance/put", Method.PUT);
            request.AddParameter("application/json; charset=utf-8", JsonConvert.SerializeObject(record), ParameterType.RequestBody);

            return Execute(request);
        }

        public IRestResponse PutChildRecord(Record record)
        {
            var request = new RestRequest("instance/putchild", Method.PUT);
            request.AddParameter("application/json; charset=utf-8", JsonConvert.SerializeObject(record), ParameterType.RequestBody);

            return Execute(request);
        }

        private IRestResponse Execute(IRestRequest request)
        {
            var client = new RestClient(baseUrl);

            request.AddHeader(ApiKeyName, apiKey);
            request.RequestFormat = DataFormat.Json;

            return client.Execute(request);
        }

        /// <summary>
        /// There is probably a RetSharp way to do this..
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private string UrlFormat(object obj)
        {
            var properties = from p in obj.GetType().GetProperties()
                             where p.GetValue(obj, null) != null
                             select p.Name + "=" + HttpUtility.UrlEncode(p.GetValue(obj, null).ToString());

            return String.Join("&", properties.ToArray());
        }
    }
}

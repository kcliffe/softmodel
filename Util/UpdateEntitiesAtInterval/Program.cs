﻿using ApiUtil;
using Newtonsoft.Json;
using RestSharp;
using SoftModel.Schema;
using SoftModel.Schema.Query;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;

namespace UpdateEntitiesAtInterval
{
    /// <summary>
    /// Periodically throw an update the API. Primairly to demo / test the throttled update notifications
    /// pushed out from the readmodel.
    /// </summary>
    class Program
    {
        private static string softModelBaseUrl;
        private static string apiKey;
        private static Random random;
        
        static void Main(string[] args)
        {
            softModelBaseUrl = ConfigurationManager.AppSettings["apiBaseUri"];
            apiKey = ConfigurationManager.AppSettings["apiKey"];
            random = new Random(Guid.NewGuid().GetHashCode());

            if (args.Length > 0)
            {
                CheatPulseApi();
            }
            else
            {
                PulseApi(50);
            }
        }

        private static void CheatPulseApi()
        {
            Console.WriteLine("Pulsing API n cheat mode");
            Console.WriteLine("Press any key to quit");

            var apiHelper = new ApiHelper(softModelBaseUrl, apiKey);

            while (Console.KeyAvailable == false)
            {
                var request = new RestRequest("notification/notify", Method.PUT);
                var notification = new { RecordDescriptorName = "Order", ItemId = "records-6004" };
                string jsonToSend = JsonConvert.SerializeObject(notification);
                request.AddParameter("application/json; charset=utf-8", jsonToSend, ParameterType.RequestBody);

                apiHelper.Put(request);

                Console.WriteLine("Sleeping {0} seconds until next update.", 5000);
                Thread.Sleep(5000);
            }
        }

        private static void PulseApi(int numRecordsToUpdate)
        {
            Console.WriteLine("Press any key to quit");
            Console.WriteLine("Retrieving {0} records for update.", numRecordsToUpdate);
            var recordsWeCanUpdate = GetXRecords(numRecordsToUpdate);
            Console.WriteLine("Retrieved {0} records.", numRecordsToUpdate);

            while (Console.KeyAvailable == false)
            {
                var recordIndex = random.Next(0, numRecordsToUpdate-1);
                var record = recordsWeCanUpdate[recordIndex];

                Console.WriteLine("Generating update for record {0}", record.Id);
                UpdateRecord(record);

                // valid use for thread.sleep!
                var interval = random.Next(1000, 10000);
                Console.WriteLine("Sleeping {0} seconds until next update.", interval / 1000);
                Thread.Sleep(interval);
            }
        }

        private static void UpdateRecord(Record record)
        {
            var apiHelper = new ApiHelper(softModelBaseUrl, apiKey);
            var random = new Random();

            record.Fields.First(x => x.FieldName == "TotalPrice").Value = random.Next(0, 10000);
            apiHelper.PutRecord(record);
        }

        private static List<Record> GetXRecords(int recordCount)
        {
            ByRecordSchemaCriteria query = new ByRecordSchemaCriteria
             {
                 Id = "recordDescriptors-Order",
                 Page = 0,
                 PageSize = recordCount
             };

            var apiHelper = new ApiHelper(softModelBaseUrl, apiKey);
            return apiHelper.QueryRecords(query);
        }
    }
}

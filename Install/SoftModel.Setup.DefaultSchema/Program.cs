﻿using ApiUtil;
using Codeplex.Data;
using Raven.Abstractions.Data;
using Raven.Client;
using Raven.Client.Document;
using Raven.Client.Indexes;
using Raven.Imports.Newtonsoft.Json;
using Raven.Imports.Newtonsoft.Json.Linq;
using RestSharp;
using SoftModel.Schema;
using SoftModel.Schema.Expressions;
using SoftModel.Schema.Security;
using SoftModel.UI.Html.Indexes;
using SoftModel.UI.Html.ViewModels.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading;
using Omu.ValueInjecter;
using System.Linq;
using System.Globalization;
using System.IO;

namespace SoftModel.Setup.DefaultSchema
{
    class Program
    {
        private static DocumentStore documentStore;
        private static string softModelBaseUrl;
        private static ApiHelper apiHelper;

        static void Main(string[] args)
        {
           
            //var validArgs = new [] { "schema", "data", "update", "persons" };
            //Action[] actions = { CreateSchemaObjects, CreateOrderRecordsViaApi, UpdateBatch, LoadPersons};

            //if (args.Length == 0 || args.Length > 1 ||
            //    (!validArgs.Contains(args[0])))
            //{
            //    Out("Usage: DefaultSchema [" + string.Join(",",validArgs) + "]. Schema to create all required tables, Data to create sample order data.");
            //    return;
            //}

            softModelBaseUrl = ConfigurationManager.AppSettings["apiBaseUri"];
            var apiKey = ConfigurationManager.AppSettings["apiKey"];

            documentStore = new DocumentStore { Url = ConfigurationManager.AppSettings["operationStoreUrl"], DefaultDatabase = "SoftModel" };
            documentStore.Initialize();

            AddPersonsToGroup();

            //IndexCreation.CreateIndexes(typeof(Schema_Search).Assembly, documentStore);

            apiHelper = new ApiHelper(softModelBaseUrl, apiKey);

            #region Prev

            // Initialize indexes
            // IndexCreation.CreateIndexes(typeof(RecordByRecordSchemaId).Assembly, documentStore);

            // AddAuditLogsPostGre();

            #endregion

            //var actionIndex = validArgs.IndexOf(args[0]);
            //actions[actionIndex]();

            documentStore.Dispose();

            Out("Done");
            Console.ReadLine();
        }

        #region API hitters

        private static void CreateSchemaObjects()
        {
            //Out("-- Enums --");
            //CreateSampleEnums();

            //Out("Creating form definition");
            //CreateSampleFormDefinition();

            // TODO. For admin user
            //Out("Creating user dashboard");
            //CreateSampleUserDashboard();

            //Out("-- Access --");
            //CreateSampleUsers();
            //CreateSampleGroups();

            Out("Creating order schema");
            CreateOrderSchemaViaApi();
            Thread.Sleep(5000);
            Out("Creating orderline schema");
            CreateOrderLineSchemaViaApi();
            Thread.Sleep(5000);
            Out("Linking order / orderline schema");
            CreateOrderOrderLineRelationship();
        }

        private static void UpdateBatch()
        {
            Out("Updating batch");

            List<Record> readBatch = apiHelper.QueryRecords(new Schema.Query.ByRecordSchemaCriteria {
                Page = 0,
                PageSize = 25,
                Id = "recordDescriptors/Order"
            }).GroupBy(r => r.Id).Select(g => g.First()).ToList(); // unique records

            Out(string.Format("{0} records retrieved", readBatch.Count));

            // Until we have the read model returning etags with records, query for them
            var recordIds = readBatch.Select(b => b.Id).Distinct();
            List<Record> batch;
            using (var session = documentStore.OpenSession("SoftModel"))
            {
                batch = session.Load<Record>(recordIds).ToList();
                // assign etags to avoid conflicts
                batch.ForEach(record => {
                    readBatch.Single(r=>r.Id == record.Id).Etag = session.Advanced.GetEtagFor(record).Value;
                    Out(string.Format("Etag for record {0} is {1}", record.Id, record.Etag));
                });
            }

            readBatch.ForEach(record => {
                var oldPrice = record.FieldValue<decimal>("TotalPrice");
                Randomize(record);
                Out(string.Format("Record {0} updated with total price {1} from {2}.",record.Id, oldPrice, record.FieldValue<decimal>("TotalPrice")));

                var response = apiHelper.PutRecord(record);
                DebugIfResponseError(response);
            });
        }

        /// <summary>
        /// Note - the Integration project must also be running
        /// </summary>
        private static void CreateOrderRecordsViaApi()
        {
            PingApi();

            CreateOrderAndOrderLineInstances(500, 10);
        }

        private static void CreateOrderAndOrderLineInstances(int numOrders, int numOrderLinesPerOrder)
        {
            // Delete existing operation model record collection
            using (var session = documentStore.OpenSession("SoftModel"))
            {
                DeleteCollection(session, "Records");
            }

            var products = new List<string> { "Valentino Rossi Poster", "Dunlop Sportsmax Rear", "Pioneer Headunit", "A cupcake", "Redband Gumboots" };            
            
            for (int i = 1; i <= numOrders; i++)
            {
                // Create an order
                var orderRef = String.Format("orderRef{0}", i);
                var totalPrice = Math.Round((decimal)new Random().NextDouble() * 10000, 2);
                var discount = Math.Round(new Random().NextDouble(), 2);
                var orderPlaced = DateTime.Now.AddDays(-new Random().Next(100, 3650) / 100);
                var record = GetOrderRecord(orderRef, orderPlaced, totalPrice, discount);

                // Execute API request to add order
                var response = apiHelper.PutRecord(record);
                DebugIfResponseError(response);
                var parentRef = IdFromResponse(response);

                // order lines
                var numOrderLines = new Random().Next(1000, numOrderLinesPerOrder * 1000) / 1000;
                for (int z = 0; z < numOrderLines; z++)
                {
                    Thread.Sleep(150);

                    // Create order line
                    var quantity = new Random().Next(1, 10);
                    //var orderLineId = String.Format("records/OrderLine{0}", i * z + 1);
                    var orderLineRef = String.Format("orderLineRef{0}", i * z + 1);
                    var product = products[new Random().Next(1, 5)];
                    var orderLineRecord = GetOrderLineRecord(orderLineRef, parentRef, "OrderLines", product, quantity);

                    // Execute API request to add order line
                    var orderLineResponse = apiHelper.PutChildRecord(orderLineRecord);
                    DebugIfResponseError(orderLineResponse);
                }
            }
        }

        private static void CreateOrderSchemaViaApi()
        {
            // Kill existing schema
            DeleteIfExists<RecordDescriptor>("recordDescriptors/Order");

            var request = new RestRequest("schema/put", Method.PUT);
            string jsonToSend = JsonConvert.SerializeObject(GetOrderRecordDescriptor());
            request.AddParameter("application/json; charset=utf-8", jsonToSend, ParameterType.RequestBody);

            var reponse = apiHelper.Put(request);
        }

        private static void CreateOrderLineSchemaViaApi()
        {
            // Kill existing schema
            DeleteIfExists<RecordDescriptor>("recordDescriptors/OrderLine");

            var request = new RestRequest("schema/put", Method.PUT);
            string jsonToSend = JsonConvert.SerializeObject(GetOrderLineRecordDescriptor());
            request.AddParameter("application/json; charset=utf-8", jsonToSend, ParameterType.RequestBody);

            var reponse = apiHelper.Put(request);
        }

        private static void CreateOrderOrderLineRelationship()
        {
            var request = new RestRequest("schema/put", Method.PUT);
            string jsonToSend = JsonConvert.SerializeObject(GetOrderWithOrderLineRelationship());
            request.AddParameter("application/json; charset=utf-8", jsonToSend, ParameterType.RequestBody);

            var reponse = apiHelper.Put(request);
        }

        private static void Spike()
        {
            // Insert (string or JObject)
            var jo = "{\"id\":\"\", \"Name\":\"Fred\", \"Address\" : { \"Street\": \"123 Street\", \"PostCode\" : \"8081\"}}";
            JObject jobject = JObject.Parse(jo);

            // Insert
            using (var session = documentStore.OpenSession("SoftModel"))
            {
                session.Store(jobject);
                session.SaveChanges();
            }

            // Retrieve
            using (var session = documentStore.OpenSession("SoftModel"))
            {
                var result = session.Load<JObject>(jobject.Property("Id").Value.ToString());
            }

            // Query
            var another = DynamicJson.Parse(jo);
            Console.WriteLine(another.Address.Street);

            // Validate Type
        }

        #endregion

        #region Others

        private static void LoadPersons()
        {
            // At some stage we'll port these to personnel, for now - they're "Persons"
            using (var session = documentStore.OpenSession("SoftModel"))
            {
                DeleteCollection(session, "Persons");
            }

            using (var bulkInsert = documentStore.BulkInsert())
            {
                using (var file = File.OpenText(@"..\..\names.txt"))
                {
                    string line;
                    while ((line = file.ReadLineAsync().Result) != null)
                    {
                        if (string.IsNullOrWhiteSpace(line)) break;
                        var names = line.Trim().Split(' ');

                        if (names.Length != 2) continue;

                        bulkInsert.Store(new Person { 
                            Id=string.Format("persons/{0}{1}", names[0].ToLower(), names[1].ToLower()), 
                            FirstName=names[0], 
                            Surname=names[1] });

                        Out(string.Format("persons/{0}{1}", names[0].ToLower(), names[1].ToLower()));
                    }
                }
            }
        }

        private static void AddPersonsToGroup()
        {
            var groupNames = new List<string> { "Waikato Users","Auckland Users","Canterbury Users","People who like yoghurt","People who don't","Dog Lovers","Random Group", "Whiskey Drinkers"};
            var groupList = new List<Group>();

            using (var session = documentStore.OpenSession("SoftModel"))
            {
                var users = session.Query<User>().Take(1024).ToList();

                //using (var bulkInsert = documentStore.BulkInsert())
                //{
                //    people.ForEach(u => {
                //        bulkInsert.Store(new User { 
                //            Name = string.Format("{0} {1}", u.FirstName, u.Surname),
                //            UserName = u.FirstName.ToLower()
                //        });
                //    });
                //};

                var skipCount = 1;
                var pageSize = (int)Math.Ceiling((decimal)users.Count / groupNames.Count);

                groupNames.ForEach(x =>
                {
                    var usersInGroup = users.Skip(skipCount * pageSize).Take(pageSize);

                    var group = new Group
                    {
                        Name = x,
                        Id = "groups/" + x.Replace(" ", "_"),
                        UserRefs = usersInGroup.Select(u => new UserRef { UserId = u.Id, UserName = u.UserName  }).ToList()
                    };

                    groupList.Add(group);
                    session.Store(group);
                });

                session.SaveChanges();
            }
        }

        private static void CreateSampleEnums()
        {
            using (var session = documentStore.OpenSession("SoftModel"))
            {
                var existingEnum = session.Load<Enumeration>("enumerations/Size");
                if (existingEnum == null)
                {
                    var selList = new Enumeration("Size");
                    selList.Options = new List<Option>
                    { 
                        new Option { Value = 1, Label = "Small" },
                        new Option { Value = 2, Label = "Medium" },
                        new Option { Value = 3, Label = "Large" }
                    };

                    session.Store(selList);
                    session.SaveChanges();
                }
            }
        }

        private static void CreateSampleRuleSet()
        {
            using (var session = documentStore.OpenSession("SoftModel"))
            {
                var existingEnum = session.Load<RuleSet>("ruleSets/Order");
                if (existingEnum == null)
                {
                    var ruleSet = new RuleSet();
                    var rule = new Rule("if (Order.TotalPrice > 0) Risk.Discount = 5;");

                    ruleSet.Rules.Add(rule);

                    session.Store(ruleSet);
                    session.SaveChanges();
                }
            }
        }

        private static void CreateSampleGroups()
        {
            using (var session = documentStore.OpenSession("SoftModel"))
            {
                var existingGroup = session.Load<Group>("groups/GuysGroup");
                if (existingGroup == null)
                {
                    var group = new Group
                    {
                        Id = "groups/GuysGroup",
                        Name = "Guys Group",
                        UserRefs = new List<UserRef>
                    {
                        new UserRef { UserId = "users/guy", UserName = "guy"}
                    }
                    };

                    session.Store(group);
                    session.SaveChanges();
                }
            }
        }

        private static void CreateSampleUsers()
        {
            using (var session = documentStore.OpenSession("SoftModel"))
            {
                var existingGuy = session.Load<Group>("users/guy");
                if (existingGuy == null)
                {
                    var user = new User
                    {
                        Id = "users/guy",
                        UserName = "guy",
                        GroupRefs = new List<GroupRef>
                        {
                            new GroupRef { GroupId = "groups/GuysGroup", GroupName = "Guys Group"}
                        }
                    };

                    session.Store(user);
                    session.SaveChanges();
                }
            }
        }

        private static void CreateSampleFormDefinition()
        {
            using (var session = documentStore.OpenSession("SoftModel"))
            {
                if (session.Load<FormDefinition>("formDefinition") != null)
                    return;

                var stringDef = new FieldDescriptor { SystemTypeName = typeof(string).ToString(), Name = "Name" };
                var intDef = new FieldDescriptor { SystemTypeName = typeof(int).ToString(), Name = "Age" };

                var formDef = new FormDefinition
                {
                    Id = "orderFormDefinition",
                    Name = "Order Form",
                    RecordDescriptorReference = new RecordDescriptorRef
                    {
                        RecordDescriptorId = "recordDescriptors/Order"
                    },
                    Controls = new List<Control> { 
                        new Control {                  
                            FieldName = "Name",
                            Properties = new Dictionary<string,dynamic> {
                                { "gridId", "g0" }
                            }
                        }
                    }
                };

                session.Store(formDef);
                session.SaveChanges();

                var retrieved = session.Load<FormDefinition>(formDef.Id);
            }
        }

        private static void CreateSampleUserDashboard()
        {
            using (var session = documentStore.OpenSession("SoftModel"))
            {
                var currentDash = session.Load<UserDashboard>("kellyc");
                if (currentDash != null)
                {
                    session.Delete(currentDash);
                    session.SaveChanges();
                }

                var userDashboard = new UserDashboard
                {
                    Id = "kellyc",
                    Views = new List<DashboardView> { 
                        new DashboardView {
                            DashboardName = "Dashboard 1",
                            PageComponents = new List<PageComponent> {
                                new PageComponent {
                                    RecordDescriptorName = "Order",
                                    TabTitle = "Stuff 1",
                                    DataviewId = "r1c1",                                            // Row 1 column 1
                                    Visualizer = new Visualizer {
                                        Type = "Tabular",
                                        Settings = new Settings {                                            
                                            RecordDescriptorName = "Order",
                                            DisplayFields = new List<DisplayFieldProxy> 
                                            { 
                                                new DisplayFieldProxy { FieldDescriptorName="OrderReference", DisplayField="Order Ref"},
                                                new DisplayFieldProxy { FieldDescriptorName="OrderDatePlaced", DisplayField="Date Placed"},
                                                new DisplayFieldProxy { FieldDescriptorName="TotalPrice", DisplayField="Total"}
                                            },
                                        }
                                    },
                                    Data = string.Empty
                                },
                                new PageComponent {
                                    RecordDescriptorName = "Order",
                                    DataviewId = "r1c2", 
                                    TabTitle = "Stuff 2",
                                    Visualizer = new Visualizer {
                                        Type = "Tabular",
                                        Settings = new Settings {                                            
                                            RecordDescriptorName = "Order",
                                            DisplayFields = new List<DisplayFieldProxy> 
                                            { 
                                                new DisplayFieldProxy { FieldDescriptorName="OrderReference", DisplayField="Order Ref"},
                                                new DisplayFieldProxy { FieldDescriptorName="OrderDatePlaced", DisplayField="Date Placed"},
                                                new DisplayFieldProxy { FieldDescriptorName="TotalPrice", DisplayField="Total"}
                                            },
                                        }
                                    },                                
                                    Data = string.Empty
                                },
                                new PageComponent {
                                    RecordDescriptorName = "Order",
                                    DataviewId = "r2c1",                                            
                                    TabTitle = "Stuff 3",
                                    Visualizer = new Visualizer {
                                        Type = "Tabular",
                                        Settings = new Settings {                                            
                                            RecordDescriptorName = "Order",
                                            DisplayFields = new List<DisplayFieldProxy> 
                                            { 
                                                new DisplayFieldProxy { FieldDescriptorName="OrderReference", DisplayField="Order Ref"},
                                                new DisplayFieldProxy { FieldDescriptorName="OrderDatePlaced", DisplayField="Date Placed"},
                                                new DisplayFieldProxy { FieldDescriptorName="TotalPrice", DisplayField="Total"}
                                            },
                                        }
                                    },              
                                    Data = string.Empty
                                },
                                new PageComponent {
                                    RecordDescriptorName = "Order",
                                    DataviewId = "r2c2",                                            
                                    TabTitle = "Stuff 4",
                                    Visualizer = new Visualizer {
                                        Type = "Tabular",
                                        Settings = new Settings {                                            
                                            RecordDescriptorName = "Order",
                                            DisplayFields = new List<DisplayFieldProxy> 
                                            { 
                                                new DisplayFieldProxy { FieldDescriptorName="OrderReference", DisplayField="Order Ref"},
                                                new DisplayFieldProxy { FieldDescriptorName="OrderDatePlaced", DisplayField="Date Placed"},
                                                new DisplayFieldProxy { FieldDescriptorName="TotalPrice", DisplayField="Total"}
                                            },
                                        }
                                    },             
                                    Data = string.Empty
                                }
                            }
                        }
                    }
                };

                session.Store(userDashboard);
                session.SaveChanges();
            }
        }

        #endregion

        #region Helpers

        private static void Out(string p)
        {
            Console.WriteLine(p);
        }

        private static void DebugIfResponseError(IRestResponse response)
        {
            if (!response.StatusCode.ToString().StartsWith("Created"))
            {
                Console.WriteLine("Bugger. {0}", response.Content);
                throw new Exception("Shes broke...");
            }
        }

        private static void Randomize(Record record)
        {
            record.Fields.Single(f => f.FieldName == "TotalPrice").Value = Math.Round((decimal)new Random().NextDouble() * 10000, 2);
        }

        private static RecordDescriptor GetOrderRecordDescriptor(bool includeRelationships = false)
        {
            var orderFieldDescriptors = new List<FieldDescriptor>
            {
                new FieldDescriptor
                {
                    FriendlyType = FriendlyTypeName.String,
                    SystemTypeName = typeof (string).ToString(),
                    Name = "OrderRef",
                    Label = "OrderRef",
                    Description = "The primary order identifier",
                    Display = true,
                    DisplayInGrid = true,
                    ReadOnly = false
                },
                new FieldDescriptor
                {
                    FriendlyType = FriendlyTypeName.Date,
                    SystemTypeName = typeof (DateTime).ToString(),
                    Name = "OrderPlacedDate",
                    Label = "Order Placed Date",
                    Description = "The date the order was placed",
                    Display = true,
                    DisplayInGrid = true,
                    ReadOnly = false
                },
                new FieldDescriptor
                {
                    FriendlyType = FriendlyTypeName.Decimal,
                    SystemTypeName = typeof (decimal).ToString(),
                    Name = "TotalPrice",
                    Label = "Total Price",
                    Description = "The total worth of the order",
                    Display = true,
                    DisplayInGrid = true,
                    ReadOnly = true
                },
                new FieldDescriptor
                {
                    FriendlyType = FriendlyTypeName.Enumeration,
                    SystemTypeName = typeof (Enumeration).ToString(),
                    Name = "Size",
                    Label = "Size",
                    EnumerationId = "Enumerations/Size",
                    Display = true,
                    DisplayInGrid = true,
                    ReadOnly = false
                },
                new FieldDescriptor
                {
                    FriendlyType = FriendlyTypeName.Double,
                    SystemTypeName = typeof (Double).ToString(),
                    Name = "Discount",
                    Label = "Discount",
                    Display = true,
                    DisplayInGrid = true,
                    ReadOnly = false
                }
            };

            var orderDescriptor = new RecordDescriptor
            {
                FieldDescriptors = orderFieldDescriptors,
                Id = "recordDescriptors/Order",
                Name = "Order",
                Label = "Order"
            };

            return orderDescriptor;
        }

        private static RecordDescriptor GetOrderWithOrderLineRelationship()
        {
            using (var session = documentStore.OpenSession("SoftModel"))
            {
                var orderDescriptor = session.Load<RecordDescriptor>("recordDescriptors/Order");
                var orderLineRecordDescriptor = GetOrderLineRecordDescriptor();

                var orderOrderLinesRelField =
                  new FieldDescriptor
                  {
                      RelatedRecordDescriptorId = orderLineRecordDescriptor.Id,
                      FriendlyType = FriendlyTypeName.OneToMany,
                      SystemTypeName = typeof(List<string>).ToString(),
                      Name = "OrderLines",
                      Display = false,
                      DisplayInGrid = false,
                      ReadOnly = true
                  };

                orderDescriptor.FieldDescriptors.Add(orderOrderLinesRelField);
                orderDescriptor.Etag = session.Advanced.GetEtagFor(orderDescriptor).Value;

                return orderDescriptor;
            }
        }

        private static RecordDescriptor GetOrderLineRecordDescriptor()
        {
            var orderLineDescriptor = new RecordDescriptor
            {
                FieldDescriptors = new List<FieldDescriptor>
                    {
                        new FieldDescriptor
                            {
                                FriendlyType = FriendlyTypeName.String,
                                SystemTypeName = typeof (string).ToString(),
                                Name = "OrderLineRef",
                                Label = "OrderLineRef",
                                Description = "Primary identifer of the order line"
                            },
                        new FieldDescriptor
                            {
                                FriendlyType = FriendlyTypeName.Integer,
                                SystemTypeName = typeof(int).ToString(),
                                Name = "Quantity",
                                Label = "Quantity",
                                Description = "Quantity of the item"
                            },
                        new FieldDescriptor
                            {
                                FriendlyType = FriendlyTypeName.String,
                                SystemTypeName = typeof(string).ToString(),
                                Name = "ProductCode",
                                Label = "ProductCode",
                                Description = "The unique product identifier"
                            },
                        new FieldDescriptor
                            {
                                FriendlyType = FriendlyTypeName.OneToOne,
                                SystemTypeName = typeof(string).ToString(),
                                Name = "OrderReference",
                                Label = "OrderReference",
                                Description = "The parent reference to the order"
                            }
                    },
                Id = "recordDescriptors/OrderLine",
                Name = "OrderLine",
                Label = "OrderLine"
            };

            return orderLineDescriptor;
        }

        private static Record GetOrderRecord(string orderRef, DateTime orderPlacedDate, decimal totalPrice, double discount)
        {
            // Create order
            return new Record
            {
                RecordDescriptorRef = new RecordDescriptorRef
                {
                    RecordDescriptorId = "recordDescriptors/Order",
                    RecordDescriptorName = "Order"
                },
                Fields = new List<FieldValue> 
                {
                   new FieldValue 
                   {                           
                       FieldName = "OrderRef",
                       Value = orderRef
                   },
                   new FieldValue 
                   {
                       FieldName = "OrderPlacedDate",
                       Value = orderPlacedDate
                   },
                   new FieldValue 
                   {
                       FieldName = "TotalPrice",
                       Value = totalPrice
                   },
                   new FieldValue 
                   {  
                       FieldName = "Size",
                       Value = "Small"
                   },
                   new FieldValue 
                   {
                       FieldName = "Discount",
                       Value = discount
                   },
                   new FieldValue 
                   {
                        FieldName = "OrderLines", 
                       Value = new List<string>()
                   }
                }
            };
        }

        /// <summary>
        /// Hmm. Maybe ChildRecord should come from the schema namespace...
        /// </summary>
        private static Record GetOrderLineRecord(string orderLineRef, string parentRef, string parentFieldName, string product, int quantity)
        {
            return new Record
            {

                RecordDescriptorRef = new RecordDescriptorRef
                {
                    RecordDescriptorId = "recordDescriptors/OrderLine",
                    RecordDescriptorName = "OrderLine"
                },
                Fields = new List<FieldValue>
                    {
                        new FieldValue
                        {
                            FieldName = "OrderLineRef", 
                            Value = orderLineRef
                        },
                        new FieldValue
                        {
                            FieldName = "Quantity", 
                            Value = quantity
                        },
                        new FieldValue
                        {
                            FieldName = "ProductCode",
                            Value = product
                        },
                        new FieldValue
                        {
                            FieldName = "OrderReference",
                            ParentFieldName = parentFieldName,
                            Value = parentRef
                        }
                    },

                ParentRecordRefFieldName = "OrderReference",
                ParentRecordDescriptorName = "Order"
            };
        }

        private static void DeleteCollection(IDocumentSession session, string collectionName)
        {
            session.Advanced.DocumentStore.DatabaseCommands.DeleteByIndex("Raven/DocumentsByEntityName",
                new IndexQuery { Query = "Tag:" + collectionName },
                allowStale: true
            );
        }

        private static void DeleteIfExists<T>(string id)
        {
            using (var session = documentStore.OpenSession("SoftModel"))
            {
                var existing = session.Load<T>(id);
                if (existing != null)
                {
                    session.Delete<T>(existing);
                    session.SaveChanges();
                }
            }
        }

        private static void PingApi()
        {
            var client = new RestClient(softModelBaseUrl);
            var request = new RestRequest("access/ping", Method.GET);
            // request.AddParameter("application/json; charset=utf-8", jsonToSend, ParameterType.RequestBody);
            request.RequestFormat = DataFormat.Json;

            var reponse = client.Execute(request);
        }

        private static string IdFromResponse(IRestResponse response)
        {
            return JsonConvert.DeserializeObject<Record>(response.Content).Id;
        }

        #endregion
    }

    static class Ext
    {
        public static int IndexOf(this string[] array, string element)
        {
            var index = 0;
            foreach (var el in array)
            {
                if (string.Compare(el, element, true, CultureInfo.InvariantCulture) == 0)
                {
                    break;
                }
                else
                {
                    index++;
                }
            }

            return index;
        }
    }
}

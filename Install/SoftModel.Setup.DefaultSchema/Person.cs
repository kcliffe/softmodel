﻿namespace SoftModel.Setup.DefaultSchema
{
    public class Person
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Newtonsoft.Json;
using SoftModel.Schema;
using SoftModel.UI.Html.ViewModels.UI;

namespace SoftModel.JsonHelper
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            //var order = CreateOrderInstance();
            //var orderJson = JsonConvert.SerializeObject(order, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.Objects });
            //Clipboard.SetText(orderJson);

            //var orderLine = CreateOrderLineInstance();
            //var orderJsonLine = JsonConvert.SerializeObject(orderLine, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.Objects});
            //Clipboard.SetText(orderJsonLine);

            var dv = GetDashboardView();
            var jsonDv = JsonConvert.SerializeObject(dv, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.None });
            Clipboard.SetText(jsonDv);
        }

        public static UserDashboard GetDashboardView()
        {
            return new UserDashboard
            {
                Id = "kellys",
                Views = new List<DashboardView>
                {
                    new DashboardView {
                        DashboardName = "My Dashboard",
                        PageComponents = new List<PageComponent>                
                        {
                            new PageComponent 
                            {                       
                                DataviewId = "r1c1",
                                TabTitle = "Persons", 
                                Visualizer = new Visualizer
                                {
                                    Type  = "Tabular",
                                    Settings = new Settings
                                    {
                                        DisplayFields = new List<DisplayFieldProxy> 
                                        { 
                                            new DisplayFieldProxy { FieldDescriptorName="Name", DisplayField="Name"},
                                            new DisplayFieldProxy { FieldDescriptorName="Age", DisplayField="Age"}
                                        },
                                        Filter = string.Empty,
                                        OrderBy = "Name asc",
                                        RecordDescriptorName = "Person Schema"
                                    }
                                }
                            },
                            new PageComponent 
                            {                       
                                DataviewId = "r1c1",
                                TabTitle = "Persons", 
                                RecordDescriptorName = "Order",
                                Visualizer = new Visualizer
                                {
                                    Type  = "Tabular",
                                    Settings = new Settings
                                    {
                                         DisplayFields = new List<DisplayFieldProxy> 
                                        { 
                                            new DisplayFieldProxy { FieldDescriptorName="Name", DisplayField="Name"},
                                            new DisplayFieldProxy { FieldDescriptorName="Age", DisplayField="Age"}
                                        },
                                        Filter = string.Empty,
                                        OrderBy = "OrderRef asc",
                                        RecordDescriptorName = "Order"
                                    }
                                }
                            }
                        }
                    }
                }
            };
        }
    }
}
